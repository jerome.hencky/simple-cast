# Simple Cast

Stream your images and videos to your TV.

This application allows to stream images and videos on your device to a media receiver device
connected to your TV. The media receiver may be a Smart TV,or a TV box, or a multimedia box,
or a chromecast. Both your device and the media receiver must be connected on the same Wi-Fi
network. The media receiver must support UPnP or Google Cast protocols.
This application requires the Google Play Services availability on your device.

## Screenshots
<img src="graphics/screenshot_1_540x1080.png" height="540" width="270"/>
<img src="graphics/screenshot_2_540x1080.png" height="540" width="270"/>
<img src="graphics/screenshot_3_540x1080.png" height="540" width="270"/>

### Setup Instructions
1. Signing setup to build a release apk
* [Create a file named keystore.properties in the root directory of your project](https://developer.android.com/studio/publish/app-signing.html#secure-shared-keystore)
    with your release keystore information.
   
   The storeFile location can be a relative or absolute filename.
   (https://docs.gradle.org/current/userguide/working_with_files.html).
   Or, you can sign your APK from Android Studio "Build -> Generate Signed APK..."

2. Firebase Crashlytics setup
* This application uses [Firebase Crashlytics](https://firebase.google.com/docs/crashlytics) to
   collect crash reports and to help to fix stability issues.
* [Add Firebase to your Android Project](https://firebase.google.com/docs/android/setup).
* In the Firebase console, browse to your project settings and download a
   google-services.json file. Add it to your Android project at app level.
* If needed, link your Fabric Crashlytics app to Firebase in the Firebase console (Project Settings
   / Integrations tab). I'm not comfortable with this part, and I'm not sure this is required.
* If you integrated your Fabric Crashlytics app to Firebase, then a
   [Fabric API key](https://docs.fabric.io/android/fabric/settings/api-keys.html) is required in the
   manifest file. Create a file named fabric.properties at the app level of your Android project,
   and this file must contain a line with the property apiKey 
   (e.g. apiKey=b0c37f7ff932518631139df45e209cc9a7593c3f).
   This API Key will be injected in you manifest file at build time, since we use a placeholder.
   
3. Chromecast setup
* Get a Chromecast device and
   [set it up for development](https://developers.google.com/cast/docs/developers#setup_for_development).
* Register an application on the [Google Cast Developer Console](http://cast.google.com/publish).
   The easiest would be to use the Styled Media Receiver option there.
   You will get an App ID when you finish registering your application.
   Create a file named cast.properties in the app level of your project, and this file must contain
   a line with the property appId (e.g. appId=4F8B3483).

4. Use gradle to build the project.

5. Compile and deploy to your Android device.
