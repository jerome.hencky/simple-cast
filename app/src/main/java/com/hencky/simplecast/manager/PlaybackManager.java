package com.hencky.simplecast.manager;

import android.app.Service;
import android.content.Context;

import androidx.annotation.Nullable;

import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.playback.Playback;
import com.hencky.simplecast.server.MediaServerHelper;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;

/**
 * Manages the interactions among the container service and the actual media playback.
 */
public class PlaybackManager implements Playback.Callback {

    private final Playback mPlayback;
    private final PlaybackServiceCallback mServiceCallback;
    private MediaPlayerState mMediaPlayerState;

    public PlaybackManager(Playback playback, PlaybackServiceCallback serviceCallback) {
        mPlayback = playback;
        mServiceCallback = serviceCallback;
        mPlayback.setCallback(this);
    }

    @Override
    public void onMediaPlayerStateChanged(MediaPlayerState state) {
        mMediaPlayerState = state;
        mServiceCallback.onMediaPlayerStateChanged(state);
    }

    @Override
    public void onMediaPlayerStateObtained(MediaPlayerState state) {
        mServiceCallback.onMediaPlayerStateObtained(state);
    }

    @Override
    public void onError(String error) {
        mServiceCallback.onError(error);
    }

    @Override
    public void onPlaybackTimeObtained(long timeSeconds) {
        mServiceCallback.onPlaybackTimeObtained(timeSeconds);
    }

    @Override
    public void onActionInvoked(String action, ActionInvocationResult result,
                                @Nullable String message) {
        mServiceCallback.onActionInvoked(action, result, message);
    }

    @Override
    public void onMuteStateChanged(boolean muted) {
        mServiceCallback.onMuteStateChanged(muted);
    }

    @Override
    public void onMuteStateObtained(boolean muted) {
        mServiceCallback.onMuteStateObtained(muted);
    }

    /**
     * Gets a {@link Context} from a service.
     *
     * @return a Context
     */
    private Context getContext() {
        Service service = (Service) mServiceCallback;
        return service.getApplicationContext();
    }

    /**
     * Handles a request to pause playback.
     */
    public void handlePauseRequest() {
        mPlayback.pause();
    }

    /**
     * Handles a request to start playback.
     */
    public void handlePlayRequest() {
        mPlayback.play();
    }

    /**
     * Handles a request to stop playback.
     */
    public void handleStopRequest() {
        mPlayback.stop();
    }

    /**
     * Handles a request to seek to a playback position specified by a time in seconds.
     */
    public void handleSeekToPlaybackPositionRequest(long timeSeconds) {
        mPlayback.seekTo(timeSeconds);
    }

    /**
     * Handles a request to set up the playback for a given {@link MediaItem}.
     *
     * @param mediaItem the requested {@link MediaItem} to set up the playback
     */
    public void handleSetupRequest(MediaItem mediaItem) {
        MediaServerHelper.getInstance(getContext()).setupMediaServer(mediaItem);
        mPlayback.setup(mediaItem);
    }

    /**
     * Handles a request to get the playback time.
     */
    public void handleRequestPlaybackTime() {
        if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
            mPlayback.requestPlaybackTime();
        }
    }

    /**
     * Handles a request to get player state of the current media.
     */
    public void handleGetMediaPlayerState() {
        mPlayback.requestMediaPlayerState();
    }

    /**
     * Handles a request to get the mute state of the sound.
     */
    public void handleGetMuteState() {
        mPlayback.requestMuteState();
    }

    /**
     * Initializes the playback for a given {@link Device}.
     *
     * @param device the Device object that is used when initializing playback.
     */
    public void init(Device device) {
        mPlayback.init(device);
    }

    /**
     * Returns whether the receiver device has the seek capability
     * among the possible interactions with the media.
     *
     * @return {@code true} when the device has the seek capability, otherwise {@code false}
     */
    public boolean hasDeviceSeekCapability() {
        return mPlayback.hasDeviceSeekCapability();
    }

    /**
     * Handles a request to mute or restore the sound.
     *
     * @param muted {@code true} if the sound should be muted,
     *              {@code false} if the sound should be restored.
     */
    public void handleSetMuteRequest(boolean muted) {
        LogHelper.d("PlaybackManager", "muted: " + muted);
        mPlayback.setMute(muted);
    }

    public interface PlaybackServiceCallback {
        /**
         * Called on media player state changed.
         *
         * @param state the new media player state
         */
        void onMediaPlayerStateChanged(MediaPlayerState state);

        /**
         * Called on media player state obtained.
         *
         * @param state the media player state that is obtained
         */
        void onMediaPlayerStateObtained(MediaPlayerState state);

        /**
         * Called on error occurred.
         *
         * @param error the error message
         */
        void onError(String error);

        /**
         * Called on playback time obtained.
         *
         * @param timeSeconds the obtained playback time in seconds
         */
        void onPlaybackTimeObtained(long timeSeconds);

        /**
         * Called on action invoked.
         *
         * @param action  The string representation of the invoked action
         * @param result  The action invocation result
         * @param message The message that can be associated to the action invocation result
         */
        void onActionInvoked(String action, ActionInvocationResult result,
                             @Nullable String message);

        /**
         * Called on mute state changed.
         *
         * @param muted {@code true} if the sound is muted,
         *              {@code false} if the sound is restored.
         */
        void onMuteStateChanged(boolean muted);

        /**
         * Called on mute state obtained.
         *
         * @param muted {@code true} if the sound is muted,
         *              {@code false} if the sound is restored.
         */
        void onMuteStateObtained(boolean muted);
    }
}
