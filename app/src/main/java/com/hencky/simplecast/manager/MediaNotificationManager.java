package com.hencky.simplecast.manager;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.media.session.MediaSessionCompat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.Kind;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.service.MediaPlaybackService;
import com.hencky.simplecast.ui.CastControllerActivity;
import com.hencky.simplecast.ui.UpnpControllerActivity;
import com.hencky.simplecast.utils.ArrayUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;
import com.hencky.simplecast.utils.PlatformUtils;
import com.hencky.simplecast.utils.PreferencesHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Keeps track of a notification and updates it automatically.
 */
public class MediaNotificationManager extends BroadcastReceiver {

    private static final String TAG = LogHelper.makeLogTag(MediaNotificationManager.class);

    /**
     * Actions.
     */
    private static final String ACTION_PAUSE = AppKeys.APP_PACKAGE_NAME + ".ACTION_PAUSE";
    private static final String ACTION_PLAY = AppKeys.APP_PACKAGE_NAME + ".ACTION_PLAY";
    private static final String ACTION_REPLAY = AppKeys.APP_PACKAGE_NAME + ".ACTION_REPLAY";
    private static final String ACTION_FORWARD = AppKeys.APP_PACKAGE_NAME + ".ACTION_FORWARD";
    private static final String ACTION_STOP_CASTING = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_STOP_CASTING";

    private static final int NOTIFICATION_ID = 112;
    private static final int REQUEST_CODE = 100;
    private static final String CHANNEL_ID = "media_playback_channel";

    private final MediaPlaybackService mMediaPlaybackService;
    private final NotificationManagerCompat mNotificationManager;
    private final PendingIntent mPauseIntent;
    private final PendingIntent mPlayIntent;
    private final PendingIntent mReplayIntent;
    private final PendingIntent mForwardIntent;
    private final PendingIntent mStopIntent;
    private MediaItem mMediaItem;
    private MediaSessionCompat mMediaSession;

    private boolean mStarted;
    private int mPlayPauseButtonPosition;
    private int mReplayButtonPosition;
    private int mForwardButtonPosition;
    private int mStopButtonPosition;

    public MediaNotificationManager(MediaPlaybackService mediaPlaybackService) {
        mMediaPlaybackService = mediaPlaybackService;

        initializeMediaSession();

        mNotificationManager = NotificationManagerCompat.from(mMediaPlaybackService);

        String pkg = mMediaPlaybackService.getPackageName();
        mPauseIntent = PendingIntent.getBroadcast(mMediaPlaybackService, REQUEST_CODE,
                new Intent(ACTION_PAUSE).setPackage(pkg), PendingIntent.FLAG_CANCEL_CURRENT);
        mPlayIntent = PendingIntent.getBroadcast(mMediaPlaybackService, REQUEST_CODE,
                new Intent(ACTION_PLAY).setPackage(pkg), PendingIntent.FLAG_CANCEL_CURRENT);
        mReplayIntent = PendingIntent.getBroadcast(mMediaPlaybackService, REQUEST_CODE,
                new Intent(ACTION_REPLAY).setPackage(pkg), PendingIntent.FLAG_CANCEL_CURRENT);
        mForwardIntent = PendingIntent.getBroadcast(mMediaPlaybackService, REQUEST_CODE,
                new Intent(ACTION_FORWARD).setPackage(pkg), PendingIntent.FLAG_CANCEL_CURRENT);
        mStopIntent = PendingIntent.getBroadcast(mMediaPlaybackService, REQUEST_CODE,
                new Intent(ACTION_STOP_CASTING).setPackage(pkg), PendingIntent.FLAG_CANCEL_CURRENT);

        // Cancel all notifications to handle the case where the Service was killed and
        // restarted by the system.
        mNotificationManager.cancelAll();
    }

    /**
     * Initializes the media session.
     */
    private void initializeMediaSession() {
        mMediaSession = new MediaSessionCompat(mMediaPlaybackService, mMediaPlaybackService.getResources().getString(R.string.app_name));
    }

    /**
     * Posts the notification and starts tracking the session to keep it
     * updated. The notification will automatically be removed if the session is
     * destroyed before {@link #stopNotification} is called.
     */
    public void startNotification() {
        // The notification must be updated after setting started to true
        Notification notification = createNotification();

        if (notification != null) {
            IntentFilter actionsFilter = new IntentFilter();
            if (mMediaItem instanceof VideoItem) {
                actionsFilter.addAction(ACTION_PLAY);
                actionsFilter.addAction(ACTION_PAUSE);
                if (mMediaPlaybackService.hasDeviceSeekCapability()) {
                    actionsFilter.addAction(ACTION_REPLAY);
                    actionsFilter.addAction(ACTION_FORWARD);
                }
            }
            actionsFilter.addAction(ACTION_STOP_CASTING);

            mMediaPlaybackService.registerReceiver(this, actionsFilter);

            mMediaPlaybackService.startForeground(NOTIFICATION_ID, notification);

            mStarted = true;
        }
    }

    /**
     * Removes the notification and stops tracking the session. If the session
     * was destroyed this has no effect.
     */
    public void stopNotification() {
        if (mStarted) {
            mStarted = false;
            try {
                mNotificationManager.cancel(NOTIFICATION_ID);
                mMediaPlaybackService.unregisterReceiver(this);
            } catch (IllegalArgumentException ex) {
                // ignore if the receiver is not registered.
            }
            mMediaPlaybackService.stopForeground(true);
            LogHelper.d(TAG, "Stopped Notification.");
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        LogHelper.d(TAG, "Received intent with action " + action);
        if (action != null) {
            switch (action) {
                case ACTION_PAUSE:
                    MediaPlaybackUtils.pausePlayback(context);
                    break;
                case ACTION_PLAY:
                    MediaPlaybackUtils.startPlayback(context);
                    break;
                case ACTION_REPLAY:
                    if (MediaPlaybackUtils.isMediaPlaying(mMediaPlaybackService.getPlaybackState())) {

                        MediaPlaybackUtils.seekToPlaybackPosition(context,
                                mMediaPlaybackService.getPlaybackTime() -
                                        PreferencesHelper.getInstance(context)
                                                .getBackwardTimeInSeconds());
                    }
                    break;
                case ACTION_FORWARD:
                    if (MediaPlaybackUtils.isMediaPlaying(mMediaPlaybackService.getPlaybackState())) {

                        MediaPlaybackUtils.seekToPlaybackPosition(context,
                                mMediaPlaybackService.getPlaybackTime() +
                                        PreferencesHelper.getInstance(context)
                                                .getForwardTimeInSeconds());
                    }
                    break;
                case ACTION_STOP_CASTING:
                    MediaPlaybackUtils.stopPlayback(context);
                    break;
                default:
                    LogHelper.w(TAG, "Unknown intent is ignored. Action=", action);
            }
        }
    }

    /**
     * Creates a PendingIntent that will start a new expanded controller activity.
     *
     * @return A PendingIntent
     */
    private PendingIntent createContentIntent() {
        Class<?> clazz = null;
        final Device selectedDevice = mMediaPlaybackService.getDevice();
        if (DeviceType.UPNP.equals(selectedDevice.getDeviceType())) {
            clazz = UpnpControllerActivity.class;
        } else if (DeviceType.CAST.equals(selectedDevice.getDeviceType())) {
            clazz = CastControllerActivity.class;
        }

        Intent intent = new Intent(mMediaPlaybackService, clazz);
        intent.putExtra(AppKeys.EXTRA_DEVICE, selectedDevice);
        intent.putExtra(AppKeys.EXTRA_MEDIA_ITEM, mMediaPlaybackService.getMediaItem());

        return PendingIntent.getActivity(mMediaPlaybackService, REQUEST_CODE, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
    }

    /**
     * Creates a notification channel.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationManager manager =
                (NotificationManager) mMediaPlaybackService.getApplicationContext()
                        .getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                mMediaPlaybackService.getString(R.string.channel_name),
                NotificationManager.IMPORTANCE_LOW);
        channel.setDescription(mMediaPlaybackService.getString(R.string.channel_description));
        channel.setShowBadge(false);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        manager.createNotificationChannel(channel);
    }

    /**
     * Creates a {@link Notification}.
     *
     * @return the Notification object
     */
    private Notification createNotification() {
        if (PlatformUtils.isAtLeastO()) {
            createChannel();
        }

        NotificationCompat.Builder notificationBuilder = createBaseNotificationBuilder();
        if (notificationBuilder == null)
            return null;

        final Device device = mMediaPlaybackService.getDevice();
        if (device != null)
            notificationBuilder.setContentText(String.format(
                    mMediaPlaybackService.getString(R.string.prepare_casting_to_device),
                    device.getName()));

        return notificationBuilder.build();
    }

    /**
     * @return the indices of the actions to show in the compact notification view
     */
    private int[] getActionsInCompactView() {
        List<Integer> actions = new ArrayList<>();
        if (mReplayButtonPosition != -1)
            actions.add(mReplayButtonPosition);
        if (mPlayPauseButtonPosition != -1)
            actions.add(mPlayPauseButtonPosition);
        if (mStopButtonPosition != -1 && mMediaItem instanceof ImageItem)
            actions.add(mStopButtonPosition);
        return ArrayUtils.toPrimitive(actions.toArray(new Integer[0]));
    }

    /**
     * Updates the Notification.
     *
     * @param state the media player state
     */
    public void updateNotification(MediaPlayerState state) {
        LogHelper.d(TAG, "Media player state: " + state + ", Started: " + mStarted);

        if (mStarted) {
            NotificationCompat.Builder notificationBuilder = createBaseNotificationBuilder();
            if (notificationBuilder == null)
                return;

            addActions(notificationBuilder, state);

            androidx.media.app.NotificationCompat.MediaStyle mediaStyle =
                    new androidx.media.app.NotificationCompat.MediaStyle();
            mediaStyle.setShowActionsInCompactView(getActionsInCompactView());
            mediaStyle.setMediaSession(mMediaSession.getSessionToken());
            notificationBuilder.setStyle(mediaStyle);

            notificationBuilder.setContentText(String.format(
                    mMediaPlaybackService.getString(R.string.cast_casting_to_device),
                    mMediaPlaybackService.getDevice().getName()));

            notificationBuilder.setContentIntent(createContentIntent());

            if (state != null) {
                if (MediaPlayerState.STOPPED == state) {
                    mMediaPlaybackService.stopForeground(false);
                }
                notificationBuilder.setOngoing(MediaPlayerState.STOPPED != state);
            }
            CustomTarget<Bitmap> notificationTarget = new CustomTarget<Bitmap>(
                    Kind.NOTIFICATION.getWidth(),
                    Kind.NOTIFICATION.getHeight()) {

                @Override
                public void onResourceReady(@NonNull Bitmap resource,
                                            @Nullable Transition<? super Bitmap> transition) {
                    LogHelper.d(MediaNotificationManager.TAG,
                            "Bitmap for notification is ready.");
                    setBitmap(resource);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {
                    setBitmap(null);
                }

                /**
                 * Updates the Notification after the Bitmap resource is loaded.
                 */
                private void update() {
                    Notification notification = notificationBuilder.build();
                    mNotificationManager.notify(NOTIFICATION_ID, notification);
                }

                /**
                 * Sets the bitmap in the Notification.
                 * @param bitmap the bitmap to set
                 */
                private void setBitmap(@Nullable Bitmap bitmap) {
                    notificationBuilder.setLargeIcon(bitmap);
                    update();
                }
            };

            new Handler(Looper.getMainLooper()).post(() ->
                    Glide.with(mMediaPlaybackService.getApplicationContext())
                            .asBitmap()
                            .load(Uri.fromFile(new File(mMediaItem.getData())))
                            .centerCrop()
                            .into(notificationTarget));
        }
    }

    /**
     * Create the base {@link NotificationCompat.Builder} object with minimal settings.
     *
     * @return the base NotificationCompat.Builder object
     */
    private NotificationCompat.Builder createBaseNotificationBuilder() {
        if (mMediaItem == null)
            return null;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                mMediaPlaybackService.getApplicationContext(), CHANNEL_ID);

        builder.setSmallIcon(R.drawable.ic_personal_video_black_24dp)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentTitle(mMediaItem.getTitle());

        return builder;
    }

    /**
     * Add actions in a {@link NotificationCompat.Builder}.
     *
     * @param notificationBuilder The Notification builder used to add an Action to the Notification
     * @param state               The playback state
     */
    private void addActions(NotificationCompat.Builder notificationBuilder,
                            MediaPlayerState state) {
        mPlayPauseButtonPosition = -1;
        mReplayButtonPosition = -1;
        mForwardButtonPosition = -1;
        mStopButtonPosition = -1;

        if (mMediaItem instanceof VideoItem) {
            // Replay action
            // added at index 0
            //   when the MediaItem is a VideoItem
            //     and the receiver device is capable of handling the replay action.
            if (mMediaPlaybackService.hasDeviceSeekCapability()) {
                mReplayButtonPosition = 0;
                int replayIcon = R.drawable.ic_replay_30_black_24dp;
                String replayTitle = mMediaPlaybackService.getString(R.string.label_replay_30);
                if (PreferencesHelper.getInstance(mMediaPlaybackService)
                        .getBackwardTimeInSeconds() == 10) {
                    replayIcon = R.drawable.ic_replay_10_black_24dp;
                    replayTitle = mMediaPlaybackService.getString(R.string.label_replay_10);
                }
                notificationBuilder.addAction(new NotificationCompat.Action(replayIcon, replayTitle,
                        mReplayIntent));
            }

            // Play / Pause action
            // added at index 0
            //   when the MediaItem is a VideoItem
            //     and the receiver device is not capable of handling the replay action
            // or at index 1
            //   when the MediaItem is a VideoItem
            //     and the receiver device is capable of handling the replay action.
            if (mReplayButtonPosition == -1) {
                mPlayPauseButtonPosition = 0;
            } else if (mReplayButtonPosition == 0) {
                mPlayPauseButtonPosition = 1;
            }
            String label;
            int icon;
            PendingIntent intent;
            if (state == null) {
                if (MediaPlaybackUtils.isMediaPlaying(mMediaPlaybackService.getPlaybackState())) {
                    icon = R.drawable.ic_pause_black_24dp;
                    label = mMediaPlaybackService.getString(R.string.label_pause);
                    intent = mPauseIntent;
                } else {
                    icon = R.drawable.ic_play_arrow_black_24dp;
                    label = mMediaPlaybackService.getString(R.string.label_play);
                    intent = mPlayIntent;
                }
            } else {
                if (MediaPlayerState.PAUSED == state) {
                    icon = R.drawable.ic_play_arrow_black_24dp;
                    label = mMediaPlaybackService.getString(R.string.label_play);
                    intent = mPlayIntent;
                } else {
                    icon = R.drawable.ic_pause_black_24dp;
                    label = mMediaPlaybackService.getString(R.string.label_pause);
                    intent = mPauseIntent;
                }
            }
            notificationBuilder.addAction(new NotificationCompat.Action(icon, label, intent));

            // Forward action
            // added at index 2
            //   when the MediaItem is a VideoItem
            //     and the receiver device is capable of handling the forward action.
            if (mMediaPlaybackService.hasDeviceSeekCapability()) {
                mForwardButtonPosition = 2;
                int forwardIcon = R.drawable.ic_forward_30_black_24dp;
                String forwardTitle = mMediaPlaybackService.getString(R.string.label_forward_30);
                if (PreferencesHelper.getInstance(mMediaPlaybackService)
                        .getForwardTimeInSeconds() == 10) {
                    forwardIcon = R.drawable.ic_forward_10_black_24dp;
                    forwardTitle = mMediaPlaybackService.getString(R.string.label_forward_10);
                }
                notificationBuilder.addAction(new NotificationCompat.Action(forwardIcon, forwardTitle,
                        mForwardIntent));
            }
        }

        // Stop action
        // added at index 0
        //   when the MediaItem is an ImageItem
        // or at index 1
        //   when the MediaItem is a VideoItem
        //     and the receiver device is not capable of handling the seek actions
        // or at index 3
        //   when the MediaItem is a VideoItem
        //     and the receiver device is capable of handling the seek actions.
        if (mPlayPauseButtonPosition == -1) {
            mStopButtonPosition = 0;
        } else if (mPlayPauseButtonPosition == 0) {
            mStopButtonPosition = 1;
        } else if (mReplayButtonPosition == 0 && mForwardButtonPosition == 2) {
            mStopButtonPosition = 3;
        }
        notificationBuilder.addAction(new NotificationCompat.Action(
                R.drawable.ic_close_black_24dp, mMediaPlaybackService.getString(R.string.label_stop), mStopIntent));
    }

    /**
     * Sets the media data.
     *
     * @param mediaItem The {@link MediaItem} that contains the media data to set.
     */
    public void setMediaData(MediaItem mediaItem) {
        mMediaItem = mediaItem;
    }
}
