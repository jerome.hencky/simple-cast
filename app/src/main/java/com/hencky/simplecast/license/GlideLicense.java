package com.hencky.simplecast.license;

import android.content.Context;

import com.hencky.simplecast.R;

import de.psdev.licensesdialog.licenses.License;

public class GlideLicense extends License {

    private static final long serialVersionUID = 4965690669036567587L;

    @Override
    public String getName() {
        return "Glide License";
    }

    @Override
    public String readSummaryTextFromResources(Context context) {
        return getContent(context, R.raw.glide_summary);
    }

    @Override
    public String readFullTextFromResources(Context context) {
        return getContent(context, R.raw.glide_full);
    }

    @Override
    public String getVersion() {
        return "";
    }

    @Override
    public String getUrl() {
        return "https://github.com/bumptech/glide/blob/master/LICENSE";
    }
}
