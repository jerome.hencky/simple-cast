package com.hencky.simplecast.model;

public enum SortType {
    NAME, DATE, DURATION
}
