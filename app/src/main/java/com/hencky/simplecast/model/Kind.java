package com.hencky.simplecast.model;

/**
 * Represents a kind of thumbnail.
 */
public enum Kind {
    NOTIFICATION("notification-thumb", 256, 256),
    MICRO("micro-thumb", 96, 96),
    MINI("mini-thumb", 512, 384),
    FULL("full-thumb", 1280, 720);

    private final String name;
    private final int width;
    private final int height;

    Kind(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
