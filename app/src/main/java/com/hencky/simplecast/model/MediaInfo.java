package com.hencky.simplecast.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents information of a media resource.
 */
public class MediaInfo implements Parcelable {

    /**
     * The unique ID of the media resource.
     */
    private String mediaId;

    /**
     * The title of the media resource.
     */
    private String mediaTitle;

    /**
     * The string representation of the media resource URI.
     */
    private String mediaUri;

    public MediaInfo() {
    }

    protected MediaInfo(Parcel in) {
        this.mediaId = in.readString();
        this.mediaTitle = in.readString();
        this.mediaUri = in.readString();
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaTitle() {
        return mediaTitle;
    }

    public void setMediaTitle(String mediaTitle) {
        this.mediaTitle = mediaTitle;
    }

    public String getMediaUri() {
        return mediaUri;
    }

    public void setMediaUri(String mediaUri) {
        this.mediaUri = mediaUri;
    }

    @Override
    public String toString() {
        return "MediaInfo{" +
                "mediaId='" + mediaId + '\'' +
                ", mediaTitle='" + mediaTitle + '\'' +
                ", mediaUri='" + mediaUri + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mediaId);
        dest.writeString(this.mediaTitle);
        dest.writeString(this.mediaUri);
    }

    public static final Creator<MediaInfo> CREATOR = new Creator<MediaInfo>() {
        @Override
        public MediaInfo createFromParcel(Parcel source) {
            return new MediaInfo(source);
        }

        @Override
        public MediaInfo[] newArray(int size) {
            return new MediaInfo[size];
        }
    };
}
