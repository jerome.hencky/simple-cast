package com.hencky.simplecast.model;

public enum DisplayMode {
    LIST, GRID
}
