package com.hencky.simplecast.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a Media item.
 */
public class MediaItem implements Parcelable {
    /**
     * The unique ID for this MediaItem.
     */
    private String id;

    /**
     * The path to the file on disk.
     */
    private String data;

    /**
     * The display name of the file.
     */
    private String displayName;

    /**
     * The MIME type of the file.
     */
    private String mimeType;

    /**
     * The size of the file in bytes.
     */
    private Long size;

    /**
     * The title of the content.
     */
    private String title;

    /**
     * The time the file was last modified Units are seconds since 1970.
     */
    private Long dateModified;

    /**
     * The getHeight of the image/video in pixels.
     */
    private Long height;

    /**
     * The getWidth of the image/video in pixels.
     */
    private Long width;

    public MediaItem() {
    }

    MediaItem(Parcel in) {
        this.id = in.readString();
        this.data = in.readString();
        this.displayName = in.readString();
        this.mimeType = in.readString();
        this.size = (Long) in.readValue(Long.class.getClassLoader());
        this.title = in.readString();
        this.dateModified = (Long) in.readValue(Long.class.getClassLoader());
        this.height = (Long) in.readValue(Long.class.getClassLoader());
        this.width = (Long) in.readValue(Long.class.getClassLoader());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "MediaItem{" +
                "id=" + id +
                ", data='" + data + '\'' +
                ", displayName='" + displayName + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", size=" + size +
                ", title='" + title + '\'' +
                ", dateModified=" + dateModified +
                ", height=" + height +
                ", width=" + width +
                "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.data);
        dest.writeString(this.displayName);
        dest.writeString(this.mimeType);
        dest.writeValue(this.size);
        dest.writeString(this.title);
        dest.writeValue(this.dateModified);
        dest.writeValue(this.height);
        dest.writeValue(this.width);
    }

    public static final Creator<MediaItem> CREATOR = new Creator<MediaItem>() {
        @Override
        public MediaItem createFromParcel(Parcel source) {
            return new MediaItem(source);
        }

        @Override
        public MediaItem[] newArray(int size) {
            return new MediaItem[size];
        }
    };
}
