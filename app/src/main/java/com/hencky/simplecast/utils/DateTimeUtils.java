package com.hencky.simplecast.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Provides utilities for date and time manipulation.
 */
public class DateTimeUtils {

    /**
     * Pattern that represents a date and a time.
     */
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd  HH:mm:ss";

    private static SimpleDateFormat dateTimeSdf = new SimpleDateFormat(DATE_TIME_PATTERN);

    /**
     * Converts a millisecond duration to a string format.
     * The form of the duration string is [HH:][M]M:SS where
     * HH: 0 to 2 digits to indicate hours (nothing when it is 0,
     * or 1 to 9, then 10 to nn, when it is greater than 0),
     * MM: 1 to 2 digits to indicate minutes (0 to 9, then 10 to 59),
     * SS: exactly 2 digits to indicate seconds (00 to 59),
     *
     * @param millis the duration to convert to a string form
     * @return the duration string
     */
    public static String toDurationString(long millis) {
        if (millis < 0) {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long hours = TimeUnit.MILLISECONDS.toHours(millis) % 24;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;

        String hoursFormat = "%d:";
        if (hours == 0) {
            hoursFormat = "%s";
        }

        String minutesFormat = "%d:";
        if (minutes > 9) {
            minutesFormat = "%02d:";
        }

        String secondsFormat = "%02d";

        return String.format(
                Locale.getDefault(),
                hoursFormat + minutesFormat + secondsFormat,
                hours == 0 ? "" : hours, minutes, seconds);
    }

    /**
     * Converts an epoch time in seconds to a datetime string whose format is
     * "yyyy-MM-dd  HH:mm:ss".
     *
     * @param epochTimeInSeconds epoch time in seconds
     * @return the formatted datetime
     */
    public static String toDateTimeString(long epochTimeInSeconds) {
        return dateTimeSdf.format(new Date(epochTimeInSeconds * 1000));
    }
}
