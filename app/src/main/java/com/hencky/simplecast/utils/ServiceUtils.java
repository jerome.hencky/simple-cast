package com.hencky.simplecast.utils;

import android.app.ActivityManager;
import android.content.Context;

import androidx.annotation.NonNull;

/**
 * Provides utilities for interacting with Android services.
 */
class ServiceUtils {

    /**
     * Checks if a service specified by name is running in foreground.
     *
     * @param context     The context that is used
     * @param serviceName The service name
     * @return {@code true} if the service is running in foreground, otherwise {@code false}
     */
    static boolean isRunningInForeground(@NonNull Context context, String serviceName) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo runningServiceInfo : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (serviceName.equals(runningServiceInfo.service.getClassName())) {
                return runningServiceInfo.foreground;
            }
        }
        return false;
    }

}
