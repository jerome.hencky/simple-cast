package com.hencky.simplecast.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

import com.hencky.simplecast.model.DisplayMode;
import com.hencky.simplecast.model.SortMode;
import com.hencky.simplecast.model.SortOrder;
import com.hencky.simplecast.model.SortType;

public class PreferencesHelper {

    /**
     * Preferences
     */
    public static final String PREF_KEY_DISPLAY_MODE = "pref_display_mode";
    public static final String PREF_KEY_NIGHT_MODE = "pref_night_mode";
    public static final String PREF_KEY_DOUBLE_TAP_GESTURE = "pref_double_tap_gesture";
    public static final String PREF_KEY_IMAGES_SORT_MODE = "pref_images_sort_mode";
    public static final String PREF_KEY_VIDEOS_SORT_MODE = "pref_videos_sort_mode";
    public static final String PREF_KEY_MEDIA_LIB_FOLDER =
            "pref_media_lib_folder";
    public static final String PREF_KEY_BACKWARD_TIME = "pref_backward_time";
    public static final String PREF_KEY_FORWARD_TIME = "pref_forward_time";

    private static PreferencesHelper mInstance;

    private final SharedPreferences mPrefs;

    public static synchronized PreferencesHelper getInstance(@NonNull Context context) {
        if (mInstance == null) {
            mInstance = new PreferencesHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    private PreferencesHelper(Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Gets the display mode in pref_general.
     *
     * @return The display mode
     */
    public DisplayMode getDisplayMode() {
        return DisplayMode.valueOf(mPrefs.getString(PREF_KEY_DISPLAY_MODE, DisplayMode.GRID.name()));
    }

    /**
     * Returns whether the night mode was enabled or not.
     *
     * @return {@code true} whether the night mode is enabled, otherwise {@code false}
     */
    public boolean isNightModeEnabled() {
        return mPrefs.getBoolean(PREF_KEY_NIGHT_MODE, false);
    }

    public boolean isDoubleTapGestureEnabled() {
        return mPrefs.getBoolean(PREF_KEY_DOUBLE_TAP_GESTURE, false);
    }

    /**
     * Gets the images sort mode in pref_general.
     *
     * @return The images sort mode
     */
    public SortMode getImagesSortMode() {
        String sortModePref = mPrefs.getString(PREF_KEY_IMAGES_SORT_MODE, "DATE_DESC");
        String[] result = sortModePref.split("_");
        return new SortMode(SortType.valueOf(result[0]), SortOrder.valueOf(result[1]));
    }

    /**
     * Gets the videos sort mode in pref_general.
     *
     * @return The videos sort mode
     */
    public SortMode getVideosSortMode() {
        String sortModePref = mPrefs.getString(PREF_KEY_VIDEOS_SORT_MODE, "DATE_DESC");
        String[] result = sortModePref.split("_");
        return new SortMode(SortType.valueOf(result[0]), SortOrder.valueOf(result[1]));
    }

    /**
     * Gets the media lib folder in pref_general.
     *
     * @return The media lib folder
     */
    public String getMediaLib_Folder() {
        String mediaLibFolder = mPrefs.getString(PREF_KEY_MEDIA_LIB_FOLDER,
                Environment.getExternalStorageDirectory().getPath());
        return mediaLibFolder.endsWith(":") ?
                mediaLibFolder.substring(0, mediaLibFolder.indexOf(":")) : mediaLibFolder;
    }

    /**
     * Saves the media lib folder in pref_general.
     *
     * @param path the path to save as media lib folder
     * @return {@code true} if the media lib folder has been successfully saved in pref_general,
     * otherwise {@code false}.
     */
    public boolean setMediaLibFolder(String path) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_KEY_MEDIA_LIB_FOLDER, path);
        return editor.commit();
    }

    /**
     * Gets the backward time in pref_general.
     *
     * @return The backward time in seconds
     */
    public int getBackwardTimeInSeconds() {
        return Integer.valueOf(mPrefs.getString(PREF_KEY_BACKWARD_TIME, "30"));
    }

    /**
     * Gets the forward time in pref_general.
     *
     * @return The forward time in seconds
     */
    public int getForwardTimeInSeconds() {
        return Integer.valueOf(mPrefs.getString(PREF_KEY_FORWARD_TIME, "30"));
    }
}
