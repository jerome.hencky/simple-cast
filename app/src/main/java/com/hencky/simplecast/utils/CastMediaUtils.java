package com.hencky.simplecast.utils;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.MediaPlayerState;

/**
 * Provides utilities for interacting with a Cast media receiver.
 */
public class CastMediaUtils {

    private static final String TAG = LogHelper.makeLogTag(CastMediaUtils.class);

    /**
     * Registers the configuration of a {@link RemoteMediaClient} for a given
     * {@link RemoteMediaClient.Callback}.
     *
     * @param context                   The context to use.
     * @param remoteMediaClientCallback The RemoteMediaClient callback to register.
     */
    public static void registerRemoteMediaClientConf(
            @NonNull Context context,
            @NonNull RemoteMediaClient.Callback remoteMediaClientCallback) {
        final CastSession castSession = getCurrentCastSession(context);
        LogHelper.d(TAG, "Registering remoteMediaClientCallback in Session: " + castSession);
        if (castSession != null) {
            final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {
                remoteMediaClient.registerCallback(remoteMediaClientCallback);
            }
        }
    }

    /**
     * Unregisters the configuration of a {@link RemoteMediaClient} for a given
     * {@link RemoteMediaClient.Callback}.
     *
     * @param context                   The context to use.
     * @param remoteMediaClientCallback The RemoteMediaClient callback to unregister.
     */
    public static void unregisterRemoteMediaClientConf(
            @NonNull Context context,
            @Nullable RemoteMediaClient.Callback remoteMediaClientCallback) {
        final CastSession castSession = getCurrentCastSession(context);
        LogHelper.d(TAG, "Unregistering remoteMediaClientCallback in Session: " + castSession);
        if (castSession != null) {
            final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {
                remoteMediaClient.unregisterCallback(remoteMediaClientCallback);
            }
        }
    }

    /**
     * Gets the current media player state.
     *
     * @param context The context to use.
     * @return the current media player state.
     */
    public static int getMediaPlayerState(@NonNull Context context) {
        int playerState = MediaStatus.PLAYER_STATE_UNKNOWN;
        final CastSession castSession = getCurrentCastSession(context);
        if (castSession != null) {
            final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {
                MediaStatus mediaStatus = remoteMediaClient.getMediaStatus();
                if (mediaStatus != null) {
                    playerState = mediaStatus.getPlayerState();
                }
            }
        }
        return playerState;
    }

    /**
     * Converts a Cast player state integer to a media player state Enum value.
     *
     * @param context     The context to use.
     * @param playerState The Cast player state integer to convert.
     * @return the converted media player state Enum value.
     */
    public static MediaPlayerState toMediaPlayerState(@NonNull Context context, int playerState) {
        MediaPlayerState mediaPlayerState = MediaPlayerState.UNKNOWN;
        if (MediaStatus.PLAYER_STATE_BUFFERING == playerState ||
                MediaStatus.PLAYER_STATE_LOADING == playerState) {
            mediaPlayerState = MediaPlayerState.TRANSITIONING;
        } else if (MediaStatus.PLAYER_STATE_PLAYING == playerState) {
            mediaPlayerState = MediaPlayerState.PLAYING;
        } else if (MediaStatus.PLAYER_STATE_PAUSED == playerState) {
            mediaPlayerState = MediaPlayerState.PAUSED;
        } else if (MediaStatus.PLAYER_STATE_IDLE == playerState) {
            final int idleReason = getPlayerStateIdleReason(context);
            if (MediaStatus.IDLE_REASON_CANCELED == idleReason ||
                    MediaStatus.IDLE_REASON_FINISHED == idleReason ||
                    MediaStatus.IDLE_REASON_INTERRUPTED == idleReason) {
                mediaPlayerState = MediaPlayerState.STOPPED;
            } else if (MediaStatus.IDLE_REASON_NONE == idleReason) {
                mediaPlayerState = MediaPlayerState.NONE;
            } else if (MediaStatus.IDLE_REASON_ERROR == idleReason) {
                mediaPlayerState = MediaPlayerState.ERROR;
            }
        }
        return mediaPlayerState;
    }


    /**
     * Gets the player state idle reason.
     *
     * @param context The context to use.
     * @return the player state idle reason.
     */
    private static int getPlayerStateIdleReason(@NonNull Context context) {
        int idleReason = MediaStatus.IDLE_REASON_NONE;
        final CastSession castSession = getCurrentCastSession(context);
        if (castSession != null) {
            final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {
                MediaStatus mediaStatus = remoteMediaClient.getMediaStatus();
                if (mediaStatus != null) {
                    idleReason = mediaStatus.getIdleReason();
                }
            }
        }
        return idleReason;
    }

    /**
     * Gets the current Cast session.
     *
     * @param context The context to use.
     * @return the current Cast session.
     */
    public static CastSession getCurrentCastSession(@NonNull Context context) {
        return CastContext.getSharedInstance(context).getSessionManager().getCurrentCastSession();
    }

    /**
     * Gets a {@link MediaInfo} object from a given {@link MediaItem}.
     *
     * @param mediaItem the MediaItem from which the MediaInfo is obtained
     * @return the MediaInfo
     */
    public static MediaInfo getMediaInfo(Context context, MediaItem mediaItem) {
        MediaMetadata mediaMetadata = null;

        if (mediaItem instanceof ImageItem) {
            mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_PHOTO);
            mediaMetadata.putInt(MediaMetadata.KEY_HEIGHT, mediaItem.getHeight().intValue());
            mediaMetadata.putInt(MediaMetadata.KEY_WIDTH, mediaItem.getWidth().intValue());
            //mediaMetadata.addImage(new WebImage(MediaUtils.getMediaUri(context, mediaItem, Kind.MINI)));
        } else if (mediaItem instanceof VideoItem) {
            mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
            //mediaMetadata.addImage(new WebImage(MediaUtils.getMediaUri(context, mediaItem, Kind.MINI)));
            //mediaMetadata.addImage(new WebImage(MediaUtils.getMediaUri(context, mediaItem, Kind.FULL)));
        }
        mediaMetadata.putString(MediaMetadata.KEY_TITLE, mediaItem.getTitle());

        MediaInfo.Builder builder = new MediaInfo.Builder(
                MediaUtils.getMediaUri(context, mediaItem).toString())
                .setContentType(mediaItem.getMimeType())
                .setMetadata(mediaMetadata)
                .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED);

        if (mediaItem instanceof VideoItem) {
            VideoItem videoItem = (VideoItem) mediaItem;
            builder.setStreamDuration(videoItem.getDuration());
        }

        return builder.build();
    }

    /**
     * Gets the current media information.
     *
     * @param context The context to use.
     * @return {@code true} if the sound is muted,
     * {@code false} if the sound is restored.
     */
    public static boolean getMuteState(@NonNull Context context) {
        boolean muted = false;
        final CastSession castSession = getCurrentCastSession(context);
        if (castSession != null) {
            final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {
                MediaStatus mediaStatus = remoteMediaClient.getMediaStatus();
                if (mediaStatus != null) {
                    muted = mediaStatus.isMute();
                }
            }
        }
        return muted;
    }

    /**
     * @param context The context to use.
     * @return the playback time in seconds.
     */
    public static long getPlaybackTime(@NonNull Context context) {
        long playbackTime = 0;
        final CastSession castSession = CastMediaUtils.getCurrentCastSession(context);
        if (castSession != null) {
            final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {
                long positionMs = remoteMediaClient.getApproximateStreamPosition();
                if (positionMs != 0) {
                    playbackTime = positionMs / 1000;
                }
            }
        }
        return playbackTime;
    }
}
