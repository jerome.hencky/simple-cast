package com.hencky.simplecast.utils;

import android.content.Context;

import androidx.annotation.NonNull;

/**
 * Provides utilities for resources manipulation.
 */
public class ResoucesUtils {

    /**
     * Converts dips to pixels unit.
     *
     * @param context The context that will be used to convert the dips to pixels
     * @param dips    The dips to convert
     * @return the pixels
     */
    public static int dipToPixels(@NonNull Context context, int dips) {
        return (int) (dips * context.getResources().getDisplayMetrics().density);
    }

}
