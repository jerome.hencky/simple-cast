package com.hencky.simplecast.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Xml;

import androidx.annotation.NonNull;

import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.service.MediaPlaybackService;

import org.fourthline.cling.support.model.ProtocolInfo;
import org.fourthline.cling.support.model.ProtocolInfos;
import org.fourthline.cling.support.model.TransportState;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * This class provides UPnP utilities.
 */
public class UpnpUtils {

    private static final String TAG = LogHelper.makeLogTag(UpnpUtils.class);

    /**
     * Converts a UPnP transport state string to a media player state Enum value.
     *
     * @param transportState The UPnP transport state string to convert.
     * @return the converted media player state Enum value.
     */
    public static MediaPlayerState toMediaPlayerState(@NonNull String transportState) {
        MediaPlayerState mediaPlayerState = MediaPlayerState.UNKNOWN;
        if (TransportState.PAUSED_PLAYBACK.name().equals(transportState)) {
            mediaPlayerState = MediaPlayerState.PAUSED;
        } else if (TransportState.STOPPED.name().equals(transportState)) {
            mediaPlayerState = MediaPlayerState.STOPPED;
        } else if (TransportState.PLAYING.name().equals(transportState)) {
            mediaPlayerState = MediaPlayerState.PLAYING;
        } else if (TransportState.NO_MEDIA_PRESENT.name().equals(transportState)) {
            mediaPlayerState = MediaPlayerState.NONE;
        } else if (TransportState.TRANSITIONING.name().equals(transportState)) {
            mediaPlayerState = MediaPlayerState.TRANSITIONING;
        }
        return mediaPlayerState;
    }

    /**
     * Finds a {@link ProtocolInfo} in a list of {@link ProtocolInfos} by MIME type.
     *
     * @param protocolInfos The list of protocol informations
     * @param mimeType      the MIME type to find in the list
     * @return a protocol information
     */
    public static ProtocolInfo findProtocolInfoByMimeType(ProtocolInfos protocolInfos,
                                                          String mimeType) {
        if (protocolInfos == null)
            return null;

        List<ProtocolInfo> foundProtocolInfos = new ArrayList<>();

        // Search the mime type among the ProtocolInfos that are supported
        // by the receiver device.
        // Manage the special case when the receiver device only supports
        // the mime type video/x-mkv (on Samsung TVs for instance)
        // instead of conventional video/x-matroska.
        for (ProtocolInfo protocolInfo : protocolInfos) {
            ProtocolInfo foundProtocolInfo = null;

            if (protocolInfo.getContentFormat().equals(mimeType)) {
                foundProtocolInfo = protocolInfo;
            } else if ("video/x-mkv".equals(protocolInfo.getContentFormat()) &&
                    "video/x-matroska".equals(mimeType)) {
                foundProtocolInfo = protocolInfo;
                foundProtocolInfos.add(protocolInfo);
            }

            if (foundProtocolInfo != null) {
                foundProtocolInfos.add(foundProtocolInfo);
                if (ProtocolInfo.WILDCARD.equals(foundProtocolInfo.getAdditionalInfo())) {
                    return foundProtocolInfo;
                }
            }
        }

        if (foundProtocolInfos.size() > 0) {
            return foundProtocolInfos.get(0);
        }

        return null;
    }

    /**
     * Converts a millisecond duration to a string format for the res@duration property.
     * The form of the duration string is H+:MM:SS[.F+] where
     * H+: one or more digits to indicate elapsed hours,
     * MM: exactly 2 digits to indicate minutes (00 to 59),
     * SS: exactly 2 digits to indicate seconds (00 to 59),
     * F+: one or more digits to indicate fractions of seconds
     *
     * @param millis the duration to convert to a string form
     * @return the duration string
     */
    private static String toResDuration(long millis) {
        if (millis < 0) {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long hours = TimeUnit.MILLISECONDS.toHours(millis) % 24;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;
        long milliseconds = millis % 1000;

        return String.format(Locale.getDefault(), "%d:%02d:%02d.%d", hours, minutes, seconds,
                milliseconds);
    }

    /**
     * Builds metadata for a given {@link MediaItem} and a given media URI string
     * and a given {@link ProtocolInfo}.
     *
     * @param mediaItem    the MediaItem
     * @param uri          the string representation of the media URI
     * @param protocolInfo the ProtocolInfo supported by the media player for the MIME type
     * @return the metadata
     */
    public static String buildMetadata(MediaItem mediaItem, String uri,
                                       ProtocolInfo protocolInfo) {
        XmlSerializer s = Xml.newSerializer();
        StringWriter sw = new StringWriter();
        try {
            s.setOutput(sw);

            s.startDocument("UTF-8", null);

            s.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            s.startTag(null, "DIDL-Lite");
            s.attribute(null, "xmlns:dc", "http://purl.org/dc/elements/1.1/");
            s.attribute(null, "xmlns",
                    "urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/");
            s.attribute(null, "xmlns:upnp",
                    "urn:schemas-upnp-org:metadata-1-0/upnp/");
            //s.attribute(null, "xmlns:dlna", "urn:schemas-dlna-org:metadata-1-0/");
            /*
            s.attribute(null, "xsi:schemaLocation", "urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/ " +
                    "http://www.upnp.org/schemas/av/didl-lite.xsd " +
                    "urn:schemas-upnp-org:metadata-1-0/upnp/ " +
                    "http://www.upnp.org/schemas/av/upnp.xsd");
                    */

            s.startTag(null, "item");
            s.attribute(null, "id", mediaItem.getId());
            s.attribute(null, "parentID", "0");
            s.attribute(null, "restricted", "1");

            s.startTag(null, "dc:title");
            s.text(mediaItem.getTitle());
            s.endTag(null, "dc:title");

            s.startTag(null, "upnp:class");
            if (mediaItem instanceof ImageItem) {
                s.text("object.item.imageItem");
            } else if (mediaItem instanceof VideoItem) {
                s.text("object.item.videoItem");
            }
            s.endTag(null, "upnp:class");

            s.startTag(null, "res");
            s.attribute(null, "protocolInfo", protocolInfo.toString());
            s.attribute(null, "size", Long.toString(mediaItem.getSize()));
            if (mediaItem instanceof ImageItem) {
                String resolution = mediaItem.getWidth() + "x" + mediaItem.getHeight();
                s.attribute(null, "resolution", resolution);
            } else if (mediaItem instanceof VideoItem) {
                VideoItem videoItem = (VideoItem) mediaItem;
                s.attribute(null, "duration",
                        toResDuration(videoItem.getDuration()));
                s.attribute(null, "resolution", videoItem.getResolution());
            }
            s.text(uri);
            s.endTag(null, "res");

            s.endTag(null, "item");

            s.endTag(null, "DIDL-Lite");

            s.endDocument();

            s.flush();

            LogHelper.d(TAG, "xmlMetaData=" + sw.toString());
        } catch (IOException e) {
            LogHelper.e(TAG, "An I/O error occurred: " + e.getMessage());
        }

        return sw.toString();
    }

    /**
     * Handles a LastChange event received from an UPnP service.
     *
     * @param context               The context to use
     * @param lastChangeEventString The String representation of the LastChange event
     */
    public static void handleLastChangeEvent(@NonNull Context context,
                                             String lastChangeEventString) {
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            Intent intent = new Intent(context, MediaPlaybackService.class);
            intent.setAction(MediaPlaybackService.ACTION_HANDLE_UPNP_SERVICE_EVENT);
            intent.putExtra(AppKeys.EXTRA_LASTCHANGE_STRING, lastChangeEventString);
            context.startService(intent);
        }
    }
}
