package com.hencky.simplecast.utils;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

import androidx.annotation.NonNull;

/**
 * Provides utilities for bitmap operations.
 */
public class BitmapUtils {
    private static final int DEFAULT_COMPRESS_QUALITY = 90;

    /**
     * Converts the given bitmap to a byte array for a given compress format.
     *
     * @param bitmap The bitmap to convert
     * @param format The compress format for the bitmap to save
     * @return The saved file
     */
    public static byte[] toByteArray(@NonNull Bitmap bitmap,
                                     @NonNull Bitmap.CompressFormat format) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        bitmap.compress(format, DEFAULT_COMPRESS_QUALITY, os);
        return os.toByteArray();
    }
}
