package com.hencky.simplecast.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.StringTokenizer;

/**
 * Provides utilities for URI manipulation.
 */
public class UriUtils {

    /**
     * URL-encodes everything between "/"-characters. Encodes spaces as '%20' instead of '+'.
     */
    public static String encodeUri(String uri) {
        StringBuilder newUri = new StringBuilder();
        StringTokenizer st = new StringTokenizer(uri, "/ ", true);
        while (st.hasMoreTokens()) {
            String tok = st.nextToken();
            switch (tok) {
                case "/":
                    newUri.append("/");
                    break;
                case " ":
                    newUri.append("%20");
                    break;
                default:
                    try {
                        newUri.append(URLEncoder.encode(tok, "UTF-8"));
                    } catch (UnsupportedEncodingException ignored) {
                    }
                    break;
            }
        }
        return newUri.toString();
    }
}
