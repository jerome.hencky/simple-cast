package com.hencky.simplecast.utils;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.service.MediaPlaybackService;

/**
 * Provides utilities for media playback operations.
 */
public class MediaPlaybackUtils {

    /**
     * Actions
     */
    public static final String ACTION_MEDIA_PLAYER_STATE_CHANGED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_MEDIA_PLAYER_STATE_CHANGED";
    public static final String ACTION_MEDIA_PLAYER_STATE_OBTAINED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_MEDIA_PLAYER_STATE_OBTAINED";
    public static final String ACTION_MUTE_STATE_CHANGED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_MUTE_STATE_CHANGED";
    public static final String ACTION_MUTE_STATE_OBTAINED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_MUTE_STATE_OBTAINED";
    public static final String ACTION_PLAYBACK_TIME_OBTAINED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_PLAYBACK_TIME_OBTAINED";

    /**
     * Sets up playback for a given {@link Device} and {@link MediaItem}.
     *
     * @param context   The context to use.
     * @param device    The Device on which the setup is done.
     * @param mediaItem The MediaItem for which the setup is done.
     */
    public static void setupPlayback(@NonNull Context context, Device device, MediaItem mediaItem) {
        if (context == null)
            return;
        Intent intent = new Intent(context, MediaPlaybackService.class);
        intent.setAction(MediaPlaybackService.ACTION_SETUP_PLAYBACK);
        intent.putExtra(AppKeys.EXTRA_DEVICE, device);
        intent.putExtra(AppKeys.EXTRA_MEDIA_ITEM, mediaItem);
        ContextCompat.startForegroundService(context, intent);
    }

    /**
     * Starts playback.
     *
     * @param context The context to use.
     */
    public static void startPlayback(@NonNull Context context) {
        if (context == null)
            return;
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            Intent intent = new Intent(context, MediaPlaybackService.class);
            intent.setAction(MediaPlaybackService.ACTION_START_PLAYBACK);
            context.startService(intent);
        }
    }

    /**
     * Pauses playback.
     *
     * @param context The context to use.
     */
    public static void pausePlayback(@NonNull Context context) {
        if (context == null)
            return;
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            Intent intent = new Intent(context, MediaPlaybackService.class);
            intent.setAction(MediaPlaybackService.ACTION_PAUSE_PLAYBACK);
            context.startService(intent);
        }
    }

    /**
     * Stops playback.
     *
     * @param context The context to use.
     */
    public static void stopPlayback(@NonNull Context context) {
        if (context == null)
            return;
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            Intent intent = new Intent(context, MediaPlaybackService.class);
            intent.setAction(MediaPlaybackService.ACTION_STOP_PLAYBACK);
            context.startService(intent);
        }
    }

    /**
     * Seeks to a new playback position for a given time in seconds.
     *
     * @param context     The context to use.
     * @param timeSeconds The time in seconds the player to seek by.
     */
    public static void seekToPlaybackPosition(@NonNull Context context, long timeSeconds) {
        if (context == null)
            return;
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            Intent intent = new Intent(context, MediaPlaybackService.class);
            intent.setAction(MediaPlaybackService.ACTION_SEEK_TO_PLAYBACK_POSITION_TIME_SECONDS);
            intent.putExtra(AppKeys.EXTRA_TIME_SECONDS, timeSeconds);
            context.startService(intent);
        }
    }

    /**
     * Requests player state of the current media.
     *
     * @param context The context to use.
     */
    public static void requestMediaPlayerState(@NonNull Context context) {
        if (context == null)
            return;
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            Intent intent = new Intent(context, MediaPlaybackService.class);
            intent.setAction(MediaPlaybackService.ACTION_REQUEST_MEDIA_PLAYER_STATE);
            context.startService(intent);
        }

    }

    /**
     * Requests mute state of the sound.
     *
     * @param context The context to use.
     */
    public static void requestMuteState(@NonNull Context context) {
        if (context == null)
            return;
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            Intent intent = new Intent(context, MediaPlaybackService.class);
            intent.setAction(MediaPlaybackService.ACTION_REQUEST_MUTE_STATE);
            context.startService(intent);
        }
    }

    /**
     * Evaluates if the media is playing for a given {@link MediaPlayerState}.
     *
     * @param mediaPlayerState the current media player state
     * @return {@code true} when the media is playing, otherwise {@code false}.
     */
    public static boolean isMediaPlaying(@NonNull MediaPlayerState mediaPlayerState) {
        return MediaPlayerState.PLAYING.equals(mediaPlayerState) ||
                MediaPlayerState.SEEKED.equals(mediaPlayerState);
    }

    /**
     * Evaluates if the playback is started for a given {@link MediaPlayerState}.
     *
     * @param mediaPlayerState the current media player state
     * @return {@code true} when the playback is started, otherwise {@code false}.
     */
    public static boolean isPlaybackStarted(@NonNull MediaPlayerState mediaPlayerState) {
        return !MediaPlayerState.NONE.equals(mediaPlayerState) &&
                !MediaPlayerState.STOPPED.equals(mediaPlayerState) &&
                !MediaPlayerState.ERROR.equals(mediaPlayerState) &&
                !MediaPlayerState.UNKNOWN.equals(mediaPlayerState);
    }

    /**
     * Broadcasts an intent when the media player state has changed
     * to all interested BroadcastReceivers.
     *
     * @param context          The context that is used.
     * @param mediaPlayerState The media player state to send
     *                         to all interested BroadcastReceivers.
     * @param deviceType       The device type that is associated
     *                         to the change of media player state.
     */
    public static void broadcastMediaPlayerStateChangedIntent(
            @NonNull Context context,
            MediaPlayerState mediaPlayerState,
            DeviceType deviceType) {
        if (context == null)
            return;
        Intent intent = new Intent();
        intent.setAction(ACTION_MEDIA_PLAYER_STATE_CHANGED);
        intent.putExtra(AppKeys.EXTRA_PLAYER_STATE, mediaPlayerState);
        intent.putExtra(AppKeys.EXTRA_DEVICE_TYPE, deviceType);
        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * Broadcasts an intent when the media player state is obtained.
     *
     * @param context          The context that is used.
     * @param mediaPlayerState The obtained media player state
     *                         to send to all interested BroadcastReceivers.
     * @param deviceType       The device type that is associated
     *                         to the obtained media player state.
     */
    public static void broadcastMediaPlayerStateObtainedIntent(
            @NonNull Context context, MediaPlayerState mediaPlayerState, DeviceType deviceType) {
        if (context == null)
            return;
        Intent intent = new Intent();
        intent.setAction(ACTION_MEDIA_PLAYER_STATE_OBTAINED);
        intent.putExtra(AppKeys.EXTRA_PLAYER_STATE, mediaPlayerState);
        intent.putExtra(AppKeys.EXTRA_DEVICE_TYPE, deviceType);
        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * Broadcasts an intent when the playback time was obtained
     * to all interested BroadcastReceivers.
     *
     * @param context     The context to use.
     * @param timeSeconds The obtained playback time in seconds to send
     *                    to all interested BroadcastReceivers.
     * @param deviceType  The device type that is associated
     *                    to the obtained media playback time.
     */
    public static void broadcastPlaybackTimeObtainedIntent(@NonNull Context context,
                                                           long timeSeconds,
                                                           DeviceType deviceType) {
        if (context == null)
            return;
        Intent intent = new Intent();
        intent.setAction(ACTION_PLAYBACK_TIME_OBTAINED);
        intent.putExtra(AppKeys.EXTRA_PLAYBACK_TIME, timeSeconds);
        intent.putExtra(AppKeys.EXTRA_DEVICE_TYPE, deviceType);
        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * Mutes or restores the sound.
     *
     * @param context The context to use.
     * @param muted   {@code true} if the sound should be muted,
     *                {@code false} if the sound should be restored.
     */
    public static void setMute(@NonNull Context context, boolean muted) {
        if (context == null)
            return;
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            Intent intent = new Intent(context, MediaPlaybackService.class);
            intent.setAction(MediaPlaybackService.ACTION_SET_MUTE);
            intent.putExtra(AppKeys.EXTRA_MUTED, muted);
            context.startService(intent);
        }
    }


    /**
     * Broadcasts an intent when the mute state has changed
     * to all interested BroadcastReceivers.
     *
     * @param context The context that is used.
     * @param muted   The mute state to send
     *                to all interested BroadcastReceivers.
     */
    public static void broadcastMuteStateChangedIntent(@NonNull Context context, boolean muted) {
        if (context == null)
            return;
        Intent intent = new Intent();
        intent.setAction(ACTION_MUTE_STATE_CHANGED);
        intent.putExtra(AppKeys.EXTRA_MUTED, muted);
        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * Broadcasts an intent when the mute state is obtained
     * to all interested BroadcastReceivers.
     *
     * @param context The context that is used.
     * @param muted   The mute state to send
     *                to all interested BroadcastReceivers.
     */
    public static void broadcastMuteStateObtainedIntent(@NonNull Context context, boolean muted) {
        if (context == null)
            return;
        Intent intent = new Intent();
        intent.setAction(ACTION_MUTE_STATE_OBTAINED);
        intent.putExtra(AppKeys.EXTRA_MUTED, muted);
        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
    }

    /**
     * Called when a media controller component (Activity or Fragment) is paused.
     *
     * @param context The context that is used.
     */
    public static void onMediaControllerComponentPaused(Context context) {
        if (context == null)
            return;
        Intent intent = new Intent(context, MediaPlaybackService.class);
        intent.setAction(MediaPlaybackService.ACTION_CONTROLLER_COMPONENT_PAUSED);
        // Ensure the media playback service is running in foreground.
        if (ServiceUtils.isRunningInForeground(context, AppKeys.MEDIA_PLAYBACK_SERVICE)) {
            context.startService(intent);
        }
    }
}
