package com.hencky.simplecast.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.loader.content.CursorLoader;

import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.Kind;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.MediaType;
import com.hencky.simplecast.model.SortMode;
import com.hencky.simplecast.model.SortOrder;
import com.hencky.simplecast.model.SortType;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.server.MediaServer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * This class provides Media utilities.
 */
public class MediaUtils {

    private static final String TAG = LogHelper.makeLogTag(MediaUtils.class);

    private static final String[] PROJECTION_IMAGES = {
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media.TITLE,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.MIME_TYPE,
            MediaStore.Images.Media.SIZE,
            MediaStore.Images.Media.HEIGHT,
            MediaStore.Images.Media.WIDTH,
            MediaStore.Video.Media.DATE_MODIFIED
    };

    private static final String[] PROJECTION_VIDEO = {
            MediaStore.Video.Media._ID,
            MediaStore.Video.Media.DATA,
            MediaStore.Video.Media.TITLE,
            MediaStore.Video.Media.DISPLAY_NAME,
            MediaStore.Video.Media.DURATION,
            MediaStore.Video.Media.MIME_TYPE,
            MediaStore.Video.Media.SIZE,
            MediaStore.Video.Media.RESOLUTION,
            MediaStore.Video.Media.DATE_MODIFIED
    };

    private static final String SELECTION_IMAGES = MediaStore.Images.Media.DATA + " LIKE ?";

    private static final String SELECTION_VIDEO = MediaStore.Video.Media.DATA + " LIKE ?";

    private static final String[] PROJECTION_IMAGES_THUMB = {
            MediaStore.Images.Thumbnails._ID,
            MediaStore.Images.Thumbnails.DATA,
            MediaStore.Images.Thumbnails.HEIGHT,
            MediaStore.Images.Thumbnails.WIDTH
    };

    private static final String[] PROJECTION_VIDEO_THUMB = {
            MediaStore.Video.Thumbnails._ID,
            MediaStore.Video.Thumbnails.DATA,
            MediaStore.Video.Thumbnails.HEIGHT,
            MediaStore.Video.Thumbnails.WIDTH
    };

    private static final String SELECTION_IMAGES_THUMB = MediaStore.Images.Thumbnails.IMAGE_ID +
            " = ?";

    private static final String SELECTION_VIDEO_THUMB = MediaStore.Video.Thumbnails.VIDEO_ID +
            " = ?";

    private static final Map<String, String> IMAGE_SORT_ORDERS = new HashMap<>(4);
    private static final Map<String, String> VIDEO_SORT_ORDERS = new HashMap<>(6);

    static {
        IMAGE_SORT_ORDERS.put(SortType.NAME.name() + SortOrder.ASC,
                MediaStore.Images.Media.DISPLAY_NAME + " ASC");
        IMAGE_SORT_ORDERS.put(SortType.NAME.name() + SortOrder.DESC,
                MediaStore.Images.Media.DISPLAY_NAME + " DESC");
        IMAGE_SORT_ORDERS.put(SortType.DATE.name() + SortOrder.ASC,
                MediaStore.Images.Media.DATE_MODIFIED + " ASC");
        IMAGE_SORT_ORDERS.put(SortType.DATE.name() + SortOrder.DESC,
                MediaStore.Images.Media.DATE_MODIFIED + " DESC");

        VIDEO_SORT_ORDERS.put(SortType.NAME.name() + SortOrder.ASC,
                MediaStore.Video.Media.DISPLAY_NAME + " ASC");
        VIDEO_SORT_ORDERS.put(SortType.NAME.name() + SortOrder.DESC,
                MediaStore.Video.Media.DISPLAY_NAME + " DESC");
        VIDEO_SORT_ORDERS.put(SortType.DATE.name() + SortOrder.ASC,
                MediaStore.Video.Media.DATE_MODIFIED + " ASC");
        VIDEO_SORT_ORDERS.put(SortType.DATE.name() + SortOrder.DESC,
                MediaStore.Video.Media.DATE_MODIFIED + " DESC");
        VIDEO_SORT_ORDERS.put(SortType.DURATION.name() + SortOrder.ASC,
                MediaStore.Video.Media.DURATION + " ASC");
        VIDEO_SORT_ORDERS.put(SortType.DURATION.name() + SortOrder.DESC,
                MediaStore.Video.Media.DURATION + " DESC");
    }

    /**
     * Returns a {@link androidx.loader.content.CursorLoader} for Media items in a specified
     * directory. Assumes that the specified directory is located in external storage.
     * Rows will be sorted as specified by the sort mode.
     *
     * @param context        the context used by the CursorLoader
     * @param sortMode       the sort mode for the Media items
     * @param mediaLibFolder the directory where to find the Media items
     * @param mediaType      the Media type to find the Media items
     * @return a CursorLoader
     */
    public static CursorLoader newCursorLoaderForMediaItems(Context context, SortMode sortMode,
                                                            String mediaLibFolder,
                                                            MediaType mediaType) {
        LogHelper.d(TAG, "mediaLibFolder: " + mediaLibFolder);
        LogHelper.d(TAG, "mediaType: " + mediaType);
        final String pathArg = mediaLibFolder + "/%";
        final String[] selectionArgs = {pathArg};
        Map<String, String> sortOrders = null;
        Uri uri = null;
        String[] projection = null;
        String selection = null;

        if (MediaType.IMAGE.equals(mediaType)) {
            sortOrders = new HashMap<>(IMAGE_SORT_ORDERS);
            uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            projection = PROJECTION_IMAGES;
            selection = SELECTION_IMAGES;
        } else if (MediaType.VIDEO.equals(mediaType)) {
            sortOrders = new HashMap<>(VIDEO_SORT_ORDERS);
            uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            projection = PROJECTION_VIDEO;
            selection = SELECTION_VIDEO;
        }
        String sortOrder = sortOrders.get(sortMode.getSortType().name() +
                sortMode.getSortOrder().name());

        return new CursorLoader(context, uri, projection, selection, selectionArgs, sortOrder);
    }

    /**
     * Gets a media {@link Uri} for a given {@link MediaItem}.
     *
     * @param context   the context to use
     * @param mediaItem the MediaItem for which the media Uri is obtained
     * @return the media Uri
     */
    public static Uri getMediaUri(@NonNull Context context, @NonNull MediaItem mediaItem) {
        return getMediaUri(context, mediaItem, null);
    }

    /**
     * Gets a media {@link Uri} for a given {@link MediaItem} and a kind of thumbnail.
     *
     * @param context   the context to use
     * @param mediaItem the MediaItem for which the media Uri is obtained
     * @param kind      the kind of thumbnail if needed
     * @return the media Uri
     */
    public static Uri getMediaUri(@NonNull Context context, @NonNull MediaItem mediaItem,
                                  Kind kind) {

        String extension = null;
        if (mediaItem instanceof VideoItem) {
            if ("video/x-mkv".equals(mediaItem.getMimeType())) {
                extension = MimeTypeMap.getSingleton().getExtensionFromMimeType("video/x-matroska");
            }
        }
        if (extension == null) {
            if (kind == null) {
                extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(
                        mediaItem.getMimeType());
            } else {
                extension = "jpg";
            }
        }

        String path;
        if (kind == null) {
            path = UriUtils.encodeUri("/" + mediaItem.getId() + "." + extension);
        } else {
            path = UriUtils.encodeUri("/" + mediaItem.getId() + "-" + kind.getName() + "." +
                    extension);
        }

        return new Uri.Builder()
                .scheme(MediaServer.DEFAULT_SCHEME)
                .encodedAuthority(NetworkUtils.getWifiIPAddress(context) + ":" + MediaServer
                        .DEFAULT_PORT)
                .encodedPath(path)
                .build();
    }

    /**
     * Gets a {@link ImageItem} for a given Cursor moved at a specific position.
     *
     * @param cursor the Cursor moved a specific position
     * @return the MediaItem
     */
    public static ImageItem getImageItem(Cursor cursor) {
        if (cursor == null)
            return null;

        ImageItem imageItem = new ImageItem();
        long imageId = cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media._ID));
        imageItem.setId(Long.toString(imageId));
        String data = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        imageItem.setData(data);
        imageItem.setDisplayName(cursor.getString(cursor.getColumnIndex(
                MediaStore.Images.Media.DISPLAY_NAME)));
        imageItem.setMimeType(cursor.getString(cursor.getColumnIndex(
                MediaStore.Images.Media.MIME_TYPE)));
        imageItem.setSize(cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.SIZE)));
        imageItem.setTitle(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.TITLE)));
        imageItem.setHeight(cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.HEIGHT)));
        imageItem.setWidth(cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.WIDTH)));
        imageItem.setDateModified(cursor.getLong(cursor.getColumnIndex(
                MediaStore.Images.Media.DATE_MODIFIED)));

        return imageItem;
    }

    /**
     * Gets a {@link VideoItem} for a given Cursor moved at a specific position.
     *
     * @param cursor the Cursor moved a specific position
     * @return the VideoItem
     */
    public static VideoItem getVideoItem(Cursor cursor) {
        if (cursor == null)
            return null;

        VideoItem videoItem = new VideoItem();
        long videoId = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media._ID));
        videoItem.setId(Long.toString(videoId));
        String data = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
        videoItem.setData(data);
        videoItem.setDisplayName(cursor.getString(cursor.getColumnIndex(
                MediaStore.Video.Media.DISPLAY_NAME)));
        videoItem.setDuration(cursor.getLong(cursor.getColumnIndex(
                MediaStore.Video.Media.DURATION)));
        videoItem.setMimeType(cursor.getString(cursor.getColumnIndex(
                MediaStore.Video.Media.MIME_TYPE)));
        videoItem.setSize(cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.SIZE)));
        videoItem.setTitle(cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.TITLE)));
        videoItem.setResolution(cursor.getString(cursor.getColumnIndex(
                MediaStore.Video.Media.RESOLUTION)));
        videoItem.setDateModified(cursor.getLong(cursor.getColumnIndex(
                MediaStore.Video.Media.DATE_MODIFIED)));

        return videoItem;
    }

    /**
     * Gets a mini thumbnail for a given {@link MediaItem}.
     *
     * @param context   the context to use
     * @param mediaItem the MediaItem from which the mini thumbnail is obtained
     * @return the mini thumbnail
     */
    public static ImageItem getMiniThumbnail(@NonNull Context context,
                                             @NonNull MediaItem mediaItem) {
        ImageItem miniThumb = null;

        ContentResolver cr = context.getContentResolver();

        // Try to find a thumbnail in the MediaStore.
        // try-with-resources statement that ensures that the declared resource cursor
        // is closed at the end of the statement.
        Uri uri = null;
        String[] projectionMediaThumb = null;
        String selectionMediaThumb = null;
        if (mediaItem instanceof ImageItem) {
            uri = MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;
            projectionMediaThumb = PROJECTION_IMAGES_THUMB;
            selectionMediaThumb = SELECTION_IMAGES_THUMB;
        } else if (mediaItem instanceof VideoItem) {
            uri = MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI;
            projectionMediaThumb = PROJECTION_VIDEO_THUMB;
            selectionMediaThumb = SELECTION_VIDEO_THUMB;
        }
        try (Cursor cursor = cr.query(
                uri,
                projectionMediaThumb,
                selectionMediaThumb,
                new String[]{mediaItem.getId()},
                null)) {
            if (cursor != null && cursor.moveToFirst()) {
                String idColumnName = null;
                String dataColumnName = null;
                String heightColumnName = null;
                String widthColumnName = null;

                if (mediaItem instanceof ImageItem) {
                    idColumnName = MediaStore.Images.Thumbnails._ID;
                    dataColumnName = MediaStore.Images.Thumbnails.DATA;
                    heightColumnName = MediaStore.Images.Thumbnails.HEIGHT;
                    widthColumnName = MediaStore.Images.Thumbnails.WIDTH;
                } else if (mediaItem instanceof VideoItem) {
                    idColumnName = MediaStore.Video.Thumbnails._ID;
                    dataColumnName = MediaStore.Video.Thumbnails.DATA;
                    heightColumnName = MediaStore.Video.Thumbnails.HEIGHT;
                    widthColumnName = MediaStore.Video.Thumbnails.WIDTH;
                }

                long id = cursor.getLong(cursor.getColumnIndex(idColumnName));
                String data = cursor.getString(cursor.getColumnIndex(dataColumnName));
                long height = cursor.getLong(cursor.getColumnIndex(heightColumnName));
                long width = cursor.getLong(cursor.getColumnIndex(widthColumnName));
                String mimeType = cr.getType(
                        MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI.buildUpon()
                                .appendEncodedPath(File.separator + id).build());

                miniThumb = new ImageItem();
                miniThumb.setId(Long.toString(id));
                miniThumb.setData(data);
                miniThumb.setHeight(height);
                miniThumb.setWidth(width);
                miniThumb.setMimeType(mimeType);
            }
        }

        return miniThumb;
    }


    /**
     * Gets a {@link ImageItem} object that represents a thumbnail for a given file and
     * a {@link Kind}.
     *
     * @param resource the file
     * @param kind     the thumbnail kind
     * @return a {@link ImageItem} object
     */
    public static ImageItem getThumbnail(@NonNull File resource, @NonNull Kind kind) {
        ImageItem thumb = new ImageItem();
        thumb.setId(FilenameUtils.getBaseName(resource.getName()));
        thumb.setData(resource.getPath());
        thumb.setHeight((long) kind.getHeight());
        thumb.setWidth((long) kind.getWidth());
        thumb.setMimeType("image/jpeg");
        return thumb;
    }
}
