package com.hencky.simplecast.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.playback.UpnpPlayback;
import com.hencky.simplecast.ui.UpnpControllerActivity;
import com.hencky.simplecast.upnp.UpnpSession;
import com.hencky.simplecast.utils.LogHelper;

public class UpnpMiniControllerFragment extends MiniControllerFragment {

    private static final String TAG = LogHelper.makeLogTag(UpnpMiniControllerFragment.class);

    private CastContext mCastContext;
    private UpnpSession mUpnpSession;

    /**
     * Broadcast receiver for tracking UPnP events.
     */
    private final BroadcastReceiver mUpnpEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);
            if (intent != null) {
                String action = intent.getAction();
                if (UpnpPlayback.ACTION_AVTRANSPORT_SETAVTRANSPORTURI.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(AppKeys
                                    .EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received SetAVTransportURI action invocation " +
                            "result: " + result.name());
                    setLoadingIndicatorVisibility(View.VISIBLE);
                    if (ActionInvocationResult.SUCCESS.equals(result)) {
                        setupMiniController();
                        setMiniControllerViewVisibility(View.VISIBLE);
                    }
                }
            }
        }
    };

    private final SessionManagerListener<UpnpSession> mUpnpSessionManagerListener =
            new UpnpSessionManagerListenerImpl();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCastContext = CastContext.getSharedInstance(getBaseFragmentContext());
        registerBroadcastReceivers();
    }

    @Override
    public void onDestroy() {
        unregisterBroadcastReceivers();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCastContext.getSessionManager().addSessionManagerListener(
                mUpnpSessionManagerListener, UpnpSession.class);
    }

    @Override
    public void onPause() {
        mCastContext.getSessionManager().removeSessionManagerListener(
                mUpnpSessionManagerListener, UpnpSession.class);
        super.onPause();
    }

    @Override
    protected Session getSession() {
        return mUpnpSession;
    }

    @Override
    protected Intent getStartActivityIntent() {
        Intent intent = new Intent();
        intent.setClass(getBaseFragmentContext(), UpnpControllerActivity.class);
        return intent;
    }

    @Override
    protected boolean hasAffinityWithDeviceType(DeviceType deviceType) {
        return DeviceType.UPNP.equals(deviceType);
    }

    @Override
    protected boolean hasAffinityWithSession(Session session) {
        return (session instanceof UpnpSession);
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter upnpEventIntentFilter = new IntentFilter(
                UpnpPlayback.ACTION_AVTRANSPORT_SETAVTRANSPORTURI);
        LocalBroadcastManager.getInstance(getBaseFragmentContext()).registerReceiver(
                mUpnpEventReceiver, upnpEventIntentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(getBaseFragmentContext()).unregisterReceiver(
                mUpnpEventReceiver);
    }

    /**
     * Monitors events of a {@link UpnpSession} instance.
     */
    private class UpnpSessionManagerListenerImpl implements SessionManagerListener<UpnpSession> {

        @Override
        public void onSessionStarting(UpnpSession upnpSession) {
        }

        @Override
        public void onSessionStarted(UpnpSession upnpSession, String s) {
            LogHelper.d(TAG, "onSessionStarted - session: " + upnpSession);
            mUpnpSession = upnpSession;
        }

        @Override
        public void onSessionStartFailed(UpnpSession upnpSession, int i) {
        }

        @Override
        public void onSessionEnding(UpnpSession upnpSession) {
        }

        @Override
        public void onSessionEnded(UpnpSession upnpSession, int i) {
            LogHelper.d(TAG, "onSessionEnded - session: " + upnpSession);
            if (upnpSession == mUpnpSession) {
                mUpnpSession = null;
            }
        }

        @Override
        public void onSessionResuming(UpnpSession upnpSession, String s) {
        }

        @Override
        public void onSessionResumed(UpnpSession upnpSession, boolean b) {
            LogHelper.d(TAG, "onSessionResumed - session: " + upnpSession);
            mUpnpSession = upnpSession;
        }

        @Override
        public void onSessionResumeFailed(UpnpSession upnpSession, int i) {
        }

        @Override
        public void onSessionSuspended(UpnpSession upnpSession, int i) {
        }
    }

}
