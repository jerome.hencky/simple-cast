package com.hencky.simplecast.ui;

import android.os.Bundle;

import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;

import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.upnp.UpnpCoreHelper;
import com.hencky.simplecast.upnp.UpnpRouteProvider;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;

public class UpnpControllerActivity extends ExpandedControllerActivity {

    private static final String TAG = LogHelper.makeLogTag(UpnpControllerActivity.class);

    @Override
    protected void buildMediaRouteSelector() {
        // Build a MediaRouteSelector with capability
        // to discover and use routes to UPnP devices.
        setMediaRouteSelector(new MediaRouteSelector.Builder()
                .addControlCategory(UpnpRouteProvider.CATEGORY_UPNP).build());
    }

    @Override
    protected void buildMediaRouterCallback() {
        setMediaRouterCallback(new MediaRouterCallback());
    }

    @Override
    public boolean hasSeekToPlaybackPositionControl() {
        return UpnpCoreHelper.getInstance(this).hasRelTimeSeekModeCapability();
    }

    @Override
    public boolean hasSetMuteControl() {
        return UpnpCoreHelper.getInstance(this).hasSetMuteCapability();
    }

    /**
     * Callback for receiving events about media routing changes.
     */
    private class MediaRouterCallback extends MediaRouter.Callback {
        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo route, int reason) {
            LogHelper.d(TAG, "Unselected route: id=" + route.getId() + ", name=" +
                    route.getName() + ", reason=" + reason);
            if (MediaRouter.UNSELECT_REASON_STOPPED == reason) {
                // Case when the user pressed the stop casting button.
                if (MediaPlaybackUtils.isPlaybackStarted(getMediaPlayerState())) {
                    Bundle extras = route.getExtras();
                    Device unselectedDevice = extras.getParcelable(AppKeys.EXTRA_DEVICE);
                    if (getSelectedDevice().getIdentifier().equals(
                            unselectedDevice.getIdentifier())) {
                        if (UpnpRouteProvider.getInstance(UpnpControllerActivity.this)
                                .providesDevice(getSelectedDevice())) {
                            MediaPlaybackUtils.stopPlayback(UpnpControllerActivity.this);
                        } else {
                            // Notify other application components (including this activity)
                            // about the change to STOPPED player state
                            // so they can update their state if needed.
                            MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                                    UpnpControllerActivity.this,
                                    MediaPlayerState.STOPPED,
                                    DeviceType.UPNP);
                        }
                    }
                }
            }
            onRouteUnselected(router, route);
        }
    }

}
