package com.hencky.simplecast.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.playback.Playback;
import com.hencky.simplecast.ui.CastControllerActivity;
import com.hencky.simplecast.utils.CastMediaUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;

public class CastMiniControllerFragment extends MiniControllerFragment {

    private static final String TAG = LogHelper.makeLogTag(CastMiniControllerFragment.class);

    private CastContext mCastContext;
    private CastSession mCastSession;

    /**
     * Broadcast receiver for tracking Cast events.
     */
    private final BroadcastReceiver mCastEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);
            if (intent != null) {
                String action = intent.getAction();
                if (Playback.ACTION_SETUP.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received Setup action invocation result: " +
                            result.name());
                    if (ActionInvocationResult.SUCCESS.equals(result)) {
                        setupMiniController();
                        setControllerButtonState();
                        setMiniControllerViewVisibility(View.VISIBLE);
                        setMiniControllerButtonVisibility(View.VISIBLE);
                    }
                }
            }
        }
    };

    private final SessionManagerListener<CastSession> mCastSessionManagerListener =
            new CastSessionManagerListenerImpl();

    /**
     * Callback for tracking player status changes.
     */
    private final RemoteMediaClient.Callback mRemoteMediaClientCallback =
            new RemoteMediaClient.Callback() {
                @Override
                public void onStatusUpdated() {
                    int playerState = CastMediaUtils.getMediaPlayerState(getBaseFragmentContext());
                    MediaPlayerState mediaPlayerState = CastMediaUtils.toMediaPlayerState(
                            getBaseFragmentContext(), playerState);
                    if (getMediaPlayerState().equals(mediaPlayerState)) {
                        LogHelper.d(TAG,
                                "onStatusUpdated - ignoring identical media player state: "
                                        + mediaPlayerState);
                        return;
                    }
                    setMediaPlayerState(mediaPlayerState);
                    LogHelper.d(TAG, "onStatusUpdated - media player state: " +
                            getMediaPlayerState());

                    // Notify other application components
                    // about the changed player state
                    // so they can update their state if needed.
                    MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                            getBaseFragmentContext(), getMediaPlayerState(), DeviceType.CAST);
                }
            };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCastContext = CastContext.getSharedInstance(getBaseFragmentContext());
        registerBroadcastReceivers();
    }

    @Override
    public void onDestroy() {
        unregisterBroadcastReceivers();
        super.onDestroy();
    }

    @Override
    protected Session getSession() {
        return mCastSession;
    }

    @Override
    public void onResume() {
        super.onResume();
        mCastContext.getSessionManager().addSessionManagerListener(
                mCastSessionManagerListener, CastSession.class);
        registerRemoteMediaClientConf();
    }

    @Override
    public void onPause() {
        mCastContext.getSessionManager().removeSessionManagerListener(
                mCastSessionManagerListener, CastSession.class);
        unregisterRemoteMediaClientConf();
        super.onPause();
    }

    @Override
    protected Intent getStartActivityIntent() {
        Intent intent = new Intent();
        intent.setClass(getBaseFragmentContext(), CastControllerActivity.class);
        return intent;
    }

    @Override
    protected boolean hasAffinityWithDeviceType(DeviceType deviceType) {
        return DeviceType.CAST.equals(deviceType);
    }

    @Override
    protected boolean hasAffinityWithSession(Session session) {
        return (session instanceof CastSession);
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter castEventIntentFilter = new IntentFilter(Playback.ACTION_SETUP);
        LocalBroadcastManager.getInstance(getBaseFragmentContext()).registerReceiver(
                mCastEventReceiver, castEventIntentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(getBaseFragmentContext()).unregisterReceiver(
                mCastEventReceiver);
    }

    /**
     * Registers the configuration of the {@link RemoteMediaClient} if needed.
     */
    private void registerRemoteMediaClientConf() {
        CastMediaUtils.registerRemoteMediaClientConf(
                getBaseFragmentContext(),
                mRemoteMediaClientCallback);
    }

    /**
     * Unregisters the configuration of the {@link RemoteMediaClient} if needed.
     */
    private void unregisterRemoteMediaClientConf() {
        CastMediaUtils.unregisterRemoteMediaClientConf(
                getBaseFragmentContext(),
                mRemoteMediaClientCallback);
    }

    /**
     * Monitors events of a {@link CastSession} instance.
     */
    private class CastSessionManagerListenerImpl implements SessionManagerListener<CastSession> {

        @Override
        public void onSessionStarting(CastSession castSession) {
        }

        @Override
        public void onSessionStarted(CastSession castSession, String s) {
            LogHelper.d(TAG, "onSessionStarted - session: " + castSession);
            mCastSession = castSession;
            registerRemoteMediaClientConf();
        }

        @Override
        public void onSessionStartFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionEnding(CastSession castSession) {
        }

        @Override
        public void onSessionEnded(CastSession castSession, int i) {
            LogHelper.d(TAG, "onSessionEnded - session: " + castSession);
            if (castSession == mCastSession) {
                mCastSession = null;
            }
            unregisterRemoteMediaClientConf();
        }

        @Override
        public void onSessionResuming(CastSession castSession, String s) {
        }

        @Override
        public void onSessionResumed(CastSession castSession, boolean b) {
            LogHelper.d(TAG, "onSessionResumed - session: " + castSession);
            mCastSession = castSession;
            registerRemoteMediaClientConf();
        }

        @Override
        public void onSessionResumeFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionSuspended(CastSession castSession, int i) {
        }
    }

}
