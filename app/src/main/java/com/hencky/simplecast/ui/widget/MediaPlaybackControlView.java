package com.hencky.simplecast.ui.widget;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hencky.simplecast.R;
import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.utils.PreferencesHelper;

/**
 * A view to control media playback.
 */
public class MediaPlaybackControlView extends FrameLayout {
    private ImageButton mPlaybackButton;
    private ImageButton mReplayButton;
    private ImageButton mForwardButton;
    private ImageButton mMuteToggleButton;
    private ViewGroup mSeekbarContainer;

    private boolean mPlaying;
    private boolean mMuted;

    public interface Listener {
        void onPlayClicked();

        void onPauseClicked();

        void onForwardClicked();

        void onReplayClicked();

        void onStopClicked();

        void onMuteToggled(boolean muted);

        boolean hasSeekToPlaybackPositionControl();

        boolean hasSetMuteControl();
    }

    private Listener mListener;

    public MediaPlaybackControlView(@NonNull Context context) {
        this(context, null);
    }

    public MediaPlaybackControlView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MediaPlaybackControlView(@NonNull Context context, @Nullable AttributeSet attrs,
                                    int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public MediaPlaybackControlView(@NonNull Context context, @Nullable AttributeSet attrs,
                                    int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        try {
            mListener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement Listener interface.");
        }

        LayoutInflater.from(context).inflate(R.layout.media_playback_control_view, this);

        mSeekbarContainer = findViewById(R.id.seekbar_container);

        mPlaybackButton = findViewById(R.id.btn_play_pause);
        mPlaybackButton.setOnClickListener(v -> {
            if (mPlaying) {
                mListener.onPauseClicked();
            } else {
                mListener.onPlayClicked();
            }
        });

        mReplayButton = findViewById(R.id.btn_replay);
        int replayIcon = R.drawable.replay_30_36dp;
        String replayTitle = context.getString(R.string.label_replay_30);
        if (PreferencesHelper.getInstance(context).getBackwardTimeInSeconds() == 10) {
        replayIcon = R.drawable.replay_10_36dp;
            replayTitle = context.getString(R.string.label_replay_10);
        }
        mReplayButton.setImageResource(replayIcon);
        mReplayButton.setContentDescription(replayTitle);
        mReplayButton.setOnClickListener(v -> mListener.onReplayClicked());

        mForwardButton = findViewById(R.id.btn_forward);
        int forwardIcon = R.drawable.forward_30_36dp;
        String forwardTitle = context.getString(R.string.label_forward_30);
        if (PreferencesHelper.getInstance(context).getForwardTimeInSeconds() == 10) {
            forwardIcon = R.drawable.forward_10_36dp;
            forwardTitle = context.getString(R.string.label_forward_10);
        }
        mForwardButton.setImageResource(forwardIcon);
        mForwardButton.setContentDescription(forwardTitle);
        mForwardButton.setOnClickListener(v -> mListener.onForwardClicked());

        ImageButton stopButton = findViewById(R.id.btn_stop);
        stopButton.setOnClickListener(v -> mListener.onStopClicked());

        mMuteToggleButton = findViewById(R.id.btn_toggle_mute);
        mMuteToggleButton.setOnClickListener(v -> {
            mMuted = !mMuted;
            setMuteState(mMuted);
            mListener.onMuteToggled(mMuted);
        });

        if (!mListener.hasSeekToPlaybackPositionControl()) {
            //mReplayButton.setVisibility(View.GONE);
            //mForwardButton.setVisibility(View.GONE);
        }

        if (!mListener.hasSetMuteControl()) {
            mMuteToggleButton.setVisibility(View.GONE);
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.playing = mPlaying;
        ss.muted = mMuted;
        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        mPlaying = ss.playing;
        setPlaybackState(mPlaying);
        mMuted = ss.muted;
        setMuteState(mMuted);
    }

    /**
     * Sets the layout for a given media.
     */
    public void setMediaLayout(MediaItem mediaItem) {
        if (mediaItem instanceof ImageItem) {
            // Hide views that are not related to image playback.
            mSeekbarContainer.setVisibility(View.GONE);
            mPlaybackButton.setVisibility(View.GONE);
            mReplayButton.setVisibility(View.GONE);
            mForwardButton.setVisibility(View.GONE);
            mMuteToggleButton.setVisibility(View.GONE);
        } else if (mediaItem instanceof VideoItem) {
            mSeekbarContainer.setVisibility(View.VISIBLE);
            mPlaybackButton.setVisibility(View.VISIBLE);
            mReplayButton.setVisibility(View.VISIBLE);
            mForwardButton.setVisibility(View.VISIBLE);
            mMuteToggleButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Sets the playback state.
     *
     * @param playing {@code true} if the media is playing, otherwise {@code false}.
     */
    public void setPlaybackState(boolean playing) {
        mPlaying = playing;
        mPlaybackButton.setContentDescription(
                getResources().getString(mPlaying ? R.string.pause_playback : R.string
                        .start_playback));
        mPlaybackButton.setImageResource(mPlaying ?
                R.drawable.ic_pause_black_36dp : R.drawable.ic_play_arrow_black_36dp);
        mReplayButton.setEnabled(playing);
        mForwardButton.setEnabled(playing);
    }

    /**
     * Sets the mute state.
     *
     * @param muted {@code true} if the sound should be muted,
     *              {@code false} if the sound should be restored.
     */
    public void setMuteState(boolean muted) {
        mMuted = muted;
        mMuteToggleButton.setContentDescription(
                getResources().getString(muted ? R.string.muting_off : R.string.muting_on));
        mMuteToggleButton.setImageResource(muted ?
                R.drawable.ic_volume_up_black_36dp : R.drawable.ic_volume_off_black_36dp);
    }

    /**
     * Represents the saved state of {@link MediaPlaybackControlView}.
     */
    private static class SavedState extends BaseSavedState {
        boolean playing;
        boolean muted;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.playing = in.readByte() != 0;
            this.muted = in.readByte() != 0;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeByte(this.playing ? (byte) 1 : (byte) 0);
            out.writeByte(this.muted ? (byte) 1 : (byte) 0);
        }

        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }
}
