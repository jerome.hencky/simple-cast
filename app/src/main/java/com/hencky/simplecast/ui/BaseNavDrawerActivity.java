package com.hencky.simplecast.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.hencky.simplecast.R;
import com.hencky.simplecast.utils.LogHelper;

public class BaseNavDrawerActivity extends BaseActivity {

    private static final String TAG = LogHelper.makeLogTag(BaseNavDrawerActivity.class);

    private DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            openNavDrawer();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // If the drawer is open, back will close it
        if (isNavDrawerOpen()) {
            closeNavDrawer();
            return;
        }
        super.onBackPressed();
    }

    /**
     * Initializes the navigation drawer.
     * Overriding activities should call this
     * in their {@link #onCreate(android.os.Bundle)}
     * after {@link #setContentView(int)}.
     */
    public void setupNavDrawer() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView = mDrawerLayout.findViewById(R.id.nav_view);
        mNavigationView.setCheckedItem(R.id.nav_media_lib);
        mNavigationView.setNavigationItemSelectedListener(item -> {
            LogHelper.d(TAG, item.getItemId());
            onNavItemClick(item.getItemId());
            return false;
        });
    }

    /**
     * Returns true if the navigation drawer is open. Otherwise false.
     */
    private boolean isNavDrawerOpen() {
        return mDrawerLayout.isDrawerOpen(mNavigationView);
    }

    /**
     * Closes the navigation drawer.
     */
    private void closeNavDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    /**
     * Opens the navigation drawer.
     */
    private void openNavDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }


    private void onNavItemClick(int itemId) {
        Class activityClass = null;
        switch (itemId) {
            case R.id.nav_settings: {
                activityClass = SettingsActivity.class;
                break;
            }
            case R.id.nav_about: {
                activityClass = AboutActivity.class;

                break;
            }
        }
        if (activityClass == null) {
            closeNavDrawer();
            return;
        }
        closeNavDrawer();
        final Intent launchIntent = new Intent(this, activityClass);
        ActivityCompat.startActivity(this, launchIntent, null);
    }

}
