package com.hencky.simplecast.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hencky.simplecast.R;
import com.hencky.simplecast.adapter.RecyclerViewCursorAdapter;
import com.hencky.simplecast.model.DisplayMode;
import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.MediaType;
import com.hencky.simplecast.model.SortMode;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.ui.widget.SpacingItemDecoration;
import com.hencky.simplecast.utils.DateTimeUtils;
import com.hencky.simplecast.utils.DrawableUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaUtils;

import java.io.File;
import java.lang.ref.WeakReference;

public class MediaItemsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = LogHelper.makeLogTag(MediaItemsFragment.class);

    /**
     * Identifies an Image Loader being used in this component.
     */
    private static final int IMAGE_LOADER = 0x01;

    /**
     * Identifies a Video Loader being used in this component.
     */
    private static final int VIDEO_LOADER = 0x02;

    private static String KEY_MEDIA_TYPE = "mediaType";

    /**
     * Represents a layout manager type.
     */
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    private Context mContext;
    private Listener mListener;
    private int mGridSpacing;
    private MediaType mMediaType;
    private ViewSwitcher mViewSwitcher;
    private TextView mPlaceholder;
    private RecyclerView mRecyclerView;
    private ImageItemAdapter mImageItemAdapter;
    private VideoItemAdapter mVideoItemAdapter;
    private LayoutManagerType mLayoutManagerType;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.ItemDecoration mDecorator;

    public interface Listener {
        DisplayMode getDisplayMode();

        SortMode getImagesSortMode();

        SortMode getVideosSortMode();

        void onClickMediaItem(MediaItem mediaItem);

        boolean hasStoragePermissionsGranted();

        String getMediaLibFolder();
    }

    private final BroadcastReceiver mUIActionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);
            if (intent != null) {
                String action = intent.getAction();
                if (MediaBrowserActivity.ACTION_DISPLAYMODE_CHANGED.equals(action)) {
                    onDisplayModeChanged(mListener.getDisplayMode());
                } else if (MediaBrowserActivity.ACTION_SORTMODE_CHANGED.equals(action)) {
                    onSortModeChanged();
                }
            }
        }
    };

    public static MediaItemsFragment newInstance(MediaType mediaType) {
        MediaItemsFragment fragment = new MediaItemsFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_MEDIA_TYPE, mediaType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        try {
            mListener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement Listener interface.");
        }
    }

    @Override
    public void onDetach() {
        mContext = null;
        mListener = null;
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGridSpacing = getResources().getDimensionPixelSize(R.dimen.grid_spacing);

        this.mMediaType = (MediaType) getArguments().getSerializable(KEY_MEDIA_TYPE);
        LogHelper.d(TAG, "arg mMediaType: " + mMediaType);
        if (MediaType.IMAGE.equals(mMediaType)) {
            mImageItemAdapter = new ImageItemAdapter(mContext);
        } else if (MediaType.VIDEO.equals(mMediaType)) {
            mVideoItemAdapter = new VideoItemAdapter(mContext);
        }

        mLayoutManager = new LinearLayoutManager(mContext);

        registerBroadcastReceivers();
    }

    @Override
    public void onDestroy() {
        unregisterBroadcastReceivers();

        try {
            LoaderManager.getInstance(this).destroyLoader(IMAGE_LOADER);
        } catch (Throwable localThrowable) {
            LogHelper.e(TAG, localThrowable, "An error occurred when destroying loader #" +
                    IMAGE_LOADER);
        }

        try {
            LoaderManager.getInstance(this).destroyLoader(VIDEO_LOADER);
        } catch (Throwable localThrowable) {
            LogHelper.e(TAG, localThrowable, "An error occurred when destroying loader #" +
                    VIDEO_LOADER);
        }

        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        restartLoader();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_media_items, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mViewSwitcher = view.findViewById(R.id.view_switcher);
        mPlaceholder = view.findViewById(R.id.placeholder);
        mRecyclerView = view.findViewById(R.id.media_items);

        int placeholderStringResId = R.string.no_media_found;
        int placeholderDrawableResId = R.drawable.ic_image_placeholder_104dp;
        if (MediaType.IMAGE.equals(mMediaType)) {
            placeholderStringResId = R.string.no_image_found;
        } else if (MediaType.VIDEO.equals(mMediaType)) {
            placeholderStringResId = R.string.no_video_found;
            placeholderDrawableResId = R.drawable.ic_movie_placeholder_104dp;
        }
        mPlaceholder.setText(placeholderStringResId);
        DrawableUtils.setCompoundDrawableTop(
                mContext, mPlaceholder, placeholderDrawableResId, 0);

        if (DisplayMode.LIST.equals(mListener.getDisplayMode())) {
            mLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        } else if (DisplayMode.GRID.equals(mListener.getDisplayMode())) {
            mLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
        }

        setupRecyclerView();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        SortMode sortMode = null;
        if (MediaType.IMAGE.equals(mMediaType)) {
            sortMode = mListener.getImagesSortMode();
        } else if (MediaType.VIDEO.equals(mMediaType)) {
            sortMode = mListener.getVideosSortMode();
        }
        return MediaUtils.newCursorLoaderForMediaItems(
                mContext,
                sortMode,
                mListener.getMediaLibFolder(),
                mMediaType);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        if (data.getCount() == 0) {
            showPlaceholder();
        } else {
            if (IMAGE_LOADER == loader.getId()) {
                mImageItemAdapter.swapCursor(data);
            } else if (VIDEO_LOADER == loader.getId()) {
                mVideoItemAdapter.swapCursor(data);
            }
            showRecyclerView();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        if (IMAGE_LOADER == loader.getId()) {
            mImageItemAdapter.swapCursor(null);
        } else if (VIDEO_LOADER == loader.getId()) {
            mVideoItemAdapter.swapCursor(null);
        }
    }

    /**
     * Called when the display mode has changed.
     *
     * @param newDisplayMode the new display mode
     */
    private void onDisplayModeChanged(DisplayMode newDisplayMode) {
        if (DisplayMode.LIST.equals(newDisplayMode)) {
            mLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        } else if (DisplayMode.GRID.equals(newDisplayMode)) {
            mLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
        }
        setupRecyclerView();
        restartLoader();
        mRecyclerView.scrollToPosition(0);
    }

    /**
     * Called when the sort mode has changed.
     */
    private void onSortModeChanged() {
        restartLoader();
        mRecyclerView.scrollToPosition(0);
    }

    /**
     * Starts a new or restarts an existing loader.
     */
    private void restartLoader() {
        if (mListener.hasStoragePermissionsGranted()) {
            if (MediaType.IMAGE.equals(mMediaType)) {
                LoaderManager.getInstance((FragmentActivity) mContext)
                        .restartLoader(IMAGE_LOADER, null, MediaItemsFragment.this);
            } else if (MediaType.VIDEO.equals(mMediaType)) {
                LoaderManager.getInstance((FragmentActivity) mContext)
                        .restartLoader(VIDEO_LOADER, null, MediaItemsFragment.this);
            }
        }
    }

    /**
     * Shows the placeholder.
     */
    private void showPlaceholder() {
        if (mViewSwitcher.getCurrentView().getId() == mRecyclerView.getId()) {
            mViewSwitcher.showPrevious();
        }
    }

    /**
     * Shows the {@link RecyclerView}.
     */
    private void showRecyclerView() {
        if (mViewSwitcher.getCurrentView().getId() == mPlaceholder.getId()) {
            mViewSwitcher.showNext();
        }
    }

    /**
     * Sets up the {@link RecyclerView}.
     */
    private void setupRecyclerView() {
        setRecyclerViewLayoutManager(mLayoutManagerType);
        addDecoratorToRecyclerView(mLayoutManagerType);
        if (MediaType.IMAGE.equals(mMediaType)) {
            mRecyclerView.setAdapter(mImageItemAdapter);
        } else if (MediaType.VIDEO.equals(mMediaType)) {
            mRecyclerView.setAdapter(mVideoItemAdapter);
        }
    }

    /**
     * Sets the RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    private void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        final int availableWidth = getResources().getDisplayMetrics().widthPixels;
        final int requestedItemWidth = getResources().getDimensionPixelSize(
                R.dimen.grid_item_width);
        final int spanCount = availableWidth / (requestedItemWidth + mGridSpacing);

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(mContext, spanCount);
                mLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(mContext);
                mLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                break;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    /**
     * Adds an item decoration to a RecyclerView for a given type of layout manager.
     *
     * @param layoutManagerType the type of layout manager.
     */
    private void addDecoratorToRecyclerView(LayoutManagerType layoutManagerType) {
        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mDecorator = new SpacingItemDecoration(mGridSpacing, true);
                break;
            case LINEAR_LAYOUT_MANAGER:
                mDecorator = new DividerItemDecoration(mContext,
                        DividerItemDecoration.VERTICAL);
                break;
            default:
                break;
        }
        // Do not accumulate item decorations.
        for (int i = 0; i < mRecyclerView.getItemDecorationCount(); i++) {
            mRecyclerView.removeItemDecorationAt(i);
        }
        mRecyclerView.addItemDecoration(mDecorator);
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter intentFilter = new IntentFilter(
                MediaBrowserActivity.ACTION_DISPLAYMODE_CHANGED);
        intentFilter.addAction(MediaBrowserActivity.ACTION_SORTMODE_CHANGED);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mUIActionReceiver,
                intentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mUIActionReceiver);
    }

    /**
     * Represents the ViewHolder for a {@link MediaItem}.
     */
    private class MediaItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private MediaItem mMediaItem;
        private final TextView mItemTitle;
        private final TextView mItemDateModified;
        private final ImageView mItemThumbnail;

        private MediaItemHolder(@NonNull View itemView) {
            super(itemView);
            mItemTitle = itemView.findViewById(R.id.item_title);
            mItemDateModified = itemView.findViewById(R.id.item_date_modified);
            mItemThumbnail = itemView.findViewById(R.id.item_thumbnail);
            itemView.setOnClickListener(this);
        }

        /**
         * Binds {@link MediaItem} to UI.
         *
         * @param mediaItem the MediaItem
         */
        private void bindItem(MediaItem mediaItem) {
            mMediaItem = mediaItem;
            mItemTitle.setText(mMediaItem.getTitle());
            mItemDateModified.setText(DateTimeUtils.toDateTimeString(mMediaItem.getDateModified()));


            int placeholderDrawableResId = R.drawable.ic_image_placeholder_48dp;
            if (mMediaItem instanceof VideoItem) {
                placeholderDrawableResId = R.drawable.ic_movie_placeholder_48dp;
            }
            Glide.with(mContext)
                    .load(Uri.fromFile(new File(mMediaItem.getData())))
                    .centerCrop()
                    .placeholder(placeholderDrawableResId)
                    .into(mItemThumbnail);
        }

        @Override
        public void onClick(View view) {
            mListener.onClickMediaItem(mMediaItem);
        }
    }

    /**
     * Represents the ViewHolder for a {@link VideoItem}.
     */
    private class VideoItemHolder extends MediaItemHolder {
        private final TextView mItemDuration;

        private VideoItemHolder(@NonNull View itemView) {
            super(itemView);
            mItemDuration = itemView.findViewById(R.id.item_duration);
        }

        /**
         * Binds {@link VideoItem} to UI.
         *
         * @param videoItem the VideoItem
         */
        private void bindItem(VideoItem videoItem) {
            super.bindItem(videoItem);
            mItemDuration.setText(DateTimeUtils.toDurationString(videoItem.getDuration()));
        }
    }

    /**
     * An adapter that provides views to a RecyclerView with data from an {@link ImageItem}.
     */
    private class ImageItemAdapter extends RecyclerViewCursorAdapter<MediaItemHolder> {
        private final WeakReference<Context> mWeakContext;

        private ImageItemAdapter(Context context) {
            mWeakContext = new WeakReference<>(context);
        }

        private Context getContext() {
            return mWeakContext.get();
        }

        @Override
        public void onBindViewHolder(MediaItemHolder holder, Cursor cursor) {
            final ImageItem imageItem = MediaUtils.getImageItem(getItem(cursor.getPosition()));
            if (imageItem != null) {
                LogHelper.d(TAG, "onBindViewHolder - " + imageItem.toString());
                holder.bindItem(imageItem);
            }
        }

        @NonNull
        @Override
        public MediaItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v;
            if (DisplayMode.GRID.equals(mListener.getDisplayMode())) {
                v = LayoutInflater.from(getContext()).inflate(R.layout.grid_image_item, parent,
                        false);
            } else if (DisplayMode.LIST.equals(mListener.getDisplayMode())) {
                v = LayoutInflater.from(getContext()).inflate(R.layout.list_image_item, parent,
                        false);
            } else {
                v = LayoutInflater.from(getContext()).inflate(R.layout.list_image_item, parent,
                        false);
            }
            return new MediaItemHolder(v);
        }
    }

    /**
     * An adapter that provides views to a RecyclerView with data from a {@link VideoItem}.
     */
    private class VideoItemAdapter extends RecyclerViewCursorAdapter<VideoItemHolder> {
        private final WeakReference<Context> mWeakContext;

        private VideoItemAdapter(Context context) {
            mWeakContext = new WeakReference<>(context);
        }

        private Context getContext() {
            return mWeakContext.get();
        }

        @Override
        public void onBindViewHolder(VideoItemHolder holder, Cursor cursor) {
            final VideoItem item = MediaUtils.getVideoItem(getItem(cursor.getPosition()));
            if (item != null) {
                LogHelper.d(TAG, "onBindViewHolder - " + item.toString());
                holder.bindItem(item);
            }
        }

        @NonNull
        @Override
        public VideoItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v;
            if (DisplayMode.GRID.equals(mListener.getDisplayMode())) {
                v = LayoutInflater.from(getContext()).inflate(R.layout.grid_video_item, parent,
                        false);
            } else if (DisplayMode.LIST.equals(mListener.getDisplayMode())) {
                v = LayoutInflater.from(getContext()).inflate(R.layout.list_video_item, parent,
                        false);
            } else {
                v = LayoutInflater.from(getContext()).inflate(R.layout.list_video_item, parent,
                        false);
            }
            return new VideoItemHolder(v);
        }
    }
}
