package com.hencky.simplecast.ui;


import android.content.Context;
import android.os.Bundle;
import android.os.Environment;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.hencky.simplecast.R;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.PreferencesHelper;
import com.obsez.android.lib.filechooser.ChooserDialog;

public class SettingsFragment extends PreferenceFragmentCompat {
    private static final String TAG = LogHelper.makeLogTag(SettingsFragment.class);

    private Context mContext;
    private Preference mMediaLibFolderPref;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        mContext = null;
        super.onDetach();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.pref_general);

        mMediaLibFolderPref = findPreference(
                PreferencesHelper.PREF_KEY_MEDIA_LIB_FOLDER);
        mMediaLibFolderPref.setDefaultValue(Environment.getExternalStorageDirectory().getPath());
        String mediaLibFolder = PreferencesHelper.getInstance(mContext).getMediaLib_Folder();
        mMediaLibFolderPref.setSummary(mediaLibFolder);
        mMediaLibFolderPref.setOnPreferenceClickListener(preference -> {
            new ChooserDialog(mContext)
                    .withFilter(true, false)
                    .withStartFile(mediaLibFolder)
                    .displayPath(true)
                    .withResources(R.string.dialog_title_pick_folder, android.R.string.ok,
                            android.R.string.cancel)
                    .withChosenListener((path, file) -> {
                        LogHelper.d(TAG, "Chosen directory: " + path);
                        PreferencesHelper.getInstance(mContext).setMediaLibFolder(path);
                        mMediaLibFolderPref.setSummary(path);
                    })
                    .build()
                    .show();
            return true;
        });

        bindPreferenceSummaryToValue(findPreference(PreferencesHelper.PREF_KEY_DISPLAY_MODE));
        bindPreferenceSummaryToValue(findPreference(PreferencesHelper.PREF_KEY_IMAGES_SORT_MODE));
        bindPreferenceSummaryToValue(findPreference(PreferencesHelper.PREF_KEY_VIDEOS_SORT_MODE));
        bindPreferenceSummaryToValue(findPreference(PreferencesHelper.PREF_KEY_BACKWARD_TIME));
        bindPreferenceSummaryToValue(findPreference(PreferencesHelper.PREF_KEY_FORWARD_TIME));
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private final Preference.OnPreferenceChangeListener mOnPreferenceChangeListener =
            (preference, value) -> {
                String stringValue = value.toString();
                LogHelper.d(TAG, "onPreferenceChange - stringValue: " + stringValue);

                if (PreferencesHelper.PREF_KEY_DISPLAY_MODE.equals(preference.getKey())) {
                    // Look up the correct display value in the preference's 'entries' list.
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);

                    CharSequence displayMode = listPreference.getEntries()[index];

                    // Set the summary to reflect the new value.
                    preference.setSummary(displayMode);

                    LogHelper.d(TAG, "onPreferenceChange - displayMode: " + displayMode);

                    return true;
                } else if (PreferencesHelper.PREF_KEY_IMAGES_SORT_MODE.equals(
                        preference.getKey())) {
                    // Look up the correct display value in the preference's 'entries' list.
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);

                    CharSequence sortMode = listPreference.getEntries()[index];

                    // Set the summary to reflect the new value.
                    preference.setSummary(sortMode);

                    LogHelper.d(TAG, "onPreferenceChange - images sortMode: " + sortMode);

                    return true;
                } else if (PreferencesHelper.PREF_KEY_VIDEOS_SORT_MODE.equals(
                        preference.getKey())) {
                    // Look up the correct display value in the preference's 'entries' list.
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);

                    CharSequence sortMode = listPreference.getEntries()[index];

                    // Set the summary to reflect the new value.
                    preference.setSummary(sortMode);

                    LogHelper.d(TAG, "onPreferenceChange - videos sortMode: " + sortMode);

                    return true;
                } else if (PreferencesHelper.PREF_KEY_BACKWARD_TIME.equals(preference.getKey())) {
                    // Look up the correct display value in the preference's 'entries' list.
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);

                    CharSequence secondsBackward = listPreference.getEntries()[index];

                    // Set the summary to reflect the new value.
                    preference.setSummary(secondsBackward);

                    LogHelper.d(TAG, "onPreferenceChange - secondsBackward: " + secondsBackward);

                    return true;
                } else if (PreferencesHelper.PREF_KEY_FORWARD_TIME.equals(preference.getKey())) {
                    // Look up the correct display value in the preference's 'entries' list.
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);

                    CharSequence secondsForward = listPreference.getEntries()[index];

                    // Set the summary to reflect the new value.
                    preference.setSummary(secondsForward);

                    LogHelper.d(TAG, "onPreferenceChange - secondsForward: " + secondsForward);

                    return true;
                }

                return false;
            };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #mOnPreferenceChangeListener
     */
    private void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(mOnPreferenceChangeListener);

        // Trigger the listener immediately with the preference's current value.
        mOnPreferenceChangeListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }
}
