package com.hencky.simplecast.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.core.view.MenuItemCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.mediarouter.app.MediaRouteActionProvider;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.Side;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.ui.widget.MediaPlaybackControlView;
import com.hencky.simplecast.utils.DateTimeUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;
import com.hencky.simplecast.utils.PreferencesHelper;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;

/**
 * An abstract class that represents an expanded controller Activity.
 * Subclasses must implement {@link #buildMediaRouteSelector()},
 * and {@link #buildMediaRouterCallback()}.
 */
public abstract class ExpandedControllerActivity extends BaseActivity
        implements MediaPlaybackControlView.Listener {

    private static final String TAG = LogHelper.makeLogTag(ExpandedControllerActivity.class);

    private GestureDetectorCompat mDetector;
    private MediaPlaybackControlView mMediaPlaybackControlView;
    private FrameLayout mForeground;
    private ProgressBar mLoadingIndicator;
    private DiscreteSeekBar mSeekBar;
    private TextView mElapsedTimeView;
    private TextView mRemainingTimeView;
    private MediaRouter mMediaRouter;
    private MediaRouter.Callback mMediaRouterCallback;
    private MediaRouteSelector mMediaRouteSelector;
    private Device mSelectedDevice;
    private MediaItem mMediaItem;
    private long mPlaybackTimeSeconds;
    private boolean mMuted;
    private boolean mStartedTrackingTouch;

    private Side mSide = Side.NONE;
    private MediaPlayerState mMediaPlayerState = MediaPlayerState.NONE;

    /**
     * Broadcast receiver for tracking media events such as
     * changed media player state,
     * or obtained media player state,
     * or obtained mute state of the sound,
     * or obtained playback time.
     */
    private final BroadcastReceiver mMediaEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);

            if (intent == null)
                return;

            String action = intent.getAction();

            if (MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED.equals(action) ||
                    MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED.equals(action)) {
                mMediaPlayerState = (MediaPlayerState) intent.getSerializableExtra(
                        AppKeys.EXTRA_PLAYER_STATE);
                LogHelper.d(TAG, "Media player state: " + mMediaPlayerState);

                runOnUiThread(() -> {
                    if (MediaPlayerState.TRANSITIONING.equals(mMediaPlayerState) ||
                            MediaPlayerState.NONE.equals(mMediaPlayerState) ||
                            MediaPlayerState.UNKNOWN.equals(mMediaPlayerState)) {
                        mLoadingIndicator.setVisibility(View.VISIBLE);
                    } else if (MediaPlaybackUtils.isMediaPlaying(mMediaPlayerState)) {
                        mSeekBar.setEnabled(true);
                    } else if (MediaPlayerState.PAUSED.equals(mMediaPlayerState)) {
                        mSeekBar.setEnabled(false);
                    } else if (MediaPlayerState.STOPPED.equals(mMediaPlayerState)) {
                        // Close this activity.
                        ExpandedControllerActivity.this.finish();
                    } else {
                        mLoadingIndicator.setVisibility(View.INVISIBLE);
                    }
                    setupPlaybackControlView();
                });
            } else if (MediaPlaybackUtils.ACTION_MUTE_STATE_OBTAINED.equals(action)) {
                mMuted = intent.getBooleanExtra(AppKeys.EXTRA_MUTED, false);
                LogHelper.d(TAG, "Obtained mute state: " + mMuted);
                runOnUiThread(() -> mMediaPlaybackControlView.setMuteState(mMuted));
            } else if (MediaPlaybackUtils.ACTION_PLAYBACK_TIME_OBTAINED.equals(action)) {
                mPlaybackTimeSeconds = intent.getLongExtra(AppKeys.EXTRA_PLAYBACK_TIME, 0);
                LogHelper.d(TAG, "Obtained playback time: " + mPlaybackTimeSeconds);
                runOnUiThread(() -> {
                    if (!mStartedTrackingTouch) {
                        mSeekBar.setProgress((int) mPlaybackTimeSeconds);
                        mElapsedTimeView.setText(DateTimeUtils.toDurationString(
                                mPlaybackTimeSeconds * 1000));
                        mRemainingTimeView.setText(DateTimeUtils.toDurationString(
                                getRemainingTimeMs()));
                    }
                });
            }
        }
    };

    /**
     * Monitors the status of a request while backdrop image loads.
     */
    private final RequestListener<Drawable> mBackdropLoadRequestListener =
            new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                            Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model,
                                               Target<Drawable> target, DataSource dataSource,
                                               boolean isFirstResource) {
                    mForeground.setForeground(ContextCompat.getDrawable(
                            ExpandedControllerActivity.this, R.drawable.bg_gradient_light));
                    mLoadingIndicator.setVisibility(View.INVISIBLE);
                    return false;
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSelectedDevice = getIntent().getParcelableExtra(AppKeys.EXTRA_DEVICE);
        LogHelper.d(TAG, mSelectedDevice);
        mMediaItem = getIntent().getParcelableExtra(AppKeys.EXTRA_MEDIA_ITEM);
        LogHelper.d(TAG, mMediaItem);

        // Get last configuration
        if (savedInstanceState != null) {
            mMediaPlayerState = (MediaPlayerState) savedInstanceState.getSerializable(
                    AppKeys.EXTRA_PLAYER_STATE);
            mPlaybackTimeSeconds = savedInstanceState.getLong(AppKeys.EXTRA_PLAYBACK_TIME);
            mMuted = savedInstanceState.getBoolean(AppKeys.EXTRA_MUTED, false);
        }

        setContentView(R.layout.activity_expanded_controller);

        setupActionBar();

        mDetector = new GestureDetectorCompat(this, new MyGestureListener());

        final View lsv = findViewById(R.id.left_side_view);
        lsv.setOnTouchListener((v, event) -> {
            mSide = Side.LEFT;
            LogHelper.d(TAG, "on on touch lsv: " + event.toString());
            return mDetector.onTouchEvent(event);
        });
        final View rsv = findViewById(R.id.right_side_view);
        rsv.setOnTouchListener(((v, event) -> {
            mSide = Side.RIGHT;
            LogHelper.d(TAG, "on on on touch rsv: " + event.toString());
            return mDetector.onTouchEvent(event);
        }));
        mMediaPlaybackControlView = findViewById(R.id.player_view);
        mElapsedTimeView = mMediaPlaybackControlView.findViewById(R.id.elapsed_time);
        mRemainingTimeView = mMediaPlaybackControlView.findViewById(R.id.remaining_time);
        mSeekBar = mMediaPlaybackControlView.findViewById(R.id.seekbar);

        TextView mStatusView = findViewById(R.id.status_text);
        mForeground = findViewById(R.id.foreground);
        mLoadingIndicator = findViewById(R.id.loading_indicator);

        loadBackdrop();

        mMediaPlaybackControlView.setMediaLayout(mMediaItem);

        setupSeekbar();

        setupMediaRouter();

        registerBroadcastReceivers();

        if (mSelectedDevice != null)
            mStatusView.setText(String.format(getString(R.string.cast_casting_to_device), mSelectedDevice.getName()));

        mMediaPlaybackControlView.requestFocus();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback);
        MediaPlaybackUtils.requestMediaPlayerState(this);
        MediaPlaybackUtils.requestMuteState(this);
    }

    @Override
    public void onPause() {
        mMediaRouter.removeCallback(mMediaRouterCallback);
        MediaPlaybackUtils.onMediaControllerComponentPaused(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterBroadcastReceivers();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(AppKeys.EXTRA_PLAYER_STATE, mMediaPlayerState);
        outState.putLong(AppKeys.EXTRA_PLAYBACK_TIME, mPlaybackTimeSeconds);
        outState.putBoolean(AppKeys.EXTRA_MUTED, mMuted);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.expanded_controller_menu, menu);
        MediaRouteActionProvider actionProvider =
                (MediaRouteActionProvider) MenuItemCompat.getActionProvider(menu
                        .findItem(R.id.media_route_menu_item));
        actionProvider.setRouteSelector(mMediaRouteSelector);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onPlayClicked() {
        MediaPlaybackUtils.startPlayback(this);
    }

    @Override
    public void onPauseClicked() {
        MediaPlaybackUtils.pausePlayback(this);
    }

    @Override
    public void onForwardClicked() {
        if (MediaPlaybackUtils.isMediaPlaying(mMediaPlayerState)) {
            final int forwardTimeInSeconds = PreferencesHelper.getInstance(this).getForwardTimeInSeconds();
            final Toast toast = Toast.makeText(ExpandedControllerActivity.this, String.format(getString(R.string.time_shift_forward), forwardTimeInSeconds), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            final long newPlaybackPositionInSeconds = mPlaybackTimeSeconds + forwardTimeInSeconds;
            MediaPlaybackUtils.seekToPlaybackPosition(this, newPlaybackPositionInSeconds);
        } else {
            seekNotAvailable();
        }
    }

    private void seekNotAvailable() {
        Toast.makeText(
                ExpandedControllerActivity.this,
                R.string.seek_notavailable,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReplayClicked() {
        if (MediaPlaybackUtils.isMediaPlaying(mMediaPlayerState)) {
            final int backwardTimeInSeconds = PreferencesHelper.getInstance(this).getBackwardTimeInSeconds();
            Toast toast = Toast.makeText(
                    ExpandedControllerActivity.this,
                    String.format(getString(R.string.time_shift_backward), backwardTimeInSeconds),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            final long newPlaybackPositionInSeconds = mPlaybackTimeSeconds - backwardTimeInSeconds;
            MediaPlaybackUtils.seekToPlaybackPosition(this, newPlaybackPositionInSeconds);
        } else {
            seekNotAvailable();
        }
    }

    @Override
    public void onStopClicked() {
        MediaPlaybackUtils.stopPlayback(this);
    }

    @Override
    public void onMuteToggled(boolean muted) {
        mMuted = muted;
        MediaPlaybackUtils.setMute(this, mMuted);
    }

    /**
     * Gets the {@link MediaPlayerState}.
     *
     * @return the media player state
     */
    public MediaPlayerState getMediaPlayerState() {
        return mMediaPlayerState;
    }

    /**
     * Sets the {@link MediaPlayerState}.
     *
     * @param mediaPlayerState the media player state to set
     */
    public void setMediaPlayerState(MediaPlayerState mediaPlayerState) {
        this.mMediaPlayerState = mediaPlayerState;
    }

    /**
     * Gets the selected {@link Device}.
     *
     * @return the selected device that is the media receiver
     */
    public Device getSelectedDevice() {
        return mSelectedDevice;
    }

    /**
     * Sets the {@link MediaRouter.Callback}.
     * Setting the MediaRouter.Callback is required in the buildMediaRouterCallback() method.
     *
     * @param mediaRouterCallback the MediaRouter.Callback to set
     * @see #buildMediaRouterCallback()
     */
    public void setMediaRouterCallback(MediaRouter.Callback mediaRouterCallback) {
        this.mMediaRouterCallback = mediaRouterCallback;
    }

    /**
     * Sets the {@link MediaRouteSelector}.
     * etting the MediaRouteSelector is required in the buildMediaRouteSelector() method.
     *
     * @param mediaRouteSelector the MediaRouteSelector to set
     * @see #buildMediaRouteSelector()
     */
    public void setMediaRouteSelector(MediaRouteSelector mediaRouteSelector) {
        this.mMediaRouteSelector = mediaRouteSelector;
    }

    /**
     * Set the visibility of the loading indicator.
     *
     * @param visibility the int value that represents the visibility to set
     */
    public void setLoadingIndicatorVisibility(int visibility) {
        mLoadingIndicator.setVisibility(visibility);
    }

    /**
     * Sets up the {@link androidx.appcompat.app.ActionBar}.
     */
    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_expand_more_white_36dp);
        if (mMediaItem != null) {
            getSupportActionBar().setTitle(mMediaItem.getTitle());
        }
    }

    /**
     * Loads the backdrop.
     */
    private void loadBackdrop() {

        if (mMediaItem == null) return;
        View backdropView = findViewById(R.id.backdrop);
        int placeholderDrawableResId = R.drawable.ic_image_placeholder_104dp;
        if (mMediaItem instanceof VideoItem) {
            placeholderDrawableResId = R.drawable.ic_movie_placeholder_104dp;
        }
        Glide.with(this)
                .load(Uri.fromFile(new File(mMediaItem.getData())))
                .listener(mBackdropLoadRequestListener)
                .centerCrop()
                .placeholder(placeholderDrawableResId)
                .dontAnimate()
                .into((ImageView) backdropView);
    }

    /**
     * Calculates the remaining time in milliseconds from the media duration and the
     * current media playback time.
     *
     * @return the remaining time in milliseconds.
     */
    private long getRemainingTimeMs() {
        if (mMediaItem instanceof VideoItem) {
            VideoItem videoItem = (VideoItem) mMediaItem;
            return (videoItem.getDuration() - (mPlaybackTimeSeconds * 1000));
        }
        return 0;
    }

    /**
     * Sets up the {@link MediaPlaybackControlView}.
     */
    protected void setupPlaybackControlView() {
        if (MediaPlayerState.PLAYING.equals(mMediaPlayerState)) {
            mMediaPlaybackControlView.setPlaybackState(true);
        } else if (MediaPlayerState.SEEKED.equals(mMediaPlayerState)) {
            mMediaPlaybackControlView.setPlaybackState(true);
        } else {
            mMediaPlaybackControlView.setPlaybackState(false);
        }
        mMediaPlaybackControlView.setMuteState(mMuted);
    }

    /**
     * Sets up the Seekbar.
     */
    private void setupSeekbar() {
        if (mMediaItem instanceof VideoItem) {
            VideoItem videoItem = (VideoItem) mMediaItem;
            mSeekBar.setMax((int) (videoItem.getDuration() / 1000));
            mSeekBar.setProgress((int) mPlaybackTimeSeconds);
            mSeekBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
                @Override
                public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                }

                @Override
                public void onStartTrackingTouch(DiscreteSeekBar seekBar) {
                    mStartedTrackingTouch = true;
                }

                @Override
                public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                    long progress = (long) seekBar.getProgress();
                    if (mPlaybackTimeSeconds != progress) {
                        LogHelper.d(TAG, "playback time: " + mPlaybackTimeSeconds +
                                ", progress: " + progress);
                        MediaPlaybackUtils.seekToPlaybackPosition(
                                ExpandedControllerActivity.this, progress);
                    }
                    mStartedTrackingTouch = false;
                }
            });

            mSeekBar.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
                @Override
                public int transform(int value) {
                    return value;
                }

                @Override
                public String transformToString(int value) {
                    return DateTimeUtils.toDurationString(value * 1000);
                }

                @Override
                public boolean useStringTransform() {
                    return true;
                }
            });

            // Set times in Views
            mElapsedTimeView.setText(
                    DateTimeUtils.toDurationString(mPlaybackTimeSeconds * 1000));
            mRemainingTimeView.setText(
                    DateTimeUtils.toDurationString(getRemainingTimeMs()));
        }
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter mediaEventIntentFilter = new IntentFilter(
                MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED);
        mediaEventIntentFilter.addAction(MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED);
        mediaEventIntentFilter.addAction(MediaPlaybackUtils.ACTION_MUTE_STATE_OBTAINED);
        mediaEventIntentFilter.addAction(MediaPlaybackUtils.ACTION_PLAYBACK_TIME_OBTAINED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMediaEventReceiver,
                mediaEventIntentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaEventReceiver);
    }

    /**
     * Sets up a {@link MediaRouter} for device discovery.
     * Implementation of this method must build a {@link MediaRouteSelector}.
     *
     * @see #mMediaRouteSelector
     * Implementation of this method must build a {@link MediaRouter.Callback}
     * and set it.
     * @see #setMediaRouterCallback(MediaRouter.Callback)
     */
    private void setupMediaRouter() {
        mMediaRouter = MediaRouter.getInstance(this);
        buildMediaRouteSelector();
        buildMediaRouterCallback();
    }

    /**
     * Builds a media route selector that describes the capabilities of routes
     * that the application would like to discover and use.
     * Implementation of this method must build a {@link MediaRouteSelector}
     * and set it.
     *
     * @see #setMediaRouteSelector(MediaRouteSelector)
     */
    protected abstract void buildMediaRouteSelector();

    /**
     * Builds a MediaRouter callback for discovery events.
     * Implementation of this method must build a {@link MediaRouter.Callback}
     * and set it.
     * The default implementation {@link DefaultMediaRouterCallback} might meet your needs.
     *
     * @see #setMediaRouterCallback(MediaRouter.Callback)
     */
    protected abstract void buildMediaRouterCallback();

    @Override
    public abstract boolean hasSeekToPlaybackPositionControl();

    @Override
    public abstract boolean hasSetMuteControl();

    /**
     * Callback for receiving events about media routing changes.
     */
    protected class DefaultMediaRouterCallback extends MediaRouter.Callback {
        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo route, int reason) {
            LogHelper.d(TAG, "Unselected route: id=" + route.getId() + ", name=" +
                    route.getName() + ", reason=" + reason);
            if (MediaRouter.UNSELECT_REASON_STOPPED == reason) {
                // Case when the user pressed the stop casting button.
                if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
                    // Notify other application components (including this activity)
                    // about the change to STOPPED player state
                    // so they can update their state if needed.
                    MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                            ExpandedControllerActivity.this,
                            MediaPlayerState.STOPPED,
                            mSelectedDevice.getDeviceType());
                }
            }
            onRouteUnselected(router, route);
        }
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String TAG = "GESTURE";

        @Override
        public boolean onDown(MotionEvent e) {
            LogHelper.d(TAG, "onDown, " + e.toString());
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            LogHelper.d(TAG, "onDoubleTap, " + e.toString());
            return false;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            LogHelper.d(TAG, "onDoubleTapEvent, " + e.toString());
            if (mMediaItem instanceof VideoItem) {
                if (PreferencesHelper.getInstance(ExpandedControllerActivity.this).isDoubleTapGestureEnabled()) {
                    if (mSide == Side.LEFT) {
                        onReplayClicked();
                    } else if (mSide == Side.RIGHT) {
                        onForwardClicked();
                    }
                }
            }
            return true;
        }

    } // End MyGestureListener class


} // End ExpandedControllerActivity class
