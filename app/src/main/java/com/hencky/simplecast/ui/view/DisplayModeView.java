package com.hencky.simplecast.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.hencky.simplecast.model.DisplayMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DisplayModeView extends LinearLayout implements DisplayItemView.Callback {

    private Callback mCallback;
    private DisplayMode mDisplayMode = DisplayMode.GRID;

    public DisplayModeView(@NonNull Context context) {
        this(context, null);
    }

    public DisplayModeView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DisplayModeView(@NonNull Context context, @Nullable AttributeSet attrs,
                           int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public DisplayModeView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                           int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onDisplayModeSelected(DisplayMode displayMode) {
        // uncheck other children
        for (int i = 0; i < getChildCount(); i++) {
            DisplayItemView v = (DisplayItemView) getChildAt(i);
            String tag = (String) v.getTag();
            if (!displayMode.name().equals(tag)) {
                v.setChecked(false);
            }
        }

        if (mCallback != null) {
            if (mDisplayMode != displayMode) {
                mCallback.onDisplayModeSelected(displayMode);
                mDisplayMode = displayMode;
            }
        }
    }

    public void addChild(DisplayMode displayMode, boolean checked) {
        if (checked) {
            mDisplayMode = displayMode;
        }
        DisplayItemView v = new DisplayItemView(getContext());
        v.setTag(displayMode.name());
        v.setDisplayMode(displayMode);
        v.setChecked(checked);
        v.setCallback(this);
        addView(v, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f));
    }

    /**
     * @param callback to be called
     */
    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public interface Callback {
        /**
         * On display mode selected.
         *
         * @param displayMode the selected display mode
         */
        void onDisplayModeSelected(DisplayMode displayMode);
    }
}
