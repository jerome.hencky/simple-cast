package com.hencky.simplecast.ui.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.google.android.gms.cast.framework.Session;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.ui.MediaBrowserActivity;
import com.hencky.simplecast.utils.DrawableUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;

import java.io.File;

/**
 * An abstract class that represents a mini controller Fragment.
 * Subclasses must implement {@link #getStartActivityIntent()},
 * {@link #hasAffinityWithDeviceType(DeviceType)},
 * and {@link #hasAffinityWithSession(Session)}.
 */
public abstract class MiniControllerFragment extends Fragment {

    private static final String TAG = LogHelper.makeLogTag(MiniControllerFragment.class);

    public interface Listener {
        Device getSelectedDevice();

        MediaItem getSelectedMediaItem();
    }

    private Listener mListener;
    private Context mContext;
    private View mMiniControllerView;
    private TextView mTitleView;
    private ImageView mMiniControllerButton;
    private ProgressBar mLoadingIndicator;
    private ProgressBar mProgressBar;

    private long mPlaybackTimeSeconds = 0;
    private MediaPlayerState mMediaPlayerState = MediaPlayerState.NONE;

    /**
     * Broadcast receiver for tracking generic events.
     */
    private final BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                    "] Received broadcast intent: " + intent);
            if (intent != null) {
                String action = intent.getAction();
                if (MediaBrowserActivity.ACTION_MEDIA_ITEM_SELECTED.equals(action)) {
                    setLoadingIndicatorVisibility(View.VISIBLE);
                }
            }
        }
    };

    /**
     * Broadcast receiver for tracking media events such as
     * changed media player state,
     * or obtained media playback state,
     * or obtained playback time.
     */
    private final BroadcastReceiver mMediaEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                    "] Received broadcast intent: " + intent);

            if (intent == null)
                return;

            String action = intent.getAction();

            if (MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED.equals(action) ||
                    MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED.equals(action)) {
                mMediaPlayerState = (MediaPlayerState) intent.getSerializableExtra(
                        AppKeys.EXTRA_PLAYER_STATE);
                LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                        "] Media player state: " + mMediaPlayerState);
                final DeviceType deviceType = (DeviceType) intent.getSerializableExtra(
                        AppKeys.EXTRA_DEVICE_TYPE);
                LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                        "] device type: " + deviceType);
                if (!hasAffinityWithDeviceType(deviceType)) {
                    LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                            "] Ignoring media player state for device type " + deviceType.name());
                    return;
                }
                setupMiniController();
                setMiniControllerViewVisibility(View.VISIBLE);
                if (MediaPlayerState.PLAYING.equals(mMediaPlayerState)) {
                    setMiniControllerButtonVisibility(View.VISIBLE);
                    setControllerButtonState();
                    setLoadingIndicatorVisibility(View.INVISIBLE);
                } else if (MediaPlayerState.PAUSED.equals(mMediaPlayerState)) {
                    setMiniControllerButtonVisibility(View.VISIBLE);
                    setControllerButtonState();
                    setLoadingIndicatorVisibility(View.INVISIBLE);
                } else if (MediaPlayerState.NONE.equals(mMediaPlayerState) ||
                        MediaPlayerState.STOPPED.equals(mMediaPlayerState)) {
                    if (mListener.getSelectedMediaItem() instanceof VideoItem) {
                        mPlaybackTimeSeconds = 0;
                    }
                    resetMiniController();
                    setLoadingIndicatorVisibility(View.INVISIBLE);
                    setMiniControllerViewVisibility(View.GONE);
                } else if (MediaPlayerState.TRANSITIONING.equals(mMediaPlayerState)) {
                    setLoadingIndicatorVisibility(View.VISIBLE);
                }
            } else if (MediaPlaybackUtils.ACTION_PLAYBACK_TIME_OBTAINED.equals(action)) {
                mPlaybackTimeSeconds = intent.getLongExtra(AppKeys.EXTRA_PLAYBACK_TIME, 0);
                LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                        "] Obtained playback time: " + mPlaybackTimeSeconds);
                final DeviceType deviceType = (DeviceType) intent.getSerializableExtra(
                        AppKeys.EXTRA_DEVICE_TYPE);
                LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                        "] device type: " + deviceType);
                if (!hasAffinityWithDeviceType(deviceType)) {
                    LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                            "] Ignoring media playback time for device type " + deviceType.name());
                    return;
                }
                if (mListener.getSelectedMediaItem() instanceof VideoItem) {
                    mProgressBar.setProgress((int) mPlaybackTimeSeconds);
                }
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        try {
            mListener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement Listener interface.");
        }
    }

    @Override
    public void onDetach() {
        mContext = null;
        mListener = null;
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        registerBroadcastReceivers();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getSession() != null) {
            LogHelper.d(TAG, "[" + this.getId() + "] Active session: " +
                    getSession().getSessionId());
            if (hasAffinityWithSession(getSession())) {
                LogHelper.d(TAG, "[" + this.getId() + "] Requesting media player state.");
                MediaPlaybackUtils.requestMediaPlayerState(mContext);
            }
        }
    }

    @Override
    public void onPause() {
        if (getSession() != null) {
            LogHelper.d(TAG, "[" + this.getId() + "] Active session: " +
                    getSession().getSessionId());
            if (hasAffinityWithSession(getSession())) {
                LogHelper.d(TAG, "[" + this.getId() + "] Notifying component paused.");
                MediaPlaybackUtils.onMediaControllerComponentPaused(mContext);
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        unregisterBroadcastReceivers();
        super.onDestroy();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mMiniControllerView = inflater.inflate(R.layout.fragment_mini_controller, container,
                false);

        mMiniControllerView.setOnClickListener(view -> {
            if (mListener.getSelectedMediaItem() != null) {
                if (getSession() != null) {
                    Intent intent = getStartActivityIntent();
                    intent.putExtra(AppKeys.EXTRA_DEVICE, mListener.getSelectedDevice());
                    intent.putExtra(AppKeys.EXTRA_MEDIA_ITEM, mListener.getSelectedMediaItem());
                    startActivity(intent);
                }
            }
        });

        mTitleView = mMiniControllerView.findViewById(R.id.mini_controller_title_view);

        mLoadingIndicator = mMiniControllerView.findViewById(
                R.id.mini_controller_loading_indicator);

        mProgressBar = mMiniControllerView.findViewById(R.id.mini_controller_progress_bar);

        mMiniControllerButton = mMiniControllerView.findViewById(R.id.mini_controller_button);
        mMiniControllerButton.setOnClickListener(view -> {
            if (mListener.getSelectedMediaItem() instanceof ImageItem) {
                MediaPlaybackUtils.stopPlayback(mContext);
            } else if (mListener.getSelectedMediaItem() instanceof VideoItem) {
                if (MediaPlaybackUtils.isMediaPlaying(mMediaPlayerState)) {
                    MediaPlaybackUtils.pausePlayback(mContext);
                } else {
                    MediaPlaybackUtils.startPlayback(mContext);
                }
            }
        });

        return mMiniControllerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*
        if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
            setupMiniController();
            setControllerButtonState();
            setLoadingIndicatorVisibility(View.INVISIBLE);
            setMiniControllerButtonVisibility(View.VISIBLE);
            setMiniControllerViewVisibility(View.VISIBLE);
        } else {
            setMiniControllerViewVisibility(View.GONE);
        }
        */
    }

    /**
     * @return the {@link Context} associated with this base Fragment.
     */
    Context getBaseFragmentContext() {
        return mContext;
    }

    /**
     * @return the {@link MediaItem} which is playing.
     */
    protected MediaItem getMediaItem() {
        return mListener.getSelectedMediaItem();
    }

    /**
     * @return the current {@link MediaPlayerState}.
     */
    protected MediaPlayerState getMediaPlayerState() {
        return mMediaPlayerState;
    }

    /**
     * Sets the {@link MediaPlayerState}.
     *
     * @param mediaPlayerState the media player state to set
     */
    protected void setMediaPlayerState(MediaPlayerState mediaPlayerState) {
        this.mMediaPlayerState = mediaPlayerState;
    }

    /**
     * Sets the visibility of the loading indicator.
     *
     * @param visibility the visibility of the loading indicator to set
     */
    void setLoadingIndicatorVisibility(int visibility) {
        mLoadingIndicator.setVisibility(visibility);
    }

    /**
     * Sets the visibility of the mini controller view.
     *
     * @param visibility the visibility of the mini controller view to set
     */
    void setMiniControllerViewVisibility(int visibility) {
        LogHelper.d(TAG, "[" + MiniControllerFragment.this.getId() +
                "] Setting MiniController visibility: " + visibility);
        mMiniControllerView.setVisibility(visibility);
    }

    /**
     * Sets the visibility of the mini controller button.
     *
     * @param visibility the visibility of the mini controller button to set
     */
    void setMiniControllerButtonVisibility(int visibility) {
        mMiniControllerButton.setVisibility(visibility);
    }

    protected abstract Session getSession();

    /**
     * Classes that extends this abstract class must instantiate an Intent
     * and set the class of the expanded controller Activity to start in it.
     * The class of the activity to start
     * must extends {@link com.hencky.simplecast.ui.ExpandedControllerActivity}.
     *
     * @return the {@link Intent} that is used to start the expanded controller Activity.
     */
    protected abstract Intent getStartActivityIntent();

    /**
     * @param deviceType the {@link DeviceType} with which this mini controller has affinity or not.
     * @return {@code true} if his mini controller has affinity with the specified DeviceType,
     * otherwise {@code false}.
     */
    protected abstract boolean hasAffinityWithDeviceType(DeviceType deviceType);

    /**
     * @param session the {@link Session} with which this mini controller has affinity or not.
     * @return {@code true} if his mini controller has affinity with the specified Session,
     * otherwise {@code false}.
     */
    protected abstract boolean hasAffinityWithSession(Session session);


    /**
     * Sets up the mini controller view.
     */
    void setupMiniController() {
        LogHelper.d(TAG, "Set up MiniController");
        if (mListener.getSelectedMediaItem() == null)
            return;

        LogHelper.d(TAG, mListener.getSelectedMediaItem());

        mProgressBar.setVisibility(View.GONE);
        mProgressBar.setProgress(0);
        mProgressBar.setMax(0);
        if (mListener.getSelectedMediaItem() instanceof VideoItem) {
            VideoItem videoItem = (VideoItem) mListener.getSelectedMediaItem();
            mProgressBar.setMax((int) (videoItem.getDuration() / 1000));
            mProgressBar.setProgress((int) mPlaybackTimeSeconds);
            mProgressBar.setVisibility(View.VISIBLE);
        }
        setMediaTitle(mListener.getSelectedMediaItem().getTitle());
        loadThumbnail();
    }

    /**
     * Resets the mini controller view.
     */
    private void resetMiniController() {
        if (mListener.getSelectedMediaItem() == null)
            return;
        if (mListener.getSelectedMediaItem() instanceof VideoItem) {
            mProgressBar.setProgress(0);
            mProgressBar.setMax(0);
        }
        setMediaTitle(null);
        Glide.with(mContext)
                .load(R.drawable.cast_album_art_placeholder)
                .centerCrop()
                .into((ImageView) mMiniControllerView.findViewById(R.id.mini_controller_icon_view));
        setMiniControllerButtonVisibility(View.INVISIBLE);
    }

    /**
     * Sets the media title in UI.
     *
     * @param title The media title to set
     */
    private void setMediaTitle(String title) {
        mTitleView.setText(title);
    }

    /**
     * Loads thumbnail in UI.
     */
    private void loadThumbnail() {
        int placeholderDrawableResId = R.drawable.ic_image_placeholder_64dp;
        if (mListener.getSelectedMediaItem() instanceof VideoItem) {
            placeholderDrawableResId = R.drawable.ic_movie_placeholder_64dp;
        }
        Glide.with(mContext)
                .load(Uri.fromFile(new File(mListener.getSelectedMediaItem().getData())))
                .centerCrop()
                .placeholder(placeholderDrawableResId)
                .into((ImageView) mMiniControllerView.findViewById(
                        R.id.mini_controller_icon_view));
    }

    /**
     * Sets the state of the controller button.
     */
    void setControllerButtonState() {
        int miniControllerButtonDrawableResId = R.drawable.ic_close_black_36dp;
        int miniControllerButtonStringResId = R.string.controller_stop;

        if (mListener.getSelectedMediaItem() instanceof VideoItem) {
            if (MediaPlaybackUtils.isMediaPlaying(mMediaPlayerState)) {
                miniControllerButtonDrawableResId = R.drawable.ic_pause_black_36dp;
                miniControllerButtonStringResId = R.string.controller_pause;
            } else {
                miniControllerButtonDrawableResId = R.drawable.ic_play_arrow_black_36dp;
                miniControllerButtonStringResId = R.string.controller_play;
            }
        }
        mMiniControllerButton.setContentDescription(
                getResources().getString(miniControllerButtonStringResId));
        mMiniControllerButton.setImageDrawable(
                DrawableUtils.tintDrawable(mContext, miniControllerButtonDrawableResId,
                        R.color.mini_controller_button_color)
        );
    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter eventIntentFilter = new IntentFilter(
                MediaBrowserActivity.ACTION_MEDIA_ITEM_SELECTED);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(
                mEventReceiver, eventIntentFilter);

        IntentFilter mediaEventIntentFilter = new IntentFilter(
                MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED);
        mediaEventIntentFilter.addAction(MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_OBTAINED);
        mediaEventIntentFilter.addAction(MediaPlaybackUtils.ACTION_PLAYBACK_TIME_OBTAINED);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(
                mMediaEventReceiver, mediaEventIntentFilter);
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
                mEventReceiver);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
                mMediaEventReceiver);
    }

}
