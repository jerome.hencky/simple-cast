package com.hencky.simplecast.ui;


import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.mediarouter.app.MediaRouteActionProvider;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaRouter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.CastState;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.DisplayMode;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.MediaType;
import com.hencky.simplecast.model.SortMode;
import com.hencky.simplecast.model.SortOrder;
import com.hencky.simplecast.model.SortType;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.playback.Playback;
import com.hencky.simplecast.playback.UpnpPlayback;
import com.hencky.simplecast.server.MediaServerHelper;
import com.hencky.simplecast.ui.view.DisplayModeView;
import com.hencky.simplecast.ui.view.SortModeView;
import com.hencky.simplecast.ui.widget.MiniControllerFragment;
import com.hencky.simplecast.upnp.UpnpCoreHelper;
import com.hencky.simplecast.upnp.UpnpRouteProvider;
import com.hencky.simplecast.upnp.UpnpSession;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;
import com.hencky.simplecast.utils.ModelUtils;
import com.hencky.simplecast.utils.NetworkUtils;
import com.hencky.simplecast.utils.PermissionUtils;
import com.hencky.simplecast.utils.PreferencesHelper;
import com.hencky.simplecast.utils.ResoucesUtils;
import com.obsez.android.lib.filechooser.ChooserDialog;

import java.lang.ref.WeakReference;

/**
 * Represents a browser for media items.
 */
public class MediaBrowserActivity extends BaseNavDrawerActivity
        implements DisplayModeView.Callback, SortModeView.Callback, MediaItemsFragment.Listener,
        MiniControllerFragment.Listener {

    private static final String TAG = LogHelper.makeLogTag(MediaBrowserActivity.class);
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    /**
     * Actions
     */
    public static final String ACTION_DISPLAYMODE_CHANGED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_DISPLAYMODE_CHANGED";
    public static final String ACTION_SORTMODE_CHANGED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_SORTMODE_CHANGED";
    public static final String ACTION_MEDIA_ITEM_SELECTED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_MEDIA_ITEM_SELECTED";

    /**
     * REQUEST_EXTERNAL_STORAGE is an app-defined int constant.
     * The callback method gets the result of the request.
     */
    private static final int REQUEST_EXTERNAL_STORAGE = 0x01;

    /**
     * Permissions required by this application to access external storage.
     */
    private static final String[] PERMISSIONS_EXTERNAL_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    /**
     * REQUEST_ACCESS_LOCATION is an app-defined int constant.
     * The callback method gets the result of the request.
     */
    private static final int REQUEST_ACCESS_LOCATION = 0x02;

    /**
     * Permissions required by this application to access location.
     */
    private static final String[] PERMISSIONS_ACCESS_LOCATION = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private final SessionManagerListener<CastSession> mCastSessionManagerListener =
            new CastSessionManagerListenerImpl();
    private final SessionManagerListener<UpnpSession> mUpnpSessionManagerListener =
            new UpnpSessionManagerListenerImpl();

    private Dialog mApiAvailabilityDialog;
    private View mRootLayout;
    private AlertDialog mAccessExternalStoragePermissionsDialog;
    private AlertDialog mAccessLocationPermissionsDialog;
    private Snackbar mRouteSelectorSnackbar;
    private Snackbar mWifiConnectionSnackbar;
    private MediaItem mSelectedMediaItem;
    private DisplayMode mDisplayMode;
    private SortMode mImagesSortMode;
    private SortMode mVideosSortMode;
    private String mMediaLibFolder;
    private UpnpRouteProvider mUpnpRouteProvider;
    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;
    private MediaRouter.Callback mMediaRouterCallback;
    private IntroductoryOverlay mIntroductoryOverlay;
    private CastContext mCastContext;
    private CastStateListener mCastStateListener;
    private CastSession mCastSession;
    private UpnpSession mUpnpSession;
    private Device mSelectedDevice;
    private MenuItem mMediaRouteMenuItem;
    private ConnectivityManager mConnectivityManager;
    private ViewPager mViewPager;
    private ProgressBar mLoadingIndicator;
    private boolean mStoragePermissionsGranted;
    private boolean mDisplayModeChanged;
    private boolean mSortModeChanged;
    private boolean mChangingConfigurations;
    private MediaPlayerState mMediaPlayerState = MediaPlayerState.NONE;
    private MediaType mSelectedMediaType = MediaType.IMAGE;

    /**
     * Broadcast receiver for tracking various events.
     */
    private final BroadcastReceiver mEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);
            if (intent != null) {
                String action = intent.getAction();

                if (Playback.ACTION_SUPPORT_MIMETYPE.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(AppKeys
                                    .EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received SupportMimeType action invocation result: " +
                            result.name());
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        String message = intent.getStringExtra(
                                AppKeys.EXTRA_ACTION_INVOCATION_RESULT_MESSAGE);
                        Toast.makeText(context,
                                String.format(getString(R.string.upnp_support_mimetype_error),
                                        message), Toast.LENGTH_LONG).show();
                    }
                } else if (Playback.ACTION_SETUP.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received Setup action invocation result: " +
                            result.name());
                    mLoadingIndicator.setVisibility(View.GONE);
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        Toast.makeText(context,
                                R.string.setup_playback_error,
                                Toast.LENGTH_LONG).show();
                    }
                } else if (UpnpPlayback.ACTION_AVTRANSPORT_SETAVTRANSPORTURI.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received SetAVTransportURI action invocation result: " +
                            result.name());
                    mLoadingIndicator.setVisibility(View.GONE);
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        Toast.makeText(context,
                                R.string.setup_playback_error,
                                Toast.LENGTH_LONG).show();
                    }
                } else if (UpnpPlayback.ACTION_AVTRANSPORT_GETPOSITIONINFO.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received GetPositionInfo action invocation result: " +
                            result.name());
                    /*
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        Toast.makeText(context,
                                R.string.upnp_avtransport_getpositioninfo_error,
                                Toast.LENGTH_LONG).show();
                    }
                    */
                } else if (UpnpPlayback.ACTION_AVTRANSPORT_STOP.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received Stop action invocation result: " +
                            result.name());
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        Toast.makeText(context,
                                R.string.upnp_avtransport_stop_error,
                                Toast.LENGTH_LONG).show();
                    }
                } else if (UpnpPlayback.ACTION_AVTRANSPORT_PLAY.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received Play action invocation result: " +
                            result.name());
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        Toast.makeText(context,
                                R.string.upnp_avtransport_play_error,
                                Toast.LENGTH_LONG).show();
                    }
                } else if (UpnpPlayback.ACTION_AVTRANSPORT_PAUSE.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received Pause action invocation result: " +
                            result.name());
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        Toast.makeText(context,
                                R.string.upnp_avtransport_pause_error,
                                Toast.LENGTH_LONG).show();
                    }
                } else if (UpnpPlayback.ACTION_AVTRANSPORT_SEEK.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    LogHelper.d(TAG, "Received Seek action invocation result: " +
                            result.name());
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        Toast.makeText(context,
                                R.string.upnp_avtransport_seek_error,
                                Toast.LENGTH_LONG).show();
                    }
                } else if (Playback.ACTION_INIT.equals(action)) {
                    ActionInvocationResult result =
                            (ActionInvocationResult) intent.getSerializableExtra(
                                    AppKeys.EXTRA_ACTION_INVOCATION_RESULT);
                    if (ActionInvocationResult.FAILURE.equals(result)) {
                        Toast.makeText(context,
                                String.format(getString(R.string.upnp_init_connection_error),
                                        getSelectedDevice()),
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

    /**
     * Broadcast receiver for tracking media events such as
     * changed media player state.
     */
    private final BroadcastReceiver mMediaEventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogHelper.d(TAG, "Received broadcast intent: " + intent);
            MediaPlayerState mediaPlayerState = (MediaPlayerState) intent.getSerializableExtra(
                    AppKeys.EXTRA_PLAYER_STATE);
            if (mMediaPlayerState.equals(mediaPlayerState)) {
                LogHelper.d(TAG, "Ignoring identical change of media player state: "
                        + mediaPlayerState);
                return;
            } else {
                DeviceType deviceType = (DeviceType) intent.getSerializableExtra(
                        AppKeys.EXTRA_DEVICE_TYPE);
                if (DeviceType.CAST.equals(deviceType)) {
                    if (MediaPlayerState.STOPPED.equals(mediaPlayerState)) {
                        // Request media player state to update the MediaPlaybackService.
                        LogHelper.d(TAG, "Requesting media player state");
                        MediaPlaybackUtils.requestMediaPlayerState(MediaBrowserActivity.this);
                    }
                }
            }
            mMediaPlayerState = mediaPlayerState;
            LogHelper.d(TAG, "Changed media player state: " + mMediaPlayerState);
        }
    };

    /**
     * Network callback that is called to know whether we are connected to Wi-Fi.
     * If we are, we search the UPnP devices.
     */
    private final ConnectivityManager.NetworkCallback mNetworkCallback =
            new ConnectivityManager.NetworkCallback() {
                @Override
                public void onCapabilitiesChanged(Network network,
                                                  NetworkCapabilities networkCapabilities) {
                    LogHelper.d(TAG, networkCapabilities);
                    if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        LogHelper.d(TAG, "Connected to Wi-Fi network.");
                        onWiFiConnected();
                    } else {
                        LogHelper.d(TAG, "The active network is not Wi-Fi.");
                    }
                }
            };

    private FragmentPagerAdapter newFragmentPagerAdapter() {
        return new MediaItemsPagerAdapter(getSupportFragmentManager(), this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.media_lib));

        mDisplayMode = PreferencesHelper.getInstance(this).getDisplayMode();
        mImagesSortMode = PreferencesHelper.getInstance(this).getImagesSortMode();
        mVideosSortMode = PreferencesHelper.getInstance(this).getVideosSortMode();

        // Get last configuration
        if (savedInstanceState != null) {
            mDisplayMode = (DisplayMode) savedInstanceState.getSerializable(
                    AppKeys.EXTRA_DISPLAY_MODE);
            mImagesSortMode = savedInstanceState.getParcelable(AppKeys.EXTRA_IMAGES_SORT_MODE);
            mVideosSortMode = savedInstanceState.getParcelable(AppKeys.EXTRA_VIDEOS_SORT_MODE);
            mSelectedDevice = savedInstanceState.getParcelable(AppKeys.EXTRA_DEVICE);
            mSelectedMediaItem = savedInstanceState.getParcelable(AppKeys.EXTRA_MEDIA_ITEM);
            mMediaPlayerState = (MediaPlayerState) savedInstanceState.getSerializable(
                    AppKeys.EXTRA_PLAYER_STATE);
            mMediaLibFolder = savedInstanceState.getString(AppKeys.EXTRA_MEDIA_LIB_FOLDER);
        }

        if (checkPlayServices()) {
            setContentView(R.layout.activity_media_browser);
            setupNavDrawer();
            setupActionBar();

            mRootLayout = findViewById(R.id.root_layout);
            mLoadingIndicator = findViewById(R.id.loading_indicator);

            setupViewPager();

            checkPermissions();

            setupMediaRouter();


            mCastStateListener = newState -> {
                if (newState != CastState.NO_DEVICES_AVAILABLE) {
                    showIntroductoryOverlay();
                }
            };

            mCastContext = CastContext.getSharedInstance(this);

            registerBroadcastReceivers();

            mConnectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(
                    Context.CONNECTIVITY_SERVICE);

            checkWiFiConnectivity();
        }
    }

    /**
     * Sets up the ViewPager.
     */
    private void setupViewPager() {
        LogHelper.d(TAG, "setupViewPager");
        mViewPager = findViewById(R.id.pager);

        FragmentPagerAdapter pagerAdapter = newFragmentPagerAdapter();
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mSelectedMediaType = MediaType.IMAGE;
                } else if (position == 1) {
                    mSelectedMediaType = MediaType.VIDEO;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mMediaLibFolder == null) {
            mMediaLibFolder = PreferencesHelper.getInstance(this).getMediaLib_Folder();
        }

        if (checkPlayServices()) {
            LogHelper.d(TAG, "checkPlayServices: true");

            mCastContext.getSessionManager().addSessionManagerListener(mCastSessionManagerListener,
                    CastSession.class);
            mCastContext.getSessionManager().addSessionManagerListener(mUpnpSessionManagerListener,
                    UpnpSession.class);

            mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback);

            mCastContext.addCastStateListener(mCastStateListener);

            checkCurrentSession();

            UpnpCoreHelper.getInstance(this).resumeRegistry();
        } else {
            LogHelper.d(TAG, "checkPlayServices: false");

            if (mApiAvailabilityDialog != null) {
                mApiAvailabilityDialog.show();
            }
        }
    }

    @Override
    protected void onPause() {
        mChangingConfigurations = isChangingConfigurations();

        if (checkPlayServices()) {
            UpnpCoreHelper.getInstance(this).pauseRegistry();
            mMediaRouter.removeCallback(mMediaRouterCallback);
            mCastContext.removeCastStateListener(mCastStateListener);
            if (mCastSession != null) {
                mCastContext.getSessionManager().removeSessionManagerListener(
                        mCastSessionManagerListener, CastSession.class);
            } else if (mUpnpSession != null) {
                mCastContext.getSessionManager().removeSessionManagerListener(
                        mUpnpSessionManagerListener, UpnpSession.class);
            }
        }

        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mApiAvailabilityDialog != null)
            mApiAvailabilityDialog.dismiss();

        if (mAccessExternalStoragePermissionsDialog != null)
            mAccessExternalStoragePermissionsDialog.dismiss();

        if (mAccessLocationPermissionsDialog != null)
            mAccessLocationPermissionsDialog.dismiss();

        if (mRouteSelectorSnackbar != null)
            mRouteSelectorSnackbar.dismiss();

        if (mWifiConnectionSnackbar != null)
            mWifiConnectionSnackbar.dismiss();

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (checkPlayServices()) {
            mConnectivityManager.unregisterNetworkCallback(mNetworkCallback);

            unregisterBroadcastReceivers();

            if (!mChangingConfigurations) {
                //mUpnpRouteProvider.setCallback(null);
                mMediaRouter.removeProvider(mUpnpRouteProvider);
                mUpnpRouteProvider.release();
            }
        }

        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(AppKeys.EXTRA_DISPLAY_MODE, mDisplayMode);
        outState.putParcelable(AppKeys.EXTRA_IMAGES_SORT_MODE, mImagesSortMode);
        outState.putParcelable(AppKeys.EXTRA_VIDEOS_SORT_MODE, mVideosSortMode);
        outState.putParcelable(AppKeys.EXTRA_DEVICE, mSelectedDevice);
        outState.putParcelable(AppKeys.EXTRA_MEDIA_ITEM, mSelectedMediaItem);
        outState.putSerializable(AppKeys.EXTRA_PLAYER_STATE, mMediaPlayerState);
        outState.putString(AppKeys.EXTRA_MEDIA_LIB_FOLDER, mMediaLibFolder);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.media_browser_menu, menu);
        mMediaRouteMenuItem = menu.findItem(R.id.media_route_menu_item);
        MediaRouteActionProvider actionProvider =
                (MediaRouteActionProvider) MenuItemCompat.getActionProvider(mMediaRouteMenuItem);
        actionProvider.setRouteSelector(mMediaRouteSelector);
        showIntroductoryOverlay();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (R.id.display_options_menu_item == itemId) {

            createPopupWindowForDisplayOptions(findViewById(itemId));
            return true;
        } else if (R.id.media_lib_folder_menu_item == itemId) {
            showMediaLibFolderPicker();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Shows the media lib folder picker.
     */
    private void showMediaLibFolderPicker() {
        new ChooserDialog(this)
                .titleFollowsDir(true)
                .withFilter(true, false)
                .withStartFile(mMediaLibFolder)
                .displayPath(true)
                .withResources(R.string.dialog_title_pick_folder, android.R.string.ok,
                        android.R.string.cancel)
                .withChosenListener((path, file) -> {
                    LogHelper.d(TAG, "Chosen folder path: " + path);
                    mMediaLibFolder = path;
                    setupViewPager();
                })
                .build()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE:
                LogHelper.d(TAG, "Received response for external storage permissions request.");
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    LogHelper.d(TAG, "External Storage permissions granted.");
                    mStoragePermissionsGranted = true;
                } else {
                    if (grantResults.length != 1)
                        return;

                    int result = grantResults[0];
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        String permission = permissions[0];
                        if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(permission)) {
                            LogHelper.d(TAG, "Read External Storage permission denied.");
                            AlertDialog.Builder builder = new AlertDialog.Builder(this,
                                    R.style.AppAlertDialogTheme);
                            mAccessExternalStoragePermissionsDialog = builder.setTitle(
                                    R.string.dialog_permission_issue)
                                    .setMessage(R.string.no_read_external_storage_permission)
                                    .setPositiveButton(android.R.string.ok,
                                            (dialogInterface, i1) -> {
                                                // do nothing
                                            })
                                    .show();
                        }
                    }
                }
                break;
            case REQUEST_ACCESS_LOCATION:
                LogHelper.d(TAG, "Received response for access location permissions request.");
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    LogHelper.d(TAG, "Access Coarse/Fine Location permissions granted.");
                } else {
                    if (grantResults.length != 2)
                        return;

                    for (int i = 0; i < grantResults.length; i++) {
                        int result = grantResults[i];
                        if (result != PackageManager.PERMISSION_GRANTED) {
                            String permission = permissions[i];
                            if (Manifest.permission.ACCESS_COARSE_LOCATION.equals(permission)) {
                                LogHelper.d(TAG, "Access Coarse Location permission denied.");
                                AlertDialog.Builder builder = new AlertDialog.Builder(this,
                                        R.style.AppAlertDialogTheme);
                                mAccessLocationPermissionsDialog = builder.setTitle(
                                        R.string.dialog_permission_issue)
                                        .setMessage(R.string.no_access_coarse_location_permission)
                                        .setPositiveButton(android.R.string.ok,
                                                (dialogInterface, i1) -> {
                                                    // do nothing
                                                })
                                        .show();
                            } else if (Manifest.permission.ACCESS_FINE_LOCATION.equals(
                                    permission)) {
                                LogHelper.d(TAG, "Access Fine Location permission denied");
                                AlertDialog.Builder builder = new AlertDialog.Builder(this,
                                        R.style.AppAlertDialogTheme);
                                mAccessLocationPermissionsDialog = builder.setTitle(
                                        R.string.dialog_permission_issue)
                                        .setMessage(R.string.no_access_fine_location_permission)
                                        .setPositiveButton(android.R.string.ok,
                                                (dialogInterface, i1) -> {
                                                    // do nothing
                                                })
                                        .show();
                            }
                        }
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onDisplayModeSelected(DisplayMode displayMode) {
        mDisplayMode = displayMode;
        mDisplayModeChanged = true;
    }

    @Override
    public void onSortModeSelected(SortMode sortMode) {
        if (mViewPager.getCurrentItem() == 0) {
            mImagesSortMode = sortMode;
        } else if (mViewPager.getCurrentItem() == 1) {
            mVideosSortMode = sortMode;
        }
        mSortModeChanged = true;
    }

    @Override
    public Device getSelectedDevice() {
        if (mUpnpSession != null) {
            return mUpnpSession.getDevice();
        } else if (mCastSession != null) {
            final Device[] selectedDevice = {null};
            runOnUiThread(() ->
                    selectedDevice[0] = ModelUtils.toDevice(mCastSession.getCastDevice()));
            return selectedDevice[0];
        }
        return null;
    }

    @Override
    public MediaItem getSelectedMediaItem() {
        return mSelectedMediaItem;
    }

    /**
     * Checks the current session with a receiver application.
     * A session is an end-to-end connection
     * from a sender application to a receiver application.
     */
    private void checkCurrentSession() {
        Session session = CastContext.getSharedInstance(this).getSessionManager()
                .getCurrentSession();
        if (session != null) {
            if (session instanceof CastSession) {
                mCastSession = (CastSession) session;
            } else if (session instanceof UpnpSession) {
                mUpnpSession = (UpnpSession) session;
            }
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     *
     * @return {@code true} if the device has the Google Play Services, otherwise {@code false}.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                if (mApiAvailabilityDialog == null) {
                    mApiAvailabilityDialog = apiAvailability.getErrorDialog(this, resultCode,
                            PLAY_SERVICES_RESOLUTION_REQUEST);
                }
            } else {
                LogHelper.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Sets up the {@link androidx.appcompat.app.ActionBar}.
     */
    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_hamburger_menu_24dp);
        setSupportActionBar(toolbar);

    }

    /**
     * Registers the broadcast receivers.
     */
    private void registerBroadcastReceivers() {
        IntentFilter eventIntentFilter = new IntentFilter(
                UpnpPlayback.ACTION_AVTRANSPORT_SETAVTRANSPORTURI);
        eventIntentFilter.addAction(Playback.ACTION_INIT);
        eventIntentFilter.addAction(Playback.ACTION_SETUP);
        eventIntentFilter.addAction(UpnpPlayback.ACTION_AVTRANSPORT_GETPOSITIONINFO);
        eventIntentFilter.addAction(UpnpPlayback.ACTION_AVTRANSPORT_STOP);
        eventIntentFilter.addAction(UpnpPlayback.ACTION_AVTRANSPORT_PLAY);
        eventIntentFilter.addAction(UpnpPlayback.ACTION_AVTRANSPORT_PAUSE);
        eventIntentFilter.addAction(UpnpPlayback.ACTION_AVTRANSPORT_SEEK);
        eventIntentFilter.addAction(Playback.ACTION_SUPPORT_MIMETYPE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mEventReceiver,
                eventIntentFilter);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMediaEventReceiver,
                new IntentFilter(MediaPlaybackUtils.ACTION_MEDIA_PLAYER_STATE_CHANGED));
    }

    /**
     * Unregisters the broadcast receivers.
     */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mEventReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaEventReceiver);
    }

    /**
     * Called when the WiFi network is connected.
     */
    private void onWiFiConnected() {
        if (mWifiConnectionSnackbar != null) {
            mWifiConnectionSnackbar.dismiss();
        }
        Device selectedDevice = getSelectedDevice();
        if (selectedDevice == null) {
            searchUpnpDevices();
        } else {
            if (DeviceType.UPNP.equals(selectedDevice.getDeviceType())) {
                UpnpCoreHelper.getInstance(MediaBrowserActivity.this).initialize(selectedDevice);
            }
        }
    }

    /**
     * Checks the connectivity to the WiFi network.
     */
    private void checkWiFiConnectivity() {
        if (NetworkUtils.isWiFiNetworkConnected(this)) {
            LogHelper.d(TAG, "Connected to Wi-Fi network.");
            onWiFiConnected();
        } else {
            LogHelper.d(TAG, "The active network is not Wi-Fi.");
        }

        mConnectivityManager.registerNetworkCallback(
                new NetworkRequest.Builder()
                        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                        .build(),
                mNetworkCallback);
    }

    /**
     * Searches UPnP devices.
     */
    private void searchUpnpDevices() {
        LogHelper.d(TAG, "Searching UPnP control points.");
        UpnpCoreHelper.getInstance(this).searchControlPoints();
    }

    /**
     * Sets up a {@link MediaRouter} for device discovery.
     */
    private void setupMediaRouter() {
        mMediaRouter = MediaRouter.getInstance(this);
        // Get a MediaRouteProvider to provide routes to UPnP devices
        mUpnpRouteProvider = UpnpRouteProvider.getInstance(this);
        mMediaRouter.addProvider(mUpnpRouteProvider);
        // Create a MediaRouteSelector
        // with capability to discover and use routes to Cast and UPnP devices.
        mMediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(CastMediaControlIntent.categoryForCast(getResources()
                        .getString(R.string.cast_app_id)))
                .addControlCategory(UpnpRouteProvider.CATEGORY_UPNP)
                .build();
        // Create a MediaRouter callback for discovery events
        mMediaRouterCallback = new MediaRouterCallback();
    }

    /**
     * Checks the permissions.
     */
    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            LogHelper.d(TAG, "Read External Storage permission granted.");
            mStoragePermissionsGranted = true;
        } else {
            requestExternalStoragePermissions();
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission
                        .ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LogHelper.d(TAG, "Access Coarse and Fine Location permissions granted.");
        } else {
            requestAccessLocationPermissions();
        }
    }

    /**
     * Requests the Access Coarse and Fine Location permissions.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestAccessLocationPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            LogHelper.d(TAG,
                    "Displaying external storage permission rationale to provide additional context.");

            // Display a SnackBar with an explanation and a button to trigger the request.
            Snackbar.make(mRootLayout, R.string.permission_access_location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, view -> ActivityCompat
                            .requestPermissions(MediaBrowserActivity.this,
                                    PERMISSIONS_ACCESS_LOCATION, REQUEST_ACCESS_LOCATION))
                    .show();
        } else {
            // Permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_ACCESS_LOCATION,
                    REQUEST_ACCESS_LOCATION);
        }
    }

    /**
     * Requests the external storage access permissions.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestExternalStoragePermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            LogHelper.d(TAG,
                    "Displaying external storage permission rationale to provide additional context.");

            // Display a SnackBar with an explanation and a button to trigger the request.
            Snackbar.make(mRootLayout, R.string.permission_external_storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, view -> ActivityCompat
                            .requestPermissions(MediaBrowserActivity.this,
                                    PERMISSIONS_EXTERNAL_STORAGE, REQUEST_EXTERNAL_STORAGE))
                    .show();
        } else {
            // Permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_EXTERNAL_STORAGE,
                    REQUEST_EXTERNAL_STORAGE);
        }
    }

    /**
     * Creates a {@link PopupWindow} for display option(s).
     *
     * @param anchorView the anchor view where to attach the popup window
     */
    private void createPopupWindowForDisplayOptions(View anchorView) {
        if (anchorView != null) {
            View layout = getLayoutInflater().inflate(R.layout.popup_display_options,
                    findViewById(R.id.toolbar), false);

            boolean isDisplayModeList = mDisplayMode.name().equals(DisplayMode.LIST.name());
            boolean isDisplayModeGrid = mDisplayMode.name().equals(DisplayMode.GRID.name());

            DisplayModeView displayModeView = layout.findViewById(R.id.display_mode);
            displayModeView.addChild(DisplayMode.LIST, isDisplayModeList);
            displayModeView.addChild(DisplayMode.GRID, isDisplayModeGrid);
            displayModeView.setCallback(this);

            SortModeView sortModeView = layout.findViewById(R.id.sort_mode);
            if (MediaType.IMAGE.equals(mSelectedMediaType)) {
                boolean isSortTypeName = mImagesSortMode.getSortType().name().equals(
                        SortType.NAME.name());
                boolean isSortTypeDate = mImagesSortMode.getSortType().name().equals(
                        SortType.DATE.name());

                sortModeView.addChild(isSortTypeName ? mImagesSortMode :
                        new SortMode(SortType.NAME, SortOrder.ASC), isSortTypeName);
                sortModeView.addChild(isSortTypeDate ? mImagesSortMode :
                        new SortMode(SortType.DATE, SortOrder.ASC), isSortTypeDate);
            } else if (MediaType.VIDEO.equals(mSelectedMediaType)) {
                boolean isSortTypeName = mVideosSortMode.getSortType().name().equals(
                        SortType.NAME.name());
                boolean isSortTypeDate = mVideosSortMode.getSortType().name().equals(
                        SortType.DATE.name());
                boolean isSortTypeDuration = mVideosSortMode.getSortType().name().equals(
                        SortType.DURATION.name());

                sortModeView.addChild(isSortTypeName ? mVideosSortMode :
                        new SortMode(SortType.NAME, SortOrder.ASC), isSortTypeName);
                sortModeView.addChild(isSortTypeDate ? mVideosSortMode :
                        new SortMode(SortType.DATE, SortOrder.ASC), isSortTypeDate);
                sortModeView.addChild(isSortTypeDuration ? mVideosSortMode :
                        new SortMode(SortType.DURATION, SortOrder.ASC), isSortTypeDuration);
            }
            sortModeView.setCallback(this);

            PopupWindow popup = new PopupWindow(this);
            popup.setContentView(layout);
            popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
            popup.setWidth(ResoucesUtils.dipToPixels(this, 256));
            popup.setOutsideTouchable(true);
            popup.setFocusable(true);
            popup.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this,
                    R.color.background_material_light)));
            popup.setElevation(ResoucesUtils.dipToPixels(this, 6));
            popup.setOnDismissListener(() -> {
                String action = null;
                if (mDisplayModeChanged) {
                    action = ACTION_DISPLAYMODE_CHANGED;
                    mDisplayModeChanged = false;
                } else if (mSortModeChanged) {
                    action = ACTION_SORTMODE_CHANGED;
                    mSortModeChanged = false;
                }
                if (action != null) {
                    // Broadcasts an intent to all interested BroadcastReceivers.
                    Intent intent = new Intent();
                    intent.setAction(action);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }
            });
            popup.showAsDropDown(anchorView);
        } else {
            LogHelper.d(TAG, "anchor view is null");
        }
    }

    /**
     * Starts the media controller.
     */
    private void startMediaController() {
        Device selectedDevice = getSelectedDevice();
        LogHelper.d(TAG, "startMediaController - " + selectedDevice);
        if (selectedDevice == null)
            return;

        // Broadcasts an intent to all interested BroadcastReceivers
        // about the media item selection.
        Intent intent = new Intent();
        intent.setAction(MediaBrowserActivity.ACTION_MEDIA_ITEM_SELECTED);
        intent.putExtra(AppKeys.EXTRA_MEDIA_ITEM, mSelectedMediaItem);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        if (DeviceType.CAST.equals(selectedDevice.getDeviceType())) {
            // case when the selected device is a Cast receiver
            if (mCastSession == null) {
                Snackbar.make(mRootLayout,
                        String.format(getString(R.string.not_yet_connected_to_device),
                                selectedDevice.getName()), Snackbar.LENGTH_SHORT)
                        .show();
                return;
            }
        }

        mLoadingIndicator.setVisibility(View.VISIBLE);

        MediaPlaybackUtils.setupPlayback(this, selectedDevice, mSelectedMediaItem);
    }

    /**
     * Shows an overlay view that highlights the Cast button to the user.
     */
    private void showIntroductoryOverlay() {
        if (mIntroductoryOverlay != null) {
            mIntroductoryOverlay.remove();
        }
        if ((mMediaRouteMenuItem != null) && mMediaRouteMenuItem.isVisible()) {
            new Handler().post(() -> {
                mIntroductoryOverlay = new IntroductoryOverlay.Builder(
                        MediaBrowserActivity.this, mMediaRouteMenuItem)
                        .setTitleText(getString(R.string.introducing_cast))
                        .setOverlayColor(R.color.colorAccent)
                        .setSingleTime()
                        .setOnOverlayDismissedListener(
                                () -> mIntroductoryOverlay = null)
                        .build();
                mIntroductoryOverlay.show();
            });
        }
    }

    @Override
    public DisplayMode getDisplayMode() {
        return mDisplayMode;
    }

    @Override
    public SortMode getImagesSortMode() {
        return mImagesSortMode;
    }

    @Override
    public SortMode getVideosSortMode() {
        return mVideosSortMode;
    }

    @Override
    public void onClickMediaItem(MediaItem mediaItem) {
        if (mMediaRouter.getSelectedRoute().isDefault()) {
            LogHelper.d(TAG, "Selected route is default route: " +
                    mMediaRouter.getDefaultRoute());
            if (NetworkUtils.isWiFiNetworkConnected(MediaBrowserActivity.this)) {
                LogHelper.d(TAG,
                        "Wi-Fi network is enabled but no media receiver is selected.");
                mRouteSelectorSnackbar = Snackbar.make(mRootLayout,
                        R.string.dialog_choose_media_receiver,
                        Snackbar.LENGTH_SHORT);
                mRouteSelectorSnackbar.show();
                return;
            } else {
                LogHelper.d(TAG, "Wi-Fi network is not enabled.");
                mWifiConnectionSnackbar = Snackbar.make(mRootLayout,
                        R.string.dialog_wifi_connection,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.wifi_settings, v ->
                                MediaBrowserActivity.this.startActivity(
                                        new Intent(Settings.ACTION_WIFI_SETTINGS)));
                mWifiConnectionSnackbar.show();
                return;
            }
        } else {
            LogHelper.d(TAG, "Selected route is not default route: " +
                    mMediaRouter.getSelectedRoute());
        }

        mSelectedMediaItem = mediaItem;

        MediaServerHelper.getInstance(MediaBrowserActivity.this).startMediaServer();

        startMediaController();
    }

    @Override
    public boolean hasStoragePermissionsGranted() {
        return mStoragePermissionsGranted;
    }

    @Override
    public String getMediaLibFolder() {
        return mMediaLibFolder == null ?
                PreferencesHelper.getInstance(this).getMediaLib_Folder() : mMediaLibFolder;
    }

    /**
     * Callback for receiving events about media routing changes.
     */
    private class MediaRouterCallback extends MediaRouter.Callback {
        @Override
        public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo route) {
            LogHelper.d(TAG, "Selected route: id=" + route.getId() + ", name=" +
                    route.getName());
            Bundle extras = route.getExtras();
            if (route.supportsControlCategory(UpnpRouteProvider.CATEGORY_UPNP)) {
                // case when an UPnP receiver device is selected
                mSelectedDevice = extras.getParcelable(AppKeys.EXTRA_DEVICE);
                UpnpCoreHelper.getInstance(MediaBrowserActivity.this).initialize(mSelectedDevice);
            } else {
                CastDevice castDevice = CastDevice.getFromBundle(extras);
                mSelectedDevice = ModelUtils.toDevice(castDevice);
            }
            LogHelper.d(TAG, mSelectedDevice);
        }

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo route, int reason) {
            LogHelper.d(TAG, "Unselected route: id=" + route.getId() + ", name=" +
                    route.getName() + ", reason=" + reason);
            if (MediaRouter.UNSELECT_REASON_STOPPED == reason) {
                // Case when the user pressed the stop casting button.
                if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
                    if (route.supportsControlCategory(UpnpRouteProvider.CATEGORY_UPNP)) {
                        Bundle extras = route.getExtras();
                        Device unselectedDevice = extras.getParcelable(AppKeys.EXTRA_DEVICE);
                        if (mSelectedDevice.getIdentifier().equals(
                                unselectedDevice.getIdentifier())) {
                            if (mUpnpRouteProvider.providesDevice(mSelectedDevice)) {
                                MediaPlaybackUtils.stopPlayback(
                                        MediaBrowserActivity.this);
                            } else {
                                // Notify other application components (including this activity)
                                // about the change to STOPPED player state
                                // so they can update their state if needed.
                                MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                                        MediaBrowserActivity.this,
                                        MediaPlayerState.STOPPED,
                                        DeviceType.UPNP);
                            }
                        }
                    } else {
                        // Notify other application components (including this activity)
                        // about the change to STOPPED player state
                        // so they can update their state if needed.
                        MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                                MediaBrowserActivity.this,
                                MediaPlayerState.STOPPED,
                                DeviceType.CAST);
                    }
                }
            }
            onRouteUnselected(router, route);
        }

        @Override
        public void onRouteAdded(MediaRouter router, MediaRouter.RouteInfo route) {
            LogHelper.d(TAG, "Added route: id=" + route.getId() + ", name=" +
                    route.getName());
            Device selectedDevice = getSelectedDevice();
            if (route.supportsControlCategory(UpnpRouteProvider.CATEGORY_UPNP)) {
                // case when an UPnP device is added
                if (selectedDevice != null) {
                    Bundle extras = route.getExtras();
                    Device device = extras.getParcelable(AppKeys.EXTRA_DEVICE);
                    if (selectedDevice.equals(device)) {
                        LogHelper.d(TAG, "Found selected device " + device.getName());
                        LogHelper.d(TAG, "Selecting route " + route.getName() +
                                " to device " + device.getName());
                        mMediaRouter.selectRoute(route);
                    }
                }
            }
        }
    }

    /**
     * Monitors events of a {@link CastSession} instance.
     */
    private class CastSessionManagerListenerImpl implements SessionManagerListener<CastSession> {
        @Override
        public void onSessionStarting(CastSession castSession) {
        }

        @Override
        public void onSessionStarted(CastSession castSession, String sessionId) {
            mCastSession = castSession;
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionStartFailed(CastSession castSession, int error) {
        }

        @Override
        public void onSessionEnding(CastSession castSession) {
        }

        @Override
        public void onSessionEnded(CastSession castSession, int error) {
            if (castSession == mCastSession) {
                mCastSession = null;
            }
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionResuming(CastSession castSession, String sessionId) {
        }

        @Override
        public void onSessionResumed(CastSession castSession, boolean wasSuspended) {
            mCastSession = castSession;
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionResumeFailed(CastSession castSession, int error) {
        }

        @Override
        public void onSessionSuspended(CastSession castSession, int reason) {
        }
    }

    /**
     * Monitors events of a {@link UpnpSession} instance.
     */
    private class UpnpSessionManagerListenerImpl implements SessionManagerListener<UpnpSession> {
        @Override
        public void onSessionStarting(UpnpSession upnpSession) {
        }

        @Override
        public void onSessionStarted(UpnpSession upnpSession, String sessionId) {
            mUpnpSession = upnpSession;
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionStartFailed(UpnpSession upnpSession, int error) {
        }

        @Override
        public void onSessionEnding(UpnpSession upnpSession) {
        }

        @Override
        public void onSessionEnded(UpnpSession upnpSession, int error) {
            if (upnpSession == mUpnpSession) {
                mUpnpSession = null;
            }
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionResuming(UpnpSession upnpSession, String sessionId) {
        }

        @Override
        public void onSessionResumed(UpnpSession upnpSession, boolean wasSuspended) {
            mUpnpSession = upnpSession;
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionResumeFailed(UpnpSession upnpSession, int error) {
        }

        @Override
        public void onSessionSuspended(UpnpSession upnpSession, int reason) {
        }
    }

    /**
     * Class providing the adapter to populate pages inside of a {@link ViewPager}.
     * Each page is represented as a {@link MediaItemsFragment}.
     */
    private static class MediaItemsPagerAdapter extends FragmentPagerAdapter {
        private static final int NUM_ITEMS = 2;

        private final WeakReference<Context> mWeakContext;

        private MediaItemsPagerAdapter(FragmentManager fm, Context context) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            mWeakContext = new WeakReference<>(context);
        }

        @Override
        public Fragment getItem(int position) {
            LogHelper.d(TAG, "position: " + position);
            if (position == 1) {
                return MediaItemsFragment.newInstance(MediaType.VIDEO);
            }
            return MediaItemsFragment.newInstance(MediaType.IMAGE);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getContext().getString(R.string.images_pager_tab_title);
                case 1:
                    return getContext().getString(R.string.videos_pager_tab_title);
                default:
                    return null;
            }
        }

        private Context getContext() {
            return mWeakContext.get();
        }
    }
}
