package com.hencky.simplecast.ui;

import android.os.Bundle;
import android.view.View;

import androidx.mediarouter.media.MediaRouteSelector;

import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.hencky.simplecast.R;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.utils.CastMediaUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;

public class CastControllerActivity extends ExpandedControllerActivity {

    private static final String TAG = LogHelper.makeLogTag(CastControllerActivity.class);

    private CastContext mCastContext;

    private final SessionManagerListener<CastSession> mCastSessionManagerListener =
            new CastSessionManagerListenerImpl();

    /**
     * Callback for tracking player status changes.
     */
    private final RemoteMediaClient.Callback mRemoteMediaClientCallback =
            new RemoteMediaClient.Callback() {
                @Override
                public void onStatusUpdated() {
                    final int playerState = CastMediaUtils.getMediaPlayerState(
                            CastControllerActivity.this);
                    MediaPlayerState mediaPlayerState = CastMediaUtils.toMediaPlayerState(
                            CastControllerActivity.this, playerState);
                    if (getMediaPlayerState().equals(mediaPlayerState)) {
                        LogHelper.d(TAG,
                                "onStatusUpdated - ignoring identical media player state: "
                                        + mediaPlayerState);
                        return;
                    }
                    setMediaPlayerState(mediaPlayerState);
                    LogHelper.d(TAG, "onStatusUpdated - media player state: " +
                            getMediaPlayerState());
                    if (MediaPlayerState.PAUSED.equals(getMediaPlayerState()) ||
                            MediaPlayerState.PLAYING.equals(getMediaPlayerState())) {
                        setLoadingIndicatorVisibility(View.INVISIBLE);
                        setupPlaybackControlView();
                    } else {
                        setLoadingIndicatorVisibility(View.VISIBLE);
                        if (MediaPlayerState.STOPPED.equals(getMediaPlayerState())) {
                            // Notify other application components (including this activity)
                            // about the change to STOPPED player state
                            // so they can update their state if needed.
                            MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(
                                    CastControllerActivity.this,
                                    MediaPlayerState.STOPPED,
                                    DeviceType.CAST);
                        }
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCastContext = CastContext.getSharedInstance(CastControllerActivity.this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mCastContext.getSessionManager().addSessionManagerListener(
                mCastSessionManagerListener, CastSession.class);
        registerRemoteMediaClientConf();
    }

    @Override
    public void onPause() {
        mCastContext.getSessionManager().removeSessionManagerListener(
                mCastSessionManagerListener, CastSession.class);
        unregisterRemoteMediaClientConf(true);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unregisterRemoteMediaClientConf(false);
        super.onDestroy();
    }

    @Override
    protected void buildMediaRouteSelector() {
        // Build a MediaRouteSelector with capability
        // to discover and use routes to Cast devices.
        setMediaRouteSelector(new MediaRouteSelector.Builder()
                .addControlCategory(CastMediaControlIntent.categoryForCast(getResources()
                        .getString(R.string.cast_app_id))).build());
    }

    @Override
    protected void buildMediaRouterCallback() {
        setMediaRouterCallback(new ExpandedControllerActivity.DefaultMediaRouterCallback());
    }

    @Override
    public boolean hasSeekToPlaybackPositionControl() {
        return true;
    }

    @Override
    public boolean hasSetMuteControl() {
        return true;
    }

    /**
     * Registers the configuration of the {@link RemoteMediaClient} if needed.
     */
    private void registerRemoteMediaClientConf() {
        CastMediaUtils.registerRemoteMediaClientConf(
                CastControllerActivity.this,
                mRemoteMediaClientCallback);
    }

    /**
     * Unregisters the configuration of the {@link RemoteMediaClient} if needed.
     *
     * @param onPause {@code true} when this activity is about to be on pause,
     *                meaning that only the progress listener has to be unregistered,
     *                otherwise {@code false}.
     */
    private void unregisterRemoteMediaClientConf(boolean onPause) {
        CastMediaUtils.unregisterRemoteMediaClientConf(
                CastControllerActivity.this,
                onPause ? null : mRemoteMediaClientCallback);
    }

    /**
     * Monitors events of a {@link CastSession} instance.
     */
    private class CastSessionManagerListenerImpl implements SessionManagerListener<CastSession> {

        @Override
        public void onSessionStarting(CastSession castSession) {
        }

        @Override
        public void onSessionStarted(CastSession castSession, String s) {
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionStartFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionEnding(CastSession castSession) {
        }

        @Override
        public void onSessionEnded(CastSession castSession, int i) {
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionResuming(CastSession castSession, String s) {
        }

        @Override
        public void onSessionResumed(CastSession castSession, boolean b) {
            invalidateOptionsMenu();
        }

        @Override
        public void onSessionResumeFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionSuspended(CastSession castSession, int i) {
        }
    }

}
