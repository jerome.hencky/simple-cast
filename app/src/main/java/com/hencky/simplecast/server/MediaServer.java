package com.hencky.simplecast.server;

import android.content.Context;

import com.hencky.simplecast.model.ImageItem;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaUtils;
import com.koushikdutta.async.http.AsyncHttpHead;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.async.http.server.AsyncHttpServerRequest;
import com.koushikdutta.async.http.server.AsyncHttpServerResponse;
import com.koushikdutta.async.http.server.HttpServerRequestCallback;

import java.io.File;
import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;

/**
 * Represents a media server.
 */
public class MediaServer {
    private static final String TAG = LogHelper.makeLogTag(MediaServer.class);

    public static final String DEFAULT_SCHEME = "http";
    public static final int DEFAULT_PORT = 8080;

    private final WeakReference<Context> mWeakContext;
    private final AsyncHttpServer mAsyncHttpServer;
    private MediaItem mMediaItem;

    public MediaServer(@NonNull Context context) {
        mWeakContext = new WeakReference<>(context);
        //mAsyncHttpServer = new AsyncHttpServer();
        mAsyncHttpServer = new AsyncHttpServer() {
            @Override
            protected boolean onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
                response.getHeaders().set("Access-Control-Allow-Origin", "*");
                /*
                response.getHeaders().set("Content-Type", mMediaItem.getMimeType());
                response.getHeaders().set("Content-Length", Long.toString(mMediaItem.getSize()));
                */
                return super.onRequest(request, response);
            }
        };
    }

    private Context getContext() {
        return mWeakContext.get();
    }

    /**
     * Sets the {@link MediaItem}.
     *
     * @param mediaItem The media item to set.
     */
    public void setMediaItem(MediaItem mediaItem) {
        this.mMediaItem = mediaItem;

        // Remove last known actions
        /*
        for (Pair<String, String> pair : mPairs) {
            mAsyncHttpServer.removeAction(pair.first, pair.second);
        }
        mPairs.clear();
        */

        if (mMediaItem instanceof ImageItem) {
            ImageItem imageItem = (ImageItem) mMediaItem;

            // Serve the image
            String regex = MediaUtils.getMediaUri(getContext(), imageItem).getEncodedPath();
            MediaFileRequestCallback mediaFileRequestCallback = new MediaFileRequestCallback(
                    imageItem);
            mAsyncHttpServer.get(regex, mediaFileRequestCallback);
            //mPairs.add(new Pair<>(AsyncHttpGet.METHOD, regex));
        } else if (mMediaItem instanceof VideoItem) {
            VideoItem videoItem = (VideoItem) mMediaItem;

            // Serve the mini-thumbnail image
            /*
            if (videoItem.getMiniThumb() != null) {
                String regex = MediaUtils.getMediaUri(getContext(), videoItem, Kind.MINI)
                        .getEncodedPath();
                MediaFileRequestCallback mediaFileRequestCallback = new MediaFileRequestCallback(
                        videoItem.getMiniThumb());
                mAsyncHttpServer.get(regex, mediaFileRequestCallback);
                //mPairs.add(new Pair<>(AsyncHttpGet.METHOD, regex));
            }
            */

            // Serve the full-thumbnail image
            /*
            if (videoItem.getFullThumb() != null) {
                String regex = MediaUtils.getMediaUri(getContext(), videoItem, Kind.FULL)
                        .getEncodedPath();
                MediaFileRequestCallback mediaFileRequestCallback = new MediaFileRequestCallback(
                        videoItem.getFullThumb());
                mAsyncHttpServer.get(regex, mediaFileRequestCallback);
                //mPairs.add(new Pair<>(AsyncHttpGet.METHOD, regex));
            }
            */

            // Serve the video file
            String regex = MediaUtils.getMediaUri(getContext(), videoItem).getEncodedPath();
            MediaFileRequestCallback videoFileRequestCallback =
                    new MediaFileRequestCallback(mMediaItem);
            mAsyncHttpServer.get(regex, videoFileRequestCallback);
            //mPairs.add(new Pair<>(AsyncHttpGet.METHOD, regex));
            mAsyncHttpServer.addAction(AsyncHttpHead.METHOD, regex, videoFileRequestCallback);
            //mPairs.add(new Pair<>(AsyncHttpHead.METHOD, regex));
        }

        //LogHelper.d(TAG, mPairs);
    }

    public void start() {
        mAsyncHttpServer.listen(DEFAULT_PORT);
        LogHelper.d(TAG, "Started Media server");
    }

    public void stop() {
        if (mAsyncHttpServer != null) {
            mAsyncHttpServer.stop();
            LogHelper.d(TAG, "Stopped Media server");
        }
    }

    private class MediaFileRequestCallback implements HttpServerRequestCallback {
        private final MediaItem mediaItem;

        private MediaFileRequestCallback(MediaItem mediaItem) {
            this.mediaItem = mediaItem;
        }

        @Override
        public void onRequest(AsyncHttpServerRequest request, AsyncHttpServerResponse response) {
            //LogHelper.d(TAG, request.toString());
            File f = new File(mediaItem.getData());
            if (f.exists()) {
                if (mediaItem instanceof VideoItem) {
                    response.getHeaders().set("Content-Type", mMediaItem.getMimeType());
                    response.getHeaders().set("Content-Length", Long.toString(mMediaItem.getSize()));
                    /*
                    response.getHeaders().set("transferMode.dlna.org", "Streaming");
                    response.getHeaders().set("contentFeatures.dlna.org", "DLNA.ORG_OP=01;DLNA.ORG_CI=0");
                    response.getHeaders().set("Date", HttpDate.format(new Date()));
                    */
                }
                response.sendFile(f);
                //LogHelper.d(TAG, request.toString());
                //LogHelper.d(TAG, response.toString());
            } else {
                response.send("Not found");
                response.code(404);
            }
        }
    }
}
