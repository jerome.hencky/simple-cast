package com.hencky.simplecast.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.manager.MediaNotificationManager;
import com.hencky.simplecast.manager.PlaybackManager;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.DeviceType;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.model.VideoItem;
import com.hencky.simplecast.playback.CastPlayback;
import com.hencky.simplecast.playback.MediaPlayerState;
import com.hencky.simplecast.playback.Playback;
import com.hencky.simplecast.playback.UpnpPlayback;
import com.hencky.simplecast.server.MediaServerHelper;
import com.hencky.simplecast.upnp.UpnpCoreHelper;
import com.hencky.simplecast.utils.BroadcastUtils;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaPlaybackUtils;

import org.fourthline.cling.support.avtransport.lastchange.AVTransportLastChangeParser;
import org.fourthline.cling.support.avtransport.lastchange.AVTransportVariable;
import org.fourthline.cling.support.lastchange.EventedValue;
import org.fourthline.cling.support.lastchange.LastChange;
import org.fourthline.cling.support.model.TransportState;
import org.fourthline.cling.support.model.TransportStatus;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Provides a service for interacting with media playback.
 */
public class MediaPlaybackService extends Service
        implements PlaybackManager.PlaybackServiceCallback {

    private static final String TAG = LogHelper.makeLogTag(MediaPlaybackService.class);

    /**
     * Actions
     */
    public static final String ACTION_START_PLAYBACK = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_START_PLAYBACK";
    public static final String ACTION_STOP_PLAYBACK = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_STOP_PLAYBACK";
    public static final String ACTION_PAUSE_PLAYBACK = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_PAUSE_PLAYBACK";
    public static final String ACTION_SEEK_TO_PLAYBACK_POSITION_TIME_SECONDS =
            AppKeys.APP_PACKAGE_NAME + ".ACTION_SEEK_TO_PLAYBACK_POSITION_TIME_SECONDS";
    public static final String ACTION_SETUP_PLAYBACK = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_SETUP_PLAYBACK";
    public static final String ACTION_HANDLE_UPNP_SERVICE_EVENT = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_HANDLE_UPNP_SERVICE_EVENT";
    public static final String ACTION_REQUEST_MEDIA_PLAYER_STATE = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_REQUEST_MEDIA_PLAYER_STATE";
    public static final String ACTION_SET_MUTE = AppKeys.APP_PACKAGE_NAME + ".ACTION_SET_MUTE";
    public static final String ACTION_REQUEST_MUTE_STATE = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_REQUEST_MUTE_STATE";
    public static final String ACTION_CONTROLLER_COMPONENT_PAUSED = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_CONTROLLER_COMPONENT_PAUSED";

    private MediaNotificationManager mMediaNotificationManager;
    private PlaybackManager mPlaybackManager;
    private Timer mProgressListener;
    private Device mDevice;
    private MediaItem mMediaItem;
    private boolean mWithSetup;
    private long mPlaybackTimeSeconds = 0;
    private MediaPlayerState mMediaPlayerState = MediaPlayerState.NONE;

    /**
     * (non-Javadoc)
     *
     * @see Service#onCreate()
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mMediaNotificationManager = new MediaNotificationManager(this);
    }

    /**
     * (non-Javadoc)
     *
     * @see Service#onDestroy()
     */
    @Override
    public void onDestroy() {
        if (mMediaItem != null) {
            if (mMediaItem instanceof VideoItem) {
                stopProgressListener();
            }
        }
        UpnpCoreHelper.getInstance(this).release();
        MediaServerHelper.getInstance(this).release();
        if (mMediaNotificationManager != null) {
            mMediaNotificationManager.stopNotification();
        }
        super.onDestroy();
    }

    /**
     * (non-Javadoc)
     *
     * @see Service#onTaskRemoved(Intent)
     */
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if (mPlaybackManager != null) {
            mPlaybackManager.handleStopRequest();
        }
        stopSelf();
    }

    /**
     * (non-Javadoc)
     *
     * @see Service#onStartCommand(Intent, int, int)
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogHelper.d(TAG, "onStartCommand - intent=" + intent);

        if (intent == null) {
            LogHelper.d(TAG, "Null-Intent received. Stopping self.");
            stopSelf();
        } else if (ACTION_SETUP_PLAYBACK.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: SETUP PLAYBACK");

            Device deviceExtra = intent.getParcelableExtra(AppKeys.EXTRA_DEVICE);
            mMediaItem = intent.getParcelableExtra(AppKeys.EXTRA_MEDIA_ITEM);

            // Check whether we need to initialize the playback manager.
            boolean withPlaybackManagerInit = true;
            if (deviceExtra == null || deviceExtra.equals(mDevice)) {
                withPlaybackManagerInit = false;
            }
            mDevice = deviceExtra;

            // Initialize the playback manager if needed.
            LogHelper.d(TAG, "Need to init playback manager: " + withPlaybackManagerInit);
            if (withPlaybackManagerInit) {
                Playback playback = null;
                if (DeviceType.UPNP.equals(mDevice.getDeviceType())) {
                    playback = UpnpPlayback.getInstance(this);
                } else if (DeviceType.CAST.equals(mDevice.getDeviceType())) {
                    // TODO instantiate Playback interface for Cast device
                    playback = CastPlayback.getInstance(this);
                }
                mPlaybackManager = new PlaybackManager(playback, this);
                mWithSetup = true;
                mPlaybackManager.init(mDevice);
            } else {
                // Stop playback if started, otherwise set up media playback.
                if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
                    mWithSetup = true;
                    stopPlayback();
                } else {
                    setupPlayback();
                    mWithSetup = false;
                }
            }
        } else if (ACTION_START_PLAYBACK.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: START PLAYBACK");
            if (mPlaybackManager != null) {
                mPlaybackManager.handlePlayRequest();
            }
        } else if (ACTION_STOP_PLAYBACK.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: STOP PLAYBACK");
            stopPlayback();
        } else if (ACTION_PAUSE_PLAYBACK.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: PAUSE PLAYBACK");
            if (mPlaybackManager != null) {
                mPlaybackManager.handlePauseRequest();
            }
        } else if (ACTION_SEEK_TO_PLAYBACK_POSITION_TIME_SECONDS.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: SEEK TO PLAYBACK POSITION TIME IN SECONDS");
            if (mPlaybackManager != null) {
                VideoItem videoItem = (VideoItem) mMediaItem;
                long durationSeconds = videoItem.getDuration() / 1000;
                LogHelper.d(TAG, "Duration in seconds: " + durationSeconds);
                long timeSeconds = intent.getLongExtra(AppKeys.EXTRA_TIME_SECONDS, 0);
                LogHelper.d(TAG, "Playback time in seconds: " + timeSeconds);
                if (timeSeconds < 0 || timeSeconds > durationSeconds) {
                    LogHelper.w(TAG, "Cannot seek to playback position " + timeSeconds +
                            " seconds.");
                } else {
                    mPlaybackManager.handleSeekToPlaybackPositionRequest(timeSeconds);
                }
            }
        } else if (ACTION_HANDLE_UPNP_SERVICE_EVENT.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: HANDLE UPNP SERVICE EVENT");
            String lastChangeEventString = intent.getStringExtra(AppKeys.EXTRA_LASTCHANGE_STRING);
            handleUpnpLastChangeEvent(lastChangeEventString);
        } else if (ACTION_REQUEST_MEDIA_PLAYER_STATE.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: REQUEST MEDIA PLAYER STATE");
            if (mPlaybackManager != null) {
                mPlaybackManager.handleGetMediaPlayerState();
            }
        } else if (ACTION_SET_MUTE.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: MUTE SOUND");
            boolean muted = intent.getBooleanExtra(AppKeys.EXTRA_MUTED, false);
            if (mPlaybackManager != null) {
                mPlaybackManager.handleSetMuteRequest(muted);
            }
        } else if (ACTION_REQUEST_MUTE_STATE.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: REQUEST MUTE STATE");
            if (mPlaybackManager != null) {
                mPlaybackManager.handleGetMuteState();
            }
        } else if (ACTION_CONTROLLER_COMPONENT_PAUSED.equals(intent.getAction())) {
            LogHelper.d(TAG, "Service received command: CONTROLLER COMPONENT PAUSED");
            if (mMediaItem instanceof VideoItem) {
                stopProgressListener();
            }
        } else {
            LogHelper.d(TAG, "Service received intent: " + intent);
        }

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onMediaPlayerStateChanged(MediaPlayerState state) {
        mMediaPlayerState = state;
        LogHelper.d(TAG, "onMediaPlayerStateChanged: " + mMediaPlayerState);

        MediaPlaybackUtils.broadcastMediaPlayerStateChangedIntent(this, mMediaPlayerState,
                getDevice().getDeviceType());

        if (MediaPlayerState.STOPPED.equals(mMediaPlayerState)) {
            mMediaNotificationManager.stopNotification();
            if (mMediaItem instanceof VideoItem) {
                stopProgressListener();
            }
            if (mWithSetup) {
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    setupPlayback();
                    mWithSetup = false;
                }, 200);
            }
        } else {
            mMediaNotificationManager.updateNotification(mMediaPlayerState);
            if (MediaPlayerState.PLAYING.equals(mMediaPlayerState)) {
                if (mMediaItem instanceof VideoItem) {
                    startProgressListener();
                }
            } else if (MediaPlayerState.PAUSED.equals(mMediaPlayerState)) {
                if (mMediaItem instanceof VideoItem) {
                    stopProgressListener();
                }
            }
        }
    }

    @Override
    public void onMediaPlayerStateObtained(MediaPlayerState state) {
        mMediaPlayerState = state;
        LogHelper.d(TAG, "onMediaPlayerStateObtained: " + mMediaPlayerState);

        MediaPlaybackUtils.broadcastMediaPlayerStateObtainedIntent(this, mMediaPlayerState,
                getDevice().getDeviceType());

        if (MediaPlayerState.STOPPED.equals(mMediaPlayerState) ||
                MediaPlayerState.UNKNOWN.equals(mMediaPlayerState)) {
            mMediaNotificationManager.stopNotification();
            if (mMediaItem instanceof VideoItem) {
                stopProgressListener();
            }
        } else {
            mMediaNotificationManager.updateNotification(mMediaPlayerState);
            if (MediaPlayerState.PLAYING.equals(mMediaPlayerState)) {
                if (mMediaItem instanceof VideoItem) {
                    startProgressListener();
                }
            } else if (MediaPlayerState.PAUSED.equals(mMediaPlayerState)) {
                if (mMediaItem instanceof VideoItem) {
                    stopProgressListener();
                }
            }
        }
    }

    @Override
    public void onError(String error) {
        LogHelper.e(TAG, error);
    }

    @Override
    public void onPlaybackTimeObtained(long timeSeconds) {
        if (DeviceType.UPNP.equals(mDevice.getDeviceType())) {
            VideoItem videoItem = (VideoItem) mMediaItem;
            long durationSeconds = videoItem.getDuration() / 1000;
            if (timeSeconds == 0) {
                if (durationSeconds == mPlaybackTimeSeconds ||
                        durationSeconds - 1 == mPlaybackTimeSeconds) {
                    // Case when playback is finished and
                    // no event to notify the end of playback is yet received
                    LogHelper.d(TAG, "onPlaybackTimeObtained - playback is finished");
                    onMediaPlayerStateChanged(MediaPlayerState.STOPPED);
                }
            }
        }
        mPlaybackTimeSeconds = timeSeconds;
        LogHelper.d(TAG, "onPlaybackTimeObtained - playback time: " + mPlaybackTimeSeconds);
        MediaPlaybackUtils.broadcastPlaybackTimeObtainedIntent(
                this, mPlaybackTimeSeconds, getDevice().getDeviceType());
    }

    @Override
    public void onActionInvoked(@NonNull String action,
                                @NonNull ActionInvocationResult result,
                                @Nullable String message) {
        switch (action) {
            case Playback.ACTION_SUPPORT_MIMETYPE:
                if (ActionInvocationResult.FAILURE.equals(result)) {
                    mMediaNotificationManager.stopNotification();
                }
                break;
            case Playback.ACTION_INIT:
                LogHelper.d(TAG, "Received Init action invocation result: " +
                        result.name());
                if (ActionInvocationResult.SUCCESS.equals(result)) {
                    if (mWithSetup) {
                        setupPlayback();
                        mWithSetup = false;
                    }
                }
                break;
            case Playback.ACTION_SETUP:
                LogHelper.d(TAG, "Received Setup action invocation result: " +
                        result.name());
                if (ActionInvocationResult.SUCCESS.equals(result)) {
                    if (DeviceType.CAST.equals(mDevice.getDeviceType())) {
                        mMediaNotificationManager.updateNotification(MediaPlayerState.PLAYING);
                        if (mMediaItem instanceof VideoItem) {
                            startProgressListener();
                        }
                    }
                } else {
                    // Case when action invocation failure
                    mMediaNotificationManager.stopNotification();
                }
                break;
            case UpnpPlayback.ACTION_AVTRANSPORT_SETAVTRANSPORTURI:
                LogHelper.d(TAG, "Received SetAVTransportURI action invocation result: " +
                        result.name());
                if (ActionInvocationResult.SUCCESS.equals(result)) {
                    // Start the playback after setup success
                    new Handler(Looper.getMainLooper()).postDelayed(() ->
                                    mPlaybackManager.handlePlayRequest(),
                            200);
                }
                break;
        }

        BroadcastUtils.broadcastActionInvocationResultIntent(this, action, result, message);
    }

    @Override
    public void onMuteStateChanged(boolean muted) {
        LogHelper.d(TAG, "onMuteStateChanged - muted: " + muted);
        MediaPlaybackUtils.broadcastMuteStateChangedIntent(this, muted);
    }

    @Override
    public void onMuteStateObtained(boolean muted) {
        LogHelper.d(TAG, "onMuteStateObtained - muted: " + muted);
        MediaPlaybackUtils.broadcastMuteStateObtainedIntent(this, muted);
    }

    public Device getDevice() {
        return mDevice;
    }

    public MediaItem getMediaItem() {
        return mMediaItem;
    }

    public MediaPlayerState getPlaybackState() {
        return mMediaPlayerState;
    }

    public long getPlaybackTime() {
        return mPlaybackTimeSeconds;
    }

    /**
     * Returns whether the receiver device has the seek capability
     * among the possible interactions with the media.
     *
     * @return {@code true} when the device has the seek capability, otherwise {@code false}
     */
    public boolean hasDeviceSeekCapability() {
        if (mPlaybackManager != null) {
            return mMediaItem instanceof VideoItem
            && MediaPlaybackUtils.isMediaPlaying(getPlaybackState())
                    && mPlaybackManager.hasDeviceSeekCapability();
        }
        return false;
    }

    /**
     * Stops playback.
     */
    private void stopPlayback() {
        if (mPlaybackManager != null) {
            mPlaybackManager.handleStopRequest();
        }
    }

    /**
     * Sets up playback.
     */
    private void setupPlayback() {
        LogHelper.d(TAG, "Setting up playback for " + mMediaItem);
        mMediaNotificationManager.setMediaData(mMediaItem);
        mMediaNotificationManager.startNotification();
        mPlaybackManager.handleSetupRequest(mMediaItem);
    }

    /**
     * Starts a progress listener.
     */
    private void startProgressListener() {
        if (mProgressListener != null) {
            mProgressListener.cancel();
        }
        mProgressListener = new Timer();
        mProgressListener.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (mPlaybackManager != null)
                    mPlaybackManager.handleRequestPlaybackTime();
            }
        }, 100, 1000);
        LogHelper.d(TAG, "Started progress listener.");
    }

    /**
     * Stops the progress listener.
     */
    private void stopProgressListener() {
        if (mProgressListener != null) {
            mProgressListener.cancel();
            mProgressListener = null;
            LogHelper.d(TAG, "Stopped progress listener.");
        }
    }

    /**
     * Handles a LastChange event for a given String, received from an UPnP service.
     *
     * @param lastChangeEventString the String that represents the LastChange event
     */
    private void handleUpnpLastChangeEvent(String lastChangeEventString) {
        if (lastChangeEventString == null)
            return;

        LastChange lastChange;

        try {
            // Handle event sent by AVTransport service
            lastChange = new LastChange(new AVTransportLastChangeParser(), lastChangeEventString);

            if ("".equals(lastChange.toString())) {
                LogHelper.d(TAG, "Ignoring event that is not sent by UPnP AVTransport service.");
                return;
            }

            // Check the TransportState
            EventedValue transportStateEventedValue = lastChange.getEventedValue(0,
                    AVTransportVariable.TransportState.class);

            if (transportStateEventedValue != null) {
                TransportState transportStateValue = (TransportState) transportStateEventedValue
                        .getValue();
                LogHelper.d(TAG, "UPnP TransportState: " + transportStateValue.getValue());
                if (TransportState.STOPPED.equals(transportStateValue)) {
                    if (MediaPlaybackUtils.isPlaybackStarted(mMediaPlayerState)) {
                        // Check the TransportStatus
                        EventedValue transportStatusEventedValue = lastChange.getEventedValue(0,
                                AVTransportVariable.TransportStatus.class);
                        if (transportStatusEventedValue != null) {
                            // case when the TransportStatus is known
                            // check the TransportStatus value
                            TransportStatus transportStatusValue =
                                    (TransportStatus) transportStatusEventedValue.getValue();
                            LogHelper.d(TAG, "UPnP TransportStatus: " + transportStatusValue
                                    .getValue());
                            if (TransportStatus.OK.equals(transportStatusValue)) {
                                onMediaPlayerStateChanged(MediaPlayerState.STOPPED);
                            } else {
                                LogHelper.d(TAG, "Ignoring UPnP STOPPED TransportState " +
                                        "when the TransportStatus is not OK.");
                            }
                        } else {
                            // case when the TransportStatus is unknown
                            onMediaPlayerStateChanged(MediaPlayerState.STOPPED);
                        }
                    } else {
                        LogHelper.d(TAG,
                                "Ignoring UPnP STOPPED TransportState when the playback" +
                                        " is not started.");
                    }
                } else if (TransportState.PAUSED_PLAYBACK.equals(transportStateValue)) {
                    onMediaPlayerStateChanged(MediaPlayerState.PAUSED);
                } else if (TransportState.PLAYING.equals(transportStateValue)) {
                    onMediaPlayerStateChanged(MediaPlayerState.PLAYING);
                } else if (TransportState.TRANSITIONING.equals(transportStateValue)) {
                    onMediaPlayerStateChanged(MediaPlayerState.TRANSITIONING);
                } else {
                    LogHelper.d(TAG, "Ignoring UPnP TransportState=" +
                            transportStateValue.getValue());
                }
            }
        } catch (Exception e) {
            LogHelper.e(TAG, "An error occurred when parsing the UPnP LastChange message: " +
                    e.getMessage());
        }
    }
}
