package com.hencky.simplecast;

/**
 * Represents the application keys.
 */
public class AppKeys {

    public static final String APP_PACKAGE_NAME = "com.hencky.simplecast";

    /**
     * Services
     */
    public static final String MEDIA_PLAYBACK_SERVICE = APP_PACKAGE_NAME +
            ".service.MediaPlaybackService";

    /**
     * Extras
     */
    public static final String EXTRA_MEDIA_LIB_FOLDER = APP_PACKAGE_NAME +
            ".EXTRA_MEDIA_LIB_FOLDER";
    public static final String EXTRA_MEDIA_ITEM = APP_PACKAGE_NAME + ".EXTRA_MEDIA_ITEM";
    public static final String EXTRA_DEVICE = APP_PACKAGE_NAME + ".EXTRA_DEVICE";
    public static final String EXTRA_DEVICE_TYPE = APP_PACKAGE_NAME + ".EXTRA_DEVICE_TYPE";
    public static final String EXTRA_TIME_SECONDS = APP_PACKAGE_NAME + ".EXTRA_TIME_SECONDS";
    public static final String EXTRA_PLAYER_STATE = APP_PACKAGE_NAME + ".EXTRA_PLAYER_STATE";
    public static final String EXTRA_DISPLAY_MODE = APP_PACKAGE_NAME + ".EXTRA_DISPLAY_MODE";
    public static final String EXTRA_IMAGES_SORT_MODE = APP_PACKAGE_NAME +
            ".EXTRA_IMAGES_SORT_MODE";
    public static final String EXTRA_VIDEOS_SORT_MODE = APP_PACKAGE_NAME +
            ".EXTRA_VIDEOS_SORT_MODE";
    public static final String EXTRA_PLAYBACK_TIME = APP_PACKAGE_NAME + ".EXTRA_PLAYBACK_TIME";
    public static final String EXTRA_ACTION_INVOCATION_RESULT = APP_PACKAGE_NAME +
            ".EXTRA_ACTION_INVOCATION_RESULT";
    public static final String EXTRA_ACTION_INVOCATION_RESULT_MESSAGE = APP_PACKAGE_NAME +
            ".EXTRA_ACTION_INVOCATION_RESULT_MESSAGE";
    public static final String EXTRA_LASTCHANGE_STRING = APP_PACKAGE_NAME +
            ".EXTRA_LASTCHANGE_STRING";
    public static final String EXTRA_MUTED = APP_PACKAGE_NAME + ".EXTRA_MUTED";
}

