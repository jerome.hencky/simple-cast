package com.hencky.simplecast.adapter;

import android.database.Cursor;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Cursor adapter designed for RecyclerView.
 */
public abstract class RecyclerViewCursorAdapter<VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<VH> {

    private Cursor mCursor;
    private int mRowIDColumn = -1;

    protected RecyclerViewCursorAdapter() {
        setHasStableIds(true);
    }

    /**
     * Swap in a new Cursor, returning the old Cursor.
     * The old Cursor is not closed.
     *
     * @param newCursor the new cursor to swap in
     * @return The previously set Cursor, if any.
     * If the given new Cursor is the same instance as the previously set
     * Cursor, null is also returned.
     */
    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor == mCursor) {
            return null;
        }
        Cursor oldCursor = mCursor;
        mCursor = newCursor;
        mRowIDColumn = (newCursor == null) ? -1 : newCursor.getColumnIndexOrThrow("_id");
        notifyDataSetChanged();
        return oldCursor;
    }

    public Cursor getCursor() {
        return mCursor;
    }

    @Override
    public int getItemCount() {
        return (mCursor == null || mCursor.isClosed()) ? 0 : mCursor.getCount();
    }

    /**
     * @param position the position where to move the cursor
     * @return The Cursor initialized to the specified position.
     */

    protected Cursor getItem(int position) {
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.moveToPosition(position);
        }
        return mCursor;
    }

    @Override
    public long getItemId(int position) {
        if ((mCursor != null) && mCursor.moveToPosition(position)) {
            return mCursor.getLong(mRowIDColumn);
        }
        return RecyclerView.NO_ID;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        if (mCursor == null) {
            throw new IllegalStateException(
                    "this should only be called when the Cursor is not null");
        }
        if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("couldn't move Cursor to position " + position);
        }
        onBindViewHolder(holder, mCursor);
    }

    public abstract void onBindViewHolder(VH holder, Cursor cursor);
}
