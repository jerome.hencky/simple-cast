package com.hencky.simplecast.playback;

import androidx.annotation.Nullable;

import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.MediaItem;

/**
 * Interface interacting with media playback.
 */
public interface Playback {

    /**
     * Actions
     */
    String ACTION_INIT = AppKeys.APP_PACKAGE_NAME + ".ACTION_INIT";
    String ACTION_SETUP = AppKeys.APP_PACKAGE_NAME + ".ACTION_SETUP";
    String ACTION_SUPPORT_MIMETYPE = AppKeys.APP_PACKAGE_NAME + ".ACTION_SUPPORT_MIMETYPE";

    /**
     * Sets up the playback.
     *
     * @param mediaItem the requested {@link MediaItem} to set up the playback
     */
    void setup(MediaItem mediaItem);

    /**
     * Starts the playback.
     */
    void play();

    /**
     * Pauses the playback.
     */
    void pause();

    /**
     * Stops the playback.
     */
    void stop();

    /**
     * Seeks to a new playback position specified by a time in seconds.
     *
     * @param timeSeconds the time in seconds
     */
    void seekTo(long timeSeconds);

    /**
     * Requests the playback time.
     */
    void requestPlaybackTime();

    /**
     * Requests player state of the current media.
     */
    void requestMediaPlayerState();

    /**
     * Initializes the playback for a given {@link Device}.
     *
     * @param device the Device object that is required when initializing playback.
     */
    void init(Device device);

    /**
     * Returns whether the receiver device has the seek capability
     * among the possible interactions with the media.
     *
     * @return {@code true} when the receiver device has the seek capability,
     * otherwise {@code false}
     */
    boolean hasDeviceSeekCapability();

    /**
     * Mutes or restores the sound.
     *
     * @param muted {@code true} if the sound should be muted,
     *              {@code false} if the sound should be restored.
     */
    void setMute(boolean muted);

    /**
     * Requests the mute state of the sound.
     */
    void requestMuteState();

    interface Callback {
        /**
         * Called on media player state changed.
         *
         * @param state the new media player state
         */
        void onMediaPlayerStateChanged(MediaPlayerState state);

        /**
         * Called on media player state obtained.
         *
         * @param state the media player state that is obtained
         */
        void onMediaPlayerStateObtained(MediaPlayerState state);

        /**
         * Called on error occurred.
         *
         * @param error the error message
         */
        void onError(String error);

        /**
         * Called on playback time obtained.
         *
         * @param timeSeconds the obtained playback time in seconds
         */
        void onPlaybackTimeObtained(long timeSeconds);

        /**
         * Called on action invoked.
         *
         * @param action  The string representation of the invoked action
         * @param result  The ActionInvocation result
         * @param message The message that can be associated to the ActionInvocation result
         */
        void onActionInvoked(String action, ActionInvocationResult result,
                             @Nullable String message);

        /**
         * Called on mute state changed.
         *
         * @param muted {@code true} if the sound is muted,
         *              {@code false} if the sound is restored.
         */
        void onMuteStateChanged(boolean muted);

        /**
         * Called on mute state obtained.
         *
         * @param muted {@code true} if the sound is muted,
         *              {@code false} if the sound is restored.
         */
        void onMuteStateObtained(boolean muted);
    }

    /**
     * Sets the callback of this interface.
     *
     * @param callback to be called
     */
    void setCallback(Callback callback);
}
