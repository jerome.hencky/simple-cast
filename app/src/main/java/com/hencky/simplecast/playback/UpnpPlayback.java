package com.hencky.simplecast.playback;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.hencky.simplecast.AppKeys;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.upnp.UpnpCoreHelper;
import com.hencky.simplecast.utils.LogHelper;
import com.hencky.simplecast.utils.MediaUtils;
import com.hencky.simplecast.utils.UpnpUtils;

import org.fourthline.cling.controlpoint.ActionCallback;
import org.fourthline.cling.model.ModelUtil;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.support.avtransport.callback.GetPositionInfo;
import org.fourthline.cling.support.avtransport.callback.GetTransportInfo;
import org.fourthline.cling.support.avtransport.callback.Pause;
import org.fourthline.cling.support.avtransport.callback.Play;
import org.fourthline.cling.support.avtransport.callback.Seek;
import org.fourthline.cling.support.avtransport.callback.SetAVTransportURI;
import org.fourthline.cling.support.avtransport.callback.Stop;
import org.fourthline.cling.support.connectionmanager.callback.GetProtocolInfo;
import org.fourthline.cling.support.model.PositionInfo;
import org.fourthline.cling.support.model.ProtocolInfo;
import org.fourthline.cling.support.model.ProtocolInfos;
import org.fourthline.cling.support.model.SeekMode;
import org.fourthline.cling.support.model.TransportInfo;
import org.fourthline.cling.support.renderingcontrol.callback.GetMute;
import org.fourthline.cling.support.renderingcontrol.callback.SetMute;

import javax.xml.datatype.DatatypeConfigurationException;

/**
 * An implementation of Playback that interacts with UPnP receiver device.
 */
public class UpnpPlayback implements Playback {

    private static final String TAG = LogHelper.makeLogTag(UpnpPlayback.class);

    /**
     * Actions
     */
    public static final String ACTION_AVTRANSPORT_SETAVTRANSPORTURI = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_AVTRANSPORT_SETAVTRANSPORTURI";
    public static final String ACTION_AVTRANSPORT_GETPOSITIONINFO = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_AVTRANSPORT_GETPOSITIONINFO";
    private static final String ACTION_AVTRANSPORT_GETTRANSPORTINFO = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_AVTRANSPORT_GETTRANSPORTINFO";
    public static final String ACTION_AVTRANSPORT_STOP = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_AVTRANSPORT_STOP";
    public static final String ACTION_AVTRANSPORT_PLAY = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_AVTRANSPORT_PLAY";
    public static final String ACTION_AVTRANSPORT_PAUSE = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_AVTRANSPORT_PAUSE";
    public static final String ACTION_AVTRANSPORT_SEEK = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_AVTRANSPORT_SEEK";
    private static final String ACTION_RENDERINGCONTROL_SETMUTE = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_RENDERINGCONTROL_SETMUTE";
    private static final String ACTION_RENDERINGCONTROL_GETMUTE = AppKeys.APP_PACKAGE_NAME +
            ".ACTION_RENDERINGCONTROL_GETMUTE";

    private static UpnpPlayback INSTANCE;

    private final Context mAppContext;

    private ProtocolInfo mProtocolInfo;
    private Callback mCallback;
    private MediaItem mMediaItem;
    private Device mDevice;
    private ProtocolInfos mSinkProtocolInfos;

    private UpnpPlayback(Context appContext) {
        mAppContext = appContext;
    }

    public static synchronized UpnpPlayback getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UpnpPlayback(context.getApplicationContext());
        }
        return INSTANCE;
    }

    @Override
    public void setup(MediaItem mediaItem) {
        mMediaItem = mediaItem;
        mProtocolInfo = UpnpUtils.findProtocolInfoByMimeType(mSinkProtocolInfos,
                mMediaItem.getMimeType());
        setAVTransportURI();
    }

    @Override
    public void play() {
        Service avTransportService = UpnpCoreHelper.getInstance(mAppContext).getAVTransportService();
        if (avTransportService == null) {
            LogHelper.d(TAG, "Cannot start playback" +
                    " because AVTransportService is null.");
            return;
        }

        ActionCallback playActionCallback = new Play(avTransportService) {
            @Override
            public void success(ActionInvocation invocation) {
                LogHelper.d(TAG, "Play action was successfully invoked.");
                mCallback.onMediaPlayerStateChanged(MediaPlayerState.PLAYING);
            }

            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation,
                                String defaultMsg) {
                LogHelper.e(TAG, "Failed to start playback. " + defaultMsg + ", " +
                        invocation.toString());
                mCallback.onActionInvoked(ACTION_AVTRANSPORT_PLAY, ActionInvocationResult.FAILURE,
                        null);
            }
        };

        UpnpCoreHelper.getInstance(mAppContext).executeAction(playActionCallback);
    }

    @Override
    public void pause() {
        Service avTransportService =
                UpnpCoreHelper.getInstance(mAppContext).getAVTransportService();
        if (avTransportService == null) {
            LogHelper.d(TAG, "Cannot pause playback" +
                    " because AVTransportService is null.");
            return;
        }

        ActionCallback pauseActionCallback = new Pause(avTransportService) {
            @Override
            public void success(ActionInvocation invocation) {
                LogHelper.d(TAG, "Pause action was successfully invoked.");
                mCallback.onMediaPlayerStateChanged(MediaPlayerState.PAUSED);
            }

            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation,
                                String defaultMsg) {
                LogHelper.e(TAG, "Failed to pause playback. " + defaultMsg + ", " +
                        invocation.toString());
                mCallback.onActionInvoked(ACTION_AVTRANSPORT_PAUSE, ActionInvocationResult.FAILURE,
                        null);
            }
        };

        UpnpCoreHelper.getInstance(mAppContext).executeAction(pauseActionCallback);
    }

    @Override
    public void stop() {
        Service avTransportService =
                UpnpCoreHelper.getInstance(mAppContext).getAVTransportService();
        if (avTransportService == null) {
            LogHelper.d(TAG, "Cannot stop playback" +
                    " because AVTransportService is null.");
            return;
        }

        ActionCallback stopActionCallback = new Stop(avTransportService) {
            @Override
            public void success(ActionInvocation invocation) {
                LogHelper.d(TAG, "Stop action was successfully invoked.");
                mCallback.onMediaPlayerStateChanged(MediaPlayerState.STOPPED);
            }

            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation,
                                String defaultMsg) {
                LogHelper.e(TAG, "Failed to stop playback. " + defaultMsg + ", " +
                        invocation.toString());
                mCallback.onActionInvoked(ACTION_AVTRANSPORT_STOP, ActionInvocationResult.FAILURE,
                        null);
            }
        };

        UpnpCoreHelper.getInstance(mAppContext).executeAction(stopActionCallback);
    }

    @Override
    public void seekTo(long timeSeconds) {
        seekTo(ModelUtil.toTimeString(timeSeconds));
    }

    @Override
    public void requestPlaybackTime() {
        Service avTransportService = UpnpCoreHelper.getInstance(mAppContext)
                .getAVTransportService();
        if (avTransportService == null) {
            LogHelper.d(TAG, "Cannot request playback time" +
                    " because AVTransportService is null.");
            return;
        }

        ActionCallback getPositionInfoActionCallback = new GetPositionInfo(avTransportService) {
            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation,
                                String defaultMsg) {
                LogHelper.e(TAG, "Failed to get position info. " + defaultMsg + ", " +
                        invocation.toString());
                mCallback.onActionInvoked(ACTION_AVTRANSPORT_GETPOSITIONINFO,
                        ActionInvocationResult.FAILURE, null);
            }

            @Override
            public void received(ActionInvocation invocation, PositionInfo positionInfo) {
                //LogHelper.d(TAG, "GetPositionInfo action was successfully invoked.");
                //LogHelper.d(TAG, positionInfo.toString());
                long relTimeSeconds = ModelUtil.fromTimeString(positionInfo.getRelTime().split(
                        "\\.")[0]);
                //LogHelper.d(TAG, "relTimeSeconds=" + relTimeSeconds);
                mCallback.onPlaybackTimeObtained(relTimeSeconds);
            }
        };

        UpnpCoreHelper.getInstance(mAppContext).executeAction(getPositionInfoActionCallback);
    }

    @Override
    public void requestMediaPlayerState() {
        Service avTransportService =
                UpnpCoreHelper.getInstance(mAppContext).getAVTransportService();
        if (avTransportService == null) {
            LogHelper.d(TAG, "Cannot request media player state" +
                    " because AVTransportService is null.");
            return;
        }

        ActionCallback getTransportInfoCallback = new GetTransportInfo(avTransportService) {
            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation,
                                String defaultMsg) {
                LogHelper.e(TAG, "Failed to get transport info. " + defaultMsg + ", " +
                        invocation.toString());
                mCallback.onActionInvoked(ACTION_AVTRANSPORT_GETTRANSPORTINFO,
                        ActionInvocationResult.FAILURE, null);
            }

            @Override
            public void received(ActionInvocation invocation, TransportInfo transportInfo) {
                String transportState = transportInfo.getCurrentTransportState().getValue();
                LogHelper.d(TAG, "Current transport state: " + transportState);
                MediaPlayerState mediaPlayerState = UpnpUtils.toMediaPlayerState(transportState);
                LogHelper.d(TAG, "Current media player state: " + mediaPlayerState);
                mCallback.onMediaPlayerStateObtained(mediaPlayerState);
            }
        };

        UpnpCoreHelper.getInstance(mAppContext).

                executeAction(getTransportInfoCallback);

    }

    @Override
    public void init(Device device) {
        LogHelper.d(TAG, "Get ProtocolInfos for " + device);

        mDevice = device;

        Service connectionManagerService =
                UpnpCoreHelper.getInstance(mAppContext).getConnectionManagerService();
        if (connectionManagerService == null) {
            LogHelper.d(TAG, "Cannot initialize playback for device " +
                    mDevice.getName() + " because ConnectionManagerService is null.");
            return;
        }

        ActionCallback getProtocolInfoCallback =
                new GetProtocolInfo(connectionManagerService,
                        UpnpCoreHelper.getInstance(mAppContext).getControlPoint()) {
                    @Override
                    public void received(ActionInvocation actionInvocation,
                                         ProtocolInfos sinkProtocolInfos,
                                         ProtocolInfos sourceProtocolInfos) {
                        LogHelper.d(TAG, "GetProtocolInfo action was successfully invoked.");
                        mSinkProtocolInfos = sinkProtocolInfos;
                        mCallback.onActionInvoked(Playback.ACTION_INIT,
                                ActionInvocationResult.SUCCESS, null);
                    }

                    @Override
                    public void failure(ActionInvocation invocation, UpnpResponse operation,
                                        String defaultMsg) {
                        LogHelper.e(TAG, "Cannot get ProtocolInfos for device " +
                                mDevice.getName() + ". " + defaultMsg + ", " +
                                invocation.toString());
                        mCallback.onActionInvoked(Playback.ACTION_INIT,
                                ActionInvocationResult.FAILURE, null);
                    }
                };

        UpnpCoreHelper.getInstance(mAppContext).executeAction(getProtocolInfoCallback);
    }

    @Override
    public boolean hasDeviceSeekCapability() {
        return UpnpCoreHelper.getInstance(mAppContext).hasRelTimeSeekModeCapability();
    }

    @Override
    public void setMute(boolean muted) {
        Service renderingControlService =
                UpnpCoreHelper.getInstance(mAppContext).getRenderingControlService();
        if (renderingControlService == null) {
            LogHelper.d(TAG, "Cannot set Mute because RenderingControlService is null.");
            return;
        }
        ActionCallback setMuteCallback =
                new SetMute(renderingControlService, muted) {
                    @Override
                    public void success(ActionInvocation invocation) {
                        LogHelper.d(TAG, "SetMute action was successfully invoked.");
                        mCallback.onMuteStateChanged(muted);
                    }

                    @Override
                    public void failure(ActionInvocation invocation, UpnpResponse operation,
                                        String defaultMsg) {
                        LogHelper.e(TAG, "Failed to invoke SetMute action. "
                                + defaultMsg + ", " + invocation.toString());
                        mCallback.onActionInvoked(ACTION_RENDERINGCONTROL_SETMUTE,
                                ActionInvocationResult.FAILURE, null);
                    }
                };

        UpnpCoreHelper.getInstance(mAppContext).executeAction(setMuteCallback);
    }

    @Override
    public void requestMuteState() {
        Service renderingControlService =
                UpnpCoreHelper.getInstance(mAppContext).getRenderingControlService();
        if (renderingControlService == null) {
            LogHelper.d(TAG, "Cannot request Mute state because" +
                    " RenderingControlService is null.");
            return;
        }
        ActionCallback getMuteCallback =
                new GetMute(renderingControlService) {
                    @Override
                    public void received(ActionInvocation actionInvocation, boolean currentMute) {
                        mCallback.onMuteStateObtained(currentMute);
                    }

                    @Override
                    public void failure(ActionInvocation invocation, UpnpResponse operation,
                                        String defaultMsg) {
                        LogHelper.e(TAG, "Failed to invoke GetMute action. "
                                + defaultMsg + ", " + invocation.toString());
                        mCallback.onActionInvoked(ACTION_RENDERINGCONTROL_GETMUTE,
                                ActionInvocationResult.FAILURE, null);
                    }
                };
        UpnpCoreHelper.getInstance(mAppContext).executeAction(getMuteCallback);
    }

    @Override
    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    /**
     * Seeks to a new playback target time.
     *
     * @param targetTime the new playback target time
     */
    private void seekTo(String targetTime) {
        LogHelper.d(TAG, "Playback target time: " + targetTime);

        Service avTransportService =
                UpnpCoreHelper.getInstance(mAppContext).getAVTransportService();
        if (avTransportService == null) {
            LogHelper.d(TAG, "Cannot seek to new position"
                    + " because AvTransportService is null.");
            return;
        }

        ActionCallback seekActionCallback = new Seek(avTransportService, SeekMode.REL_TIME,
                targetTime) {
            @Override
            public void success(ActionInvocation invocation) {
                LogHelper.d(TAG, "Seek action was successfully invoked.");
                mCallback.onMediaPlayerStateChanged(MediaPlayerState.SEEKED);
            }

            @Override
            public void failure(ActionInvocation invocation, UpnpResponse operation,
                                String defaultMsg) {
                LogHelper.e(TAG, "Failed to seek player to a new playback time. " +
                        defaultMsg + ", " + invocation.toString());
                mCallback.onActionInvoked(ACTION_AVTRANSPORT_SEEK, ActionInvocationResult.FAILURE,
                        null);
            }
        };

        UpnpCoreHelper.getInstance(mAppContext).executeAction(seekActionCallback);
    }

    /**
     * Sets an URI for media playback to the AVTransport service.
     */
    private void setAVTransportURI() {
        LogHelper.d(TAG, "Set AVTransportURI");

        if (mProtocolInfo == null) {
            LogHelper.e(TAG, "The ProtocolInfo was not found. MIME type " +
                    mMediaItem.getMimeType() + " is not supported by the receiver device.");
            mCallback.onActionInvoked(Playback.ACTION_SUPPORT_MIMETYPE,
                    ActionInvocationResult.FAILURE,
                    mMediaItem.getMimeType());

            // Log an exception to Crashlytics as non-fatal issue.
            try {
                StringBuilder mSinkProtocolInfosSb = new StringBuilder();
                if (mSinkProtocolInfos != null) {
                    for (ProtocolInfo protocolInfo : mSinkProtocolInfos) {
                        LogHelper.d(TAG, "ProtocolInfo: " + protocolInfo.toString());
                        mSinkProtocolInfosSb.append(protocolInfo.toString()).append("\n");
                    }
                }
                String message = "The MIME type " +
                        mMediaItem.getMimeType() +
                        " is not supported by the receiver device " +
                        mDevice.getName() +
                        "\n\nSupported Protocol Infos:\n\n" +
                        mSinkProtocolInfosSb.toString();
                throw new DatatypeConfigurationException(message);
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

            return;
        } else {
            // Force the MIME type to video/x-mkv in the VideoItem object
            // when it was found in the ProtocolInfos object.
            if ("video/x-mkv".equals(mProtocolInfo.getContentFormat())) {
                mMediaItem.setMimeType("video/x-mkv");
            }
        }

        Uri uri = MediaUtils.getMediaUri(mAppContext, mMediaItem);
        LogHelper.d(TAG, "URI to set: " + uri.toString());

        Service avTransportService =
                UpnpCoreHelper.getInstance(mAppContext).getAVTransportService();
        if (avTransportService == null) {
            LogHelper.d(TAG, "Cannot Set AVTransportURI"
                    + " because AvTransportService is null.");
            return;
        }

        ActionCallback setAVTransportURIActionCallback =
                new SetAVTransportURI(avTransportService,
                        uri.toString(),
                        UpnpUtils.buildMetadata(mMediaItem, uri.toString(), mProtocolInfo)) {
                    @Override
                    public void success(ActionInvocation invocation) {
                        LogHelper.d(TAG, "SetAVTransportURI action was successfully invoked.");
                        mCallback.onActionInvoked(ACTION_AVTRANSPORT_SETAVTRANSPORTURI,
                                ActionInvocationResult.SUCCESS, null);
                    }

                    @Override
                    public void failure(ActionInvocation invocation, UpnpResponse operation,
                                        String defaultMsg) {
                        LogHelper.e(TAG, "Cannot set URI. " + defaultMsg + ", " +
                                invocation.toString());
                        mCallback.onActionInvoked(ACTION_AVTRANSPORT_SETAVTRANSPORTURI,
                                ActionInvocationResult.FAILURE, null);
                    }
                };

        UpnpCoreHelper.getInstance(mAppContext).executeAction(setAVTransportURIActionCallback);
    }
}
