package com.hencky.simplecast.playback;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.MediaSeekOptions;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.hencky.simplecast.model.ActionInvocationResult;
import com.hencky.simplecast.model.Device;
import com.hencky.simplecast.model.MediaItem;
import com.hencky.simplecast.utils.CastMediaUtils;
import com.hencky.simplecast.utils.LogHelper;

/**
 * An implementation of Playback that interacts with Cast receiver device.
 */
public class CastPlayback implements Playback {

    private static final String TAG = LogHelper.makeLogTag(CastPlayback.class);

    private static CastPlayback INSTANCE;

    private final Context mAppContext;
    private Callback mCallback;

    private CastPlayback(Context appContext) {
        mAppContext = appContext;
    }

    public static synchronized CastPlayback getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new CastPlayback(context.getApplicationContext());
        }
        return INSTANCE;
    }

    @Override
    public void setup(MediaItem mediaItem) {
        CastSession castSession = CastMediaUtils.getCurrentCastSession(mAppContext);
        if (castSession == null)
            return;

        final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
        if (remoteMediaClient == null)
            return;

        MediaInfo mediaInfo = CastMediaUtils.getMediaInfo(mAppContext, mediaItem);
        MediaLoadOptions mediaLoadOptions = new MediaLoadOptions.Builder().build();

        // Load the remote media with specified options
        PendingResult<RemoteMediaClient.MediaChannelResult> result =
                remoteMediaClient.load(mediaInfo, mediaLoadOptions);
        result.setResultCallback(mediaChannelResult -> {
            Status status = mediaChannelResult.getStatus();
            LogHelper.d(TAG, "Success of load media command: " + status.isSuccess());
            if (status.isSuccess()) {
                mCallback.onActionInvoked(Playback.ACTION_SETUP, ActionInvocationResult.SUCCESS,
                        null);
            } else {
                mCallback.onActionInvoked(Playback.ACTION_SETUP, ActionInvocationResult.FAILURE,
                        null);
            }
        });
    }

    @Override
    public void play() {
        CastSession castSession = CastMediaUtils.getCurrentCastSession(mAppContext);
        if (castSession == null)
            return;

        final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
        if (remoteMediaClient == null)
            return;

        PendingResult<RemoteMediaClient.MediaChannelResult> result = remoteMediaClient.play();
        result.setResultCallback(mediaChannelResult -> {
            Status status = mediaChannelResult.getStatus();
            LogHelper.d(TAG, "Play command result is success: " + status.isSuccess());
            if (status.isSuccess()) {
                mCallback.onMediaPlayerStateChanged(MediaPlayerState.PLAYING);
            }
        });
    }

    @Override
    public void pause() {
        CastSession castSession = CastMediaUtils.getCurrentCastSession(mAppContext);
        if (castSession == null)
            return;

        final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
        if (remoteMediaClient == null)
            return;

        PendingResult<RemoteMediaClient.MediaChannelResult> result = remoteMediaClient.pause();
        result.setResultCallback(mediaChannelResult -> {
            Status status = mediaChannelResult.getStatus();
            LogHelper.d(TAG, "Pause command result is success: " + status.isSuccess());
            if (status.isSuccess()) {
                mCallback.onMediaPlayerStateChanged(MediaPlayerState.PAUSED);
            }
        });
    }

    @Override
    public void stop() {
        CastSession castSession = CastMediaUtils.getCurrentCastSession(mAppContext);
        if (castSession == null)
            return;

        final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
        if (remoteMediaClient == null)
            return;

        PendingResult<RemoteMediaClient.MediaChannelResult> result = remoteMediaClient.stop();
        result.setResultCallback(mediaChannelResult -> {
            Status status = mediaChannelResult.getStatus();
            LogHelper.d(TAG, "Stop command result is success: " + status.isSuccess());
            if (status.isSuccess()) {
                mCallback.onMediaPlayerStateChanged(MediaPlayerState.STOPPED);
            }
        });
    }

    @Override
    public void seekTo(long timeSeconds) {
        CastSession castSession = CastMediaUtils.getCurrentCastSession(mAppContext);
        if (castSession == null)
            return;

        final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
        if (remoteMediaClient == null)
            return;

        MediaSeekOptions.Builder builder = new MediaSeekOptions.Builder();
        builder.setPosition(timeSeconds * 1000);
        PendingResult<RemoteMediaClient.MediaChannelResult> result =
                remoteMediaClient.seek(builder.build());
        result.setResultCallback(mediaChannelResult -> {
            Status status = mediaChannelResult.getStatus();
            LogHelper.d(TAG, "Seek command result is success: " + status.isSuccess());
            if (status.isSuccess()) {
                mCallback.onMediaPlayerStateChanged(MediaPlayerState.SEEKED);
            }
        });
    }

    @Override
    public void requestPlaybackTime() {
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() ->
                mCallback.onPlaybackTimeObtained(CastMediaUtils.getPlaybackTime(mAppContext)));
    }

    @Override
    public void init(Device device) {
        mCallback.onActionInvoked(Playback.ACTION_INIT, ActionInvocationResult.SUCCESS, null);
    }

    @Override
    public boolean hasDeviceSeekCapability() {
        return true;
    }

    @Override
    public void setMute(boolean muted) {
        CastSession castSession = CastMediaUtils.getCurrentCastSession(mAppContext);
        if (castSession == null)
            return;

        final RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();
        if (remoteMediaClient == null)
            return;

        PendingResult<RemoteMediaClient.MediaChannelResult> result =
                remoteMediaClient.setStreamMute(muted);
        result.setResultCallback(mediaChannelResult -> {
            Status status = mediaChannelResult.getStatus();
            LogHelper.d(TAG, "Mute command result is success: " + status.isSuccess());
            if (status.isSuccess()) {
                mCallback.onMuteStateChanged(muted);
            }
        });
    }

    @Override
    public void requestMuteState() {
        boolean muted = CastMediaUtils.getMuteState(mAppContext);
        LogHelper.d(TAG, "requestMuteState - muted: " + muted);
        mCallback.onMuteStateObtained(muted);
    }

    @Override
    public void requestMediaPlayerState() {
        int playerState = CastMediaUtils.getMediaPlayerState(mAppContext);
        LogHelper.d(TAG, "requestMediaPlayerState - playerState: " + playerState);
        MediaPlayerState mediaPlayerState = CastMediaUtils.toMediaPlayerState(mAppContext,
                playerState);
        LogHelper.d(TAG, "Current media player state: " + mediaPlayerState);
        mCallback.onMediaPlayerStateObtained(mediaPlayerState);
    }

    @Override
    public void setCallback(Callback callback) {
        mCallback = callback;
    }
}
