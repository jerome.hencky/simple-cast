package com.hencky.simplecast.playback;

/**
 * Represents the media player state.
 */
public enum MediaPlayerState {
    NONE, STOPPED, PAUSED,
    PLAYING, SEEKED, ERROR,
    TRANSITIONING, UNKNOWN
}
