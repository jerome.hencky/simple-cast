<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- This application -->
    <string name="app_author">Auteur</string>
    <string name="app_desc">
        Cette application permet de diffuser des images et des vidéos de votre appareil vers un
        appareil récepteur multimédia qui est connecté à votre TV. Le récepteur multimédia peut être
        une Smart TV, un boîtier TV, un boîtier multimédia ou un chromecast. Votre appareil et le
        récepteur multimédia doivent tous deux être connectés au même réseau Wi-Fi. Le récepteur
        multimédia doit prendre en charge les protocoles UPnP ou Google Cast.
    </string>
    <string name="app_privacy_policy">Règles de confidentialité</string>
    <string name="app_libraries">Librairies</string>
    <string name="app_libraries_desc">Librairies utilisées par cette application.</string>

    <!-- Cast -->
    <string name="media_route_menu_title">Jouer sur&#8230;</string>
    <string name="introducing_cast">Choisissez un récepteur multimédia</string>
    <string name="dialog_choose_media_receiver">
        Choisissez un récepteur multimédia avec l\'icône Cast pour diffuser un fichier multimédia.
    </string>
    <string name="not_yet_connected_to_device">
        Pas encore connecté à %1$s. Attendez un petit peu.
    </string>

    <!-- Folder picker -->
    <string name="dialog_title_pick_folder">Choisissez un dossier</string>

    <!-- Menu -->
    <string name="menu_about">À propos</string>
    <string name="menu_settings">Paramètres</string>
    <string name="menu_media_lib">Médiathèque</string>
    <string name="description_media_lib_folder">Dossier de la médiathèque</string>

    <!-- Miscellaneous -->
    <string name="about">À propos</string>

    <!-- Network -->
    <string name="dialog_wifi_connection">
        Connectez-vous au réseau Wi-Fi pour découvrir des récepteurs multimédia.
    </string>
    <string name="wifi_settings">Réglages Wi-Fi</string>

    <!-- Notifications -->
    <string name="label_pause">Mettre en pause</string>
    <string name="label_play">Lire</string>
    <string name="label_replay_10">Se déplacer 10s en arrière</string>
    <string name="label_forward_10">Se déplacer 10s en avant</string>
    <string name="label_replay_30">Se déplacer 30s en arrière</string>
    <string name="label_forward_30">Se déplacer 30s en avant</string>
    <string name="label_stop">Arrêter la lecture</string>
    <string name="prepare_casting_to_device">Préparation de la diffusion sur %1$s</string>

    <!-- Permissions -->
    <string name="dialog_permission_issue">Problème de permission</string>
    <string name="permission_external_storage_rationale">
        L\'accès au stockage externe est nécessaire pour diffuser des fichiers multimédia et pour
        la mise en cache des images.
    </string>
    <string name="no_read_external_storage_permission">
        Cette application ne peut pas fonctionner proprement parce qu\'elle n\'a pas la permission
        de lire des fichiers multimédia sur le stockage externe.
    </string>
    <string name="permission_access_location_rationale">
        L\'accès à l\'emplacement est nécessaire pour la précision du mode nuit.
    </string>
    <string name="no_access_coarse_location_permission">
        Le mode nuit ne peut pas être précis s\'il n\'a pas la permission d\'accéder à un lieu
        approximatif.
    </string>
    <string name="no_access_fine_location_permission">
        Le mode nuit ne peut pas être précis s\'il n\'a pas la permission d\'accéder à un
        emplacement précis.
    </string>

    <!-- Playback -->

    <string name="forward">Lire 30s en avant</string>
    <string name="replay">Lire 30s en arrière</string>
    <string name="start_playback">Démarrer la lecture</string>
    <string name="pause_playback">Mettre en pause</string>
    <string name="stop_playback">Arrêter la lecture</string>
    <string name="muting_on">Couper le son</string>
    <string name="muting_off">Réactiver le son</string>

    <!-- Playback controller -->
    <string name="time_shift_backward">-%1$d secondes</string>
    <string name="time_shift_forward">+%1d secondes</string>
    <string name="controller_pause">Mettre en pause</string>
    <string name="controller_play">Lire</string>
    <string name="controller_stop">Arrêter</string>
    <string name="expanded_controller_backdrop">Toile de fond du contrôleur</string>

    <!-- Settings -->
    <string name="setting_night_mode_title">Mode nuit</string>
    <string name="setting_night_mode_summary_off">Ne pas activer le mode nuit.</string>
    <string name="setting_night_mode_summary_on">
        Activer automatiquement le mode nuit lorsque l\'application détermine qu\'il fait nuit.
    </string>
    <string name="pref_display_settings_title">Affichage</string>
    <string name="pref_display_mode_title">
        Mode d\'affichage au lancement de l\'application
    </string>
    <string name="pref_images_sort_mode_title">
        Mode de tri des images au lancement de l\'application
    </string>
    <string name="pref_videos_sort_mode_title">
        Mode de tri des vidéos au lancement de l\'application.
    </string>
    <string name="pref_storage_title">Stockage</string>
    <string name="media_lib_folder">
        Dossier de la médiathèque au lancement de l\'application.
    </string>
    <string name="pref_playback_settings_title">Lecture</string>
    <string name="setting_double_tap_gesture_title">Geste de double frappe</string>
    <string name="setting_double_tap_gesture_summary_off">Désactiver la détection du geste
        de double frappe.</string>
    <string name="setting_double_tap_gesture_summary_on">
        Activer la détection du geste de double frappe sur chaque moitié (droite et gauche)
        de l\'écran pour déplacer la position de lecture.
    </string>
    <string name="pref_time_backward_title">
        Temps associé au déplacement en arrière de la position de lecture</string>
    <string name="pref_time_forward_title">
        Temps associé au déplacement en avant de la position de lecture</string>

    <!-- UPnP core -->
    <string name="setup_playback_error">
        L\'application n\'a pas réussit à configurer le fichier multimédia sur le récepteur
        multimédia.
    </string>
    <string name="upnp_avtransport_getpositioninfo_error">
        L\'application n\'a pas réussit à obtenir la position de lecture de la vidéo sur le
        récepteur multimédia.
    </string>
    <string name="upnp_avtransport_stop_error">
        L\'application n\'a pas réussit à arrêter la lecture du fichier multimédia sur le récepteur
        multimédia.
    </string>
    <string name="upnp_avtransport_play_error">
        L\'application n\'a pas réussit à démarrer la lecture du fichier multimédia sur le
        récepteur multimédia.
    </string>
    <string name="upnp_avtransport_pause_error">
        L\'application n\'a pas réussit à mettre en pause la vidéo sur le récepteur multimédia.
    </string>
    <string name="upnp_avtransport_seek_error">
        L\'application n\'a pas réussit à rechercher la nouvelle position de lecture sur le
        récepteur multimédia.
    </string>
    <string name="upnp_support_mimetype_error">
        Le MIME Type %1$s du fichier multimédia n\'est pas supporté par le récepteur multimédia.
    </string>
    <string name="upnp_init_connection_error">
        L\'application n\'a pas réussit à initialiser la connexion à l\'appareil %1$s.
    </string>

    <!-- Media browser -->
    <string name="media_lib">Médiathèque</string>
    <string name="description_display_settings">Options d\'affichage</string>
    <string name="display_mode">Mode d\'affichage</string>
    <string name="sort_mode">Mode de tri</string>
    <string name="settings">Paramètres</string>
    <string name="no_media_found">Pas de fichier multimédia trouvé</string>
    <string name="no_image_found">Pas d\'image trouvée</string>
    <string name="no_video_found">Pas de vidéo trouvée</string>

    <!-- Media controller -->
    <string name="cast_controller_title">Contrôleur Cast</string>
    <string name="upnp_controller_title">Contrôleur UPnP</string>

    <!-- Pager -->
    <string name="images_pager_tab_title">IMAGES</string>
    <string name="videos_pager_tab_title">VIDÉOS</string>
    <string name="seek_notavailable">
        Le déplacement de la position de lecture n\'est pas disponible lorsque le média n\'est pas en lecture
    </string>
</resources>
